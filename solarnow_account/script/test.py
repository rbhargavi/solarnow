import oerplib

SOURCE_USER = 'admin'
SOURCE_PASSWORD = 'admin'
SOURCE_HOSTNAME = '127.0.0.1'
SOURCE_DATABASE = '80_solarnow1'
SOURCE_PORT = 8069

# Prepare the connection to the server
oerp = oerplib.OERP(SOURCE_HOSTNAME, protocol='xmlrpc', port=SOURCE_PORT)

# Login (the object returned is a browsable record)
user = oerp.login(SOURCE_USER, SOURCE_PASSWORD, SOURCE_DATABASE)
print(user.name)            # name of the user connected

count = 0
# Use all methods of a model class
invoice_obj = oerp.get('account.invoice')
for pl in invoice_obj.browse(43094).payplan_line_ids:
    count += 1
    print(str(count) + ' ' + str(pl.id) + ' ' + pl.name + ' ' + str(pl.amount))
