# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp.exceptions import Warning
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime

class wizard_invoice_postpone(models.TransientModel):
    _name = 'wizard.invoice.postpone'

    postpone_start_date = fields.Date("Start Date", required=True)

    postpone_period = fields.Integer('Postpone period', require=True)
    postpone_period_unit = fields.Selection([
        ('day', 'Days'),
        #('week', 'Week'),
        ('month', 'Month'),
        #('year', 'Year')
    ], string="Postpone period unit", required=True)

    @api.multi
    def do_postpone(self):
        # TODO: check for 3 times postpone max 1 month 

        delta = None
        if self.postpone_period_unit == 'month':
            delta = relativedelta(months=self.postpone_period)
        elif self.postpone_period_unit == 'day':
            delta = relativedelta(days=self.postpone_period)

        for inv in self.env['account.invoice'].browse(self.env.context['active_ids']):
            if inv.state != 'open' or not inv.is_credit:
                raise Warning('You can only perform Reschedule on an open payplan invoice!')

            if inv.move_id:
                for line in inv.move_id.line_id:
                    if line.payplan_type in ('interest','principal','downpayment') and line.date_maturity and line.date_maturity >= self.postpone_start_date:
                        line.date_maturity = (datetime.strptime(line.date_maturity, DEFAULT_SERVER_DATE_FORMAT) + delta).strftime(DEFAULT_SERVER_DATE_FORMAT)

        return {}
