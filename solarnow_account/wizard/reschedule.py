# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.addons.solarnow_account.model import util

class wizard_invoice_reschedule(models.TransientModel):
    _name = 'wizard.invoice.reschedule'

    def _default_invoice(self):
        assert self.env.context['active_model'] == 'account.invoice'
        assert len(self.env.context['active_ids']) == 1
        return self.env['account.invoice'].browse(self.env.context['active_ids'])
        

    def _default_open_balance(self):
        if 'active_model' in self.env.context and \
                        self.env.context['active_model'] == 'account.invoice':
            assert len(self.env.context['active_ids']) == 1
            return self.env['account.invoice'].browse(self.env.context['active_ids']).residual_principal
        return 0
    
    @api.onchange('reschedule_reason')
    def _compute_reschedule(self):
        if self.reschedule_reason == 'solarnow':
            self.reschedule_fee = 0
        else:
            self.reschedule_fee = self.invoice.company_id.reschedule_fee

    invoice = fields.Many2one('account.invoice', string="orginal invoice", default=_default_invoice)

    open_balance = fields.Float('Open Principal', default=_default_open_balance)
    reschedule_fee = fields.Float('Reschedule fee', compute='_compute_reschedule')
    reschedule_reason = fields.Selection([('solarnow','Solarnow'),('customer','Customer')], default='customer', required=True, string="Responsible")

    payment_term = fields.Many2one('account.payment.term', "Payment term")

    installments = fields.Integer('Number of installments')

    installment_interval = fields.Integer('Installment interval')
    installment_interval_unit = fields.Selection([
        # ('day', 'Days'),
        # ('week', 'Week'),
        ('month', 'Month'),
        # ('year', 'Year')
    ], string="Installment interval unit")
    
    grace_period = fields.Integer('Grace period')
    grace_period_unit = fields.Selection([
        # ('day', 'Days'),
        # ('week', 'Week'),
        ('month', 'Month'),
        # ('year', 'Year')
    ], string="Grace period unit")

    @api.onchange('payment_term')
    def onchange_payment_term(self):
        self.deposit = 0 #self.payment_term.deposit
        self.installments = self.payment_term.installments
        self.installment_interval = self.payment_term.installment_interval
        self.installment_interval_unit = self.payment_term.installment_interval_unit
        self.grace_period = self.payment_term.grace_period
        self.grace_period_unit = self.payment_term.grace_period_unit

    @api.multi
    def do_reschedule(self):
        original = self.invoice
        paid_principal = original.amount_total - original.amount_interest - original.residual_principal
        
        if original.state != 'open' or not original.is_credit:
            raise Warning('You can only perform Reschedule on an open payplan invoice!')

        # FIXME: only fully paid installments ?
        paid_dates = []
        unpaid_installments = []
        for l in original.move_id.line_id:
            if l.payplan_type in ('principal', 'interest', 'downpayment'):
                if l.date_paid:
                    paid_dates.append(l.date_paid)
                if l.payplan_type in ('principal', 'interest') and l.installment_state not in ('paid', 'late'):
                    unpaid_installments.append(l)

        last_paid_date = max(paid_dates) if paid_dates else original.create_date

        #
        # REFUND original
        # 
        refund = original.with_context(open_balance=self.open_balance).sudo().refund()[0]
        refund.with_context(open_balance=self.open_balance).sudo().signal_workflow('invoice_open')

        #
        # RECONCILE
        #
        refund_move_lines = [l for l in refund.move_id.line_id]
        original_move_lines = [l for l in original.move_id.line_id]
        list_accounts = [original.account_id.id, original.company_id.receivable_interest_account_id.id]
        util.collect_and_reconcile(self, refund_move_lines + original_move_lines, account_ids=list_accounts)

        #
        # CREATE new invoice
        #
        invoice = original.sudo().copy()

        delete_lines = []
        for line in invoice.invoice_line:
            if line.is_interest_line:
                delete_lines.append((2,line.id))
        if delete_lines:
            invoice.write({'invoice_line': delete_lines})
            invoice.button_reset_taxes()


        if not invoice.currency_id.is_zero(paid_principal):
            account = self.env['ir.property'].get('property_account_income_categ', 'product.category')

            grouped_interest = {}
            for line in original.invoice_line:
                if line.is_interest_line:
                    continue

                group_key = invoice._interest_grouping_key(line)
                grouped_interest.setdefault(group_key, {'amount': 0, 'taxes': map(lambda t: t.id, line.invoice_line_tax_id)})['amount'] += line.price_unit * line.quantity            
            
            for paid, part in zip(grouped_interest.values(), util.proportion(map(lambda x: x['amount'], grouped_interest.values()), paid_principal)):
                invoice.write({
                    'invoice_line': [(0,0,{
                        'name': 'paid principal before reschedule',
                        'price_unit': part,
                        'quantity': -1,
                        'account_id': account.id, # QUESTION: what if products in original invoice split over different accounts
                        'invoice_line_tax_id': [(6,0,paid['taxes'])]
                    })]
                })

        if not invoice.currency_id.is_zero(self.reschedule_fee):
            account = self.env['ir.property'].get('property_account_income_categ', 'product.category') # FIXME: different account
            invoice.write({
                'invoice_line': [(0, 0, {
                    'name': 'reschedule fee',
                    'price_unit': self.reschedule_fee,
                    'quantity': 1,
                    'account_id': account.id 
                })]
            })

        company = self.env.user.company_id

        original.reference = original.reference + ' RESCHEDULED'
        for l in unpaid_installments:
            l.write({
                'name': l.name + ' RESCHEDULED',
                'reschedule_refund': refund.id
            })

        # Remove subsidy lines
        if not invoice.currency_id.is_zero(invoice.amount_subsidy):
            for line in invoice.invoice_line.filtered(lambda l: l.discount_id and l.discount_id.subsidized):
                invoice.write({
                    'invoice_line': [(2, line.id, 0)]
                })

        # QUESTION: no deposit on reschedule, or not default ?
        invoice.deposit = 0
        invoice.installment_interval_unit = self.installment_interval_unit
        invoice.installments = self.installments
        invoice.interest_rate = company.interest_rate
        invoice.installment_interval = self.installment_interval
        invoice.grace_period = self.grace_period
        invoice.grace_period_unit = self.grace_period_unit
        invoice.reschedule_of = original
        invoice.payplan_date = last_paid_date

        # Update sale order
        # if invoice.sale_order:
        #     invoice.sale_order.deposit = invoice.deposit
        #     invoice.sale_order.installment_interval_unit = invoice.installment_interval_unit
        #     invoice.sale_order.installments = invoice.installments
        #     invoice.sale_order.interest_rate = company.interest_rate
        #     invoice.sale_order.installment_interval = invoice.installment_interval
        #     invoice.sale_order.grace_period = invoice.grace_period
        #     invoice.sale_order.grace_period_unit = invoice.grace_period_unit

        invoice.update_interest()
        invoice.button_reset_taxes()
        invoice.signal_workflow('invoice_open')

        # TODO: Go to rescheduled invoice
        return {}
