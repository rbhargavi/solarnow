# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
from openerp.exceptions import Warning
from openerp.addons.solarnow_account.model import util

class wizard_invoice_paynow(models.TransientModel):
    _name = 'wizard.invoice.paynow'

    def _default_open_balance(self):
        assert self.env.context['active_model'] == 'account.invoice'
        assert len(self.env.context['active_ids']) == 1
        
        return self.env['account.invoice'].browse(self.env.context['active_ids']).residual_principal

    def _default_interest(self):
        assert self.env.context['active_model'] == 'account.invoice'
        assert len(self.env.context['active_ids']) == 1

        return self.env['account.invoice'].browse(self.env.context['active_ids']).paynow_interest

    def _default_paynow_fee(self):
        assert self.env.context['active_model'] == 'account.invoice'
        assert len(self.env.context['active_ids']) == 1
        
        return self.env['account.invoice'].browse(self.env.context['active_ids']).company_id.paynow_fee

    open_balance = fields.Float('Open Principal', default=_default_open_balance)
    interest = fields.Float('Interest', default=_default_interest)
    paynow_fee = fields.Float('Paynow fee', default=_default_paynow_fee)

    @api.multi
    def do_paynow(self):
        original = self.env['account.invoice'].browse(self.env.context['active_ids']) 
        paid_principal = original.amount_total - original.amount_interest - original.residual_principal

        if original.state != 'open' or not original.is_credit:
            raise Warning('You can only perform PayNow on an open payplan invoice!')

        #
        # REFUND original
        # 
        refund = original.with_context(open_balance=self.open_balance).refund()[0]
        refund.with_context(open_balance=self.open_balance).signal_workflow('invoice_open')

        #
        # RECONCILE
        #
        util.collect_and_reconcile(self, 
                                   [l for l in refund.move_id.line_id] + [l for l in original.move_id.line_id],
                                   account_ids=[original.account_id.id, original.company_id.receivable_interest_account_id.id], 
                                   )
        #
        # CREATE new invoice
        #
        invoice = original.copy()
        delete_lines = []
        for line in invoice.invoice_line:
            if line.is_interest_line:
                delete_lines.append((2,line.id))
        if delete_lines:
            invoice.write({'invoice_line': delete_lines})
            invoice.button_reset_taxes()

        # unset payplan
        invoice.payment_term = False
        invoice.is_credit = False

        if not invoice.currency_id.is_zero(paid_principal):
            account = self.env['ir.property'].get('property_account_income_categ', 'product.category')
            invoice.write({
                'invoice_line': [(0,0,{
                    'name': 'paid principal before paynow',
                    'price_unit': paid_principal,
                    'quantity': -1,
                    'account_id': account.id # QUESTION: what if products in original invoice split over different accounts
                })]
            })

        # FIXME: split by taxes
        if self.interest:        
            invoice.write({
                'invoice_line': [(0,0,{
                    'name': 'interest due',
                    'price_unit': self.interest,
                    'quantity': 1,
                    'product_id': invoice.company_id.interest_product_id and invoice.company_id.interest_product_id.id or False,
                    'account_id': invoice.company_id.get_interest_income_account()
                })]
            })

        if self.paynow_fee:
            account = self.env['ir.property'].get('property_account_income_categ', 'product.category')
            invoice.write({
                'invoice_line': [(0,0,{
                    'name': 'paynow fee',
                    'price_unit': self.paynow_fee,
                    'quantity': 1,
                    'account_id': account.id # QUESTION: what if products in original invoice split over different accounts
                })]
            })

        invoice.signal_workflow('invoice_open')

        return {}
