openerp.solarnow_account = function (instance, local) {

    instance.PayplanPreview = instance.web.form.AbstractField.extend({
        template: "PayplanPreviewTemplate",
        init: function(parent, action) {
            this._super.apply(this, arguments);
        },
        start: function() {
            var self = this;
            this._super();
            this.display_field();
            this.render_value();
        },
        display_field: function () {
            var self = this;

        },
        render_value: function() {
            var self = this;

            var payplan = JSON.parse(this.get('value'));
            if (!payplan.installments) {
                return;
            }

            var orig_date_header = '';
            var orig_date_footer = '';
            if(!payplan.is_preview) {
                orig_date_header += '<td>Original Due Date</td>';
                orig_date_footer += '<td> </td>';
            }

            self.$el.find(".body").html('<table id="payplan-data" style="width:100%;"><thead><tr><td>Date</td><td>Principal</td><td>Interest</td><td>Amount</td>' + orig_date_header + '</tr></thead></table>');

            for (var i = 0; i < payplan.installments.length; i++) {
                var orig_date_value = '';
                if(!payplan.is_preview)
                    orig_date_value += '<td> '+ instance.web.format_value(payplan.installments[i][5], {type: 'date'}) +'</td>';

                self.$el.find('#payplan-data').append('<tr> \
                                                      <td> '+ instance.web.format_value(payplan.installments[i][0], {type: 'date'}) +'</td> \
                                                      <td> '+ instance.web.format_value(payplan.installments[i][1], {type: 'float', digits: [69,0]}) +'</td> \
                                                      <td> '+ instance.web.format_value(payplan.installments[i][2], {type: 'float', digits: [69,0]}) +'</td> \
                                                      <td> '+ instance.web.format_value(payplan.installments[i][4], {type: 'float', digits: [69,0]}) +'</td> \
                                                      ' + orig_date_value + ' \
                                                      </tr>')
            }
            self.$el.find('#payplan-data').append('<tfoot><tr> \
                                                  <td> Total </td> \
                                                  <td> '+ instance.web.format_value(payplan.total_principal, {type: 'float', digits: [69,0]}) +'</td> \
                                                  <td> '+ instance.web.format_value(payplan.total_interest, {type: 'float', digits: [69,0]}) +'</td> \
                                                  <td> '+ instance.web.format_value(payplan.total_amount, {type: 'float', digits: [69,0]}) +'</td> \
                                                  ' + orig_date_footer + ' \
                                                  </tr></tfoot>')
 
        }
    });
    instance.web.form.widgets.add('PayplanPreview', 'instance.PayplanPreview');
};
