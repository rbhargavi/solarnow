# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.sql import drop_view_if_exists


class res_partner_all_payment_overview(models.Model):
    _name = 'res.partner.all.payment.overview'
    _auto = False

    ref = fields.Char("Contact reference", size=128, readonly=True)
    name = fields.Char("Name", size=128, readonly=True)
    partner_id = fields.Many2one("res.partner", 'Partner', readonly=True)
    paid = fields.Float("Paid", readonly=True)
    owed = fields.Float("Owed", readonly=True)
    currently_owed = fields.Float("Currently owed", readonly=True)

    def init(self, cr):
        drop_view_if_exists(cr, 'res_partner_all_payment_overview')
        cr.execute("""
            create or replace view res_partner_all_payment_overview as (
            select
            res_partner.id as id,
            ref,
            name,
            res_partner.id as partner_id,
            paid as paid,
            owed,
            owed -paid as currently_owed
            from
            res_partner
            left join (
            select partner_id, sum(debit) owed
            from account_move_line
            where
            payplan_type in ('principal', 'interest', 'downpayment')
            and date_maturity < now() -- '2016-05-17'
            group by partner_id
            ) as owed_amounts on (res_partner.id = owed_amounts.partner_id)

            left join (
            select partner_id, sum(credit - debit) paid
            from account_move_line
            inner join account_journal on (journal_id = account_journal.id)
            where
            account_journal.type = 'bank' and
            account_id in (4920, 4915)
            group by partner_id
            ) as paid_amounts on (owed_amounts.partner_id = paid_amounts.partner_id)
            )
        """)
