# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from datetime import date
import openerp.addons.decimal_precision as dp
from util import collect_and_reconcile
import logging

_logger = logging.getLogger(__name__)


class res_partner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    def _cron_compute_days_payments(self):
        all_move_lines = self.env['account.move.line'].search([('date_maturity','<', date.today()),('installment_state','=','open')])
        all_move_lines._compute_payplan()

    @api.multi
    @api.depends('invoice_ids', 'invoice_ids.state')
    def _compute_open_payplans(self):
        for partner in self:
            open = partner.invoice_ids.filtered(lambda r: r.state == 'open')
            paid = partner.invoice_ids.filtered(lambda r: r.state == 'paid')
            if not open and paid:
                partner.no_open_payplans = True
            else:
                partner.no_open_payplans = False

    @api.multi
    @api.depends('invoice_ids', 'invoice_ids.state')
    def _compute_running_payplans(self):
        # This method is written bypassing the orm
        # to avoid recursive calls of computed field
        # causing cpu load at 100%
        if len(self) > 1:
            str_ids = str(tuple(self.ids))
        else:
            str_ids = "(" + str(self.id) + ")"
        self.env.cr.execute("""
            SELECT partner_id, amount_total
            FROM res_partner_running_payplans
            WHERE res_partner_running_payplans.partner_id IN """ + str_ids
        )
        res = self.env.cr.dictfetchall()
        for partner in self:
            for item in res:
                if item['partner_id'] == partner.id:
                    partner.total_amount_running_payplans = item['amount_total']
                    break

    @api.multi
    @api.depends('comment_ids')
    def _get_date_last_comment(self):
        for partner in self:
            comments = partner.comment_ids.sorted(key=lambda r: r.create_date, reverse=True)
            if comments:
                partner.date_last_comment = comments[0].create_date

    @api.multi
    def _get_paid_count(self):
        for partner in self:
            partner.paid_count = 0.0
            for line in partner.bank_statement_line_ids:
                partner.paid_count += line.amount

    account_move_line_ids = fields.One2many('account.move.line', 'partner_id', string='Journal Items')
    no_open_payplans = fields.Boolean("Customer has no open payplans", compute='_compute_open_payplans', store=True)

    payplan_ids = fields.One2many('account.invoice', 'partner_id', string='Invoices',
        readonly=True, copy=False, domain=[('is_credit','=',True),('type','=','out_invoice')])

    bank_statement_line_ids = fields.One2many('account.bank.statement.line', 'partner_id', 'Bank Statement Lines')
    paid_count = fields.Float(compute='_get_paid_count', string='Paid count')

    payment_summary = fields.Float(string="summary") # payment_summary
    installment_count = fields.Integer(string="Installment count") # installment_count
    installment_sum = fields.Integer(string="Installment sum") # installment_sum


    payplan_summary = fields.Float(string="Open Amount", required=False, default=0.0)
    overdue_installment_sum = fields.Integer(string="Amount Overdue", required=False, default=0)
    days_overdue = fields.Integer(rstring="Days Overdue", required=False, default=0)
    overdue_installment_count = fields.Integer(string="Overdue Installments", required=False, default=0)
    amount_next_payment = fields.Float(string="Amount Next Payment", digits=dp.get_precision('Account'))
    total_amount_running_payplans = fields.Float(compute='_compute_running_payplans', store=True, string="Total Amount Running Payplans", digits=dp.get_precision('Account'), readonly=True)
    days_next_payment = fields.Integer(string="Days until next payment", required=False, default=0)
    date_last_payment = fields.Date(string="Date last payment")
    date_first_payment = fields.Date(string="Date first payment")
    date_last_comment = fields.Date(string="Date last comment", compute="_get_date_last_comment")

    @api.model
    def _cron_compute_portfolio(self):
        self._compute_portfolio(tuple(e.id for e in self.env['res.partner'].search([])))

    @api.model
    def _compute_portfolio(self, ids):
        self.env.cr.execute("""
             create or replace view res_partner_portfolio as (
             with installments as (
               select partner_id, min(date_maturity) next_payment_date
               from account_move_line
               where partner_id is not null
               and payplan_type in ('principal', 'interest', 'downpayment')
               and installment_state = 'open'
               group by partner_id
             )
             select
                 account_move_line.partner_id as id,
                 account_move_line.partner_id,
                 sum(case when type = 'bank' then debit else 0 end) as payment_summary,
                 sum(case when payplan_type in ('principal', 'interest', 'downpayment') then open_amount else 0 end) as payplan_summary,
                 sum(case when payplan_type in ('principal', 'interest', 'downpayment') then amount else 0 end) as installment_sum,
                 sum(case when payplan_type in ('principal','downpayment') then 1 else 0 end) as installment_count,
                 sum(case when payplan_type in ('principal', 'interest', 'downpayment') and installment_state = 'overdue' then open_amount else 0 end) as overdue_installment_sum,
                 sum(case when payplan_type in ('principal','downpayment') and installment_state = 'overdue' then 1 else 0 end) as overdue_installment_count,

                 max(case when payplan_type in ('principal', 'interest', 'downpayment') and installment_state = 'overdue' then now()::date - date_maturity end) as days_overdue,
                 min(case when payplan_type in ('principal', 'interest', 'downpayment') and installment_state = 'paid' then date_maturity end) as date_first_payment,
                 max(case when payplan_type in ('principal', 'interest', 'downpayment') and installment_state = 'paid' then date_maturity end) as date_last_payment,

                 next_payment_date - now()::date as days_next_payment,
                 sum(case when payplan_type in ('principal', 'interest', 'downpayment') and installment_state = 'open' and date_maturity = next_payment_date then amount end) as amount_next_payment

             from account_move_line
             left join installments on (account_move_line.partner_id = installments.partner_id)
             inner join account_account on (account_move_line.account_id = account_account.id)
             where account_move_line.partner_id in %s
             group by account_move_line.partner_id, next_payment_date
             )
         """, [ids])

        self.env.cr.execute("""
                UPDATE res_partner SET
                    payment_summary = (CASE WHEN res_partner_portfolio.payment_summary IS NOT NULL THEN res_partner_portfolio.payment_summary ELSE 0 END),
                    installment_count = (CASE WHEN res_partner_portfolio.installment_count IS NOT NULL THEN res_partner_portfolio.installment_count ELSE 0 END),
                    installment_sum = (CASE WHEN res_partner_portfolio.installment_sum IS NOT NULL THEN res_partner_portfolio.installment_sum ELSE 0 END),
                    payplan_summary = (CASE WHEN res_partner_portfolio.payplan_summary IS NOT NULL THEN res_partner_portfolio.payplan_summary ELSE 0 END),
                    overdue_installment_sum = (CASE WHEN res_partner_portfolio.overdue_installment_sum IS NOT NULL THEN res_partner_portfolio.overdue_installment_sum ELSE 0 END),
                    days_overdue = (CASE WHEN res_partner_portfolio.days_overdue IS NOT NULL THEN res_partner_portfolio.days_overdue ELSE 0 END),
                    overdue_installment_count = (CASE WHEN res_partner_portfolio.overdue_installment_count IS NOT NULL THEN res_partner_portfolio.overdue_installment_count ELSE 0 END),
                    amount_next_payment = (CASE WHEN res_partner_portfolio.amount_next_payment IS NOT NULL THEN res_partner_portfolio.amount_next_payment ELSE 0 END),
                    days_next_payment = (CASE WHEN res_partner_portfolio.days_next_payment IS NOT NULL THEN res_partner_portfolio.days_next_payment ELSE 0 END),
                    date_last_payment = res_partner_portfolio.date_last_payment,
                    date_first_payment = res_partner_portfolio.date_first_payment
                FROM res_partner_portfolio
                WHERE res_partner.id = res_partner_portfolio.partner_id AND res_partner.id IN %s
            """, [ids])

    def book_balance(self, src_account, dst_account, max_amount=0):
        """Compute balance for partner on src_account. Book to dst_account
        """
        if max_amount == 0:
            return False

        for partner in self:

            reserved_lines = []
            for sale_order in partner.sale_order_ids:
                if sale_order.downpayment_line_id:
                    reserved_lines.append(sale_order.downpayment_line_id.id)

            # compute balance
            unreconciled_lines = []
            total_balance = 0
            for move_line in partner.account_move_line_ids:
                if move_line.account_id == src_account:
                    total_balance += move_line.debit - move_line.credit
                    if not move_line.reconcile_id \
                            and not move_line.reconcile_partial_id \
                            and not move_line.id in reserved_lines:
                        unreconciled_lines += move_line

            balance = sum([l.debit - l.credit for l in unreconciled_lines])

            # don't automatically move debit balance or if moving it would create shortage 
            if balance > 0 or balance < total_balance:
                continue

            move_ref = 'Moving balance for {name} from {src} to {dst}'.format(
                name=partner.name,
                src=src_account.name,
                dst=dst_account.name)

            move_dict = {
                'ref': move_ref,
                'journal_id': src_account.company_id.downpayment_journal_id.id,
                #  'period_id': invoice.period_id.id,
                'line_id': [
                    (0,0,{
                        'name': move_ref,
                        'account_id': src_account.id,
                        'credit': 0, # updated later on #balance > 0 and balance or 0, # balance is negative, see sanity check
                        #'debit': balance < 0 and min(-balance, max_amount) or 0,
                        'debit': False,
                        'partner_id': partner.id,
                    })
                ]
            }
            booked_lines = []
            unreconciled_lines.sort(key=lambda l3: l3.date_paid)
            for l in unreconciled_lines:
                if l.debit:
                    continue

                bank_statement_line = l.get_bank_statement_line(l)
                move_dict['line_id'].append(
                    (0,0,{
                        'name': 'dst ' + move_ref,
                        'account_id': dst_account.id,
                        'credit': min(l.credit, max_amount - sum([l2.credit - l2.debit for l2 in booked_lines])),
                        'debit': l.debit,
                        'partner_id': partner.id,
                        'bank_statement_line': bank_statement_line and bank_statement_line.id or False,
                    })
                )

                booked_lines.append(l)
                booked_balance = sum([l2.credit - l2.debit for l2 in booked_lines])

                # update total booked balance
                move_dict['line_id'][0][2]['debit'] = booked_balance

                if booked_balance > max_amount:
                    bank_statement_line = l.get_bank_statement_line(l)

                    leftover_amount = booked_balance - max_amount
                    move_dict['line_id'].extend([
                        (0,0,{
                            'name': "Leftover when moving balance for {name}".format(name=partner.name),
                            'account_id': src_account.id,
                            'credit': leftover_amount,
                            'debit': 0,
                            'partner_id': partner.id,
                            'bank_statement_line': bank_statement_line and bank_statement_line.id or False
                        })
                        ])
                    
                    # exit loop when there's a leftover 
                    break

            if not booked_lines:
                return False

            move = self.env['account.move'].create(move_dict)
            move.post()

            # reconcile
            collect_and_reconcile(self,
                                  [l for l in move.line_id if l.account_id.id == src_account.id and l.debit > 0] + booked_lines,
                                  account_ids=[src_account.id]
                                  )

            return move

    @api.multi
    def find_open_downpayments(self):
        orders_list = []
        for partner in self:
            for order in partner.sale_order_ids:
                if order.state in ['assessment', 'approval', 'incomplete', 'sent', 'approved']:
                    if not order.downpayment_line_id:
                        orders_list += order
        return orders_list

    def init(self, cr):
        cr.execute("""
             create or replace view res_partner_running_payplans AS (
                 SELECT account_invoice.partner_id,
                        SUM(account_invoice.amount_total) AS amount_total
                 FROM account_invoice
                 WHERE account_invoice.state like 'open'
                 GROUP BY account_invoice.partner_id
             )
         """)
