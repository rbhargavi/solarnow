# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
import numpy
import json
import re
import util

from operator import itemgetter
from datetime import datetime
from dateutil.relativedelta import relativedelta

from openerp import models, fields, api, exceptions
import openerp.addons.decimal_precision as dp
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

from util import collect_and_reconcile

_logger = logging.getLogger(__name__)


class account_invoice_line(models.Model):
    _inherit = 'account.invoice.line'
    
    is_interest_line = fields.Boolean("Is interest line (internal use)")


class account_invoice(models.Model):
    _inherit = 'account.invoice'

    @api.onchange('payment_term')
    def onchange_payment_term(self):
        self.is_credit = self.payment_term.is_credit
        self.deposit = self.payment_term.deposit
        self.installments = self.payment_term.installments
        self.installment_interval = self.payment_term.installment_interval
        self.installment_interval_unit = self.payment_term.installment_interval_unit
        self.grace_period = self.payment_term.grace_period
        self.grace_period_unit = self.payment_term.grace_period_unit

    @api.one
    def _payplan_parameters(self):
        # FIXME: better ui
        adjustments = {}
        if self.adjustments and re.match(r'^[0-9]+:[0-9]+(,[0-9]+:[0-9]+)*$', self.adjustments):
            adjustments = eval('{' + self.adjustments + '}')
            
        amount = sum(l.price_unit * l.quantity for l in self.invoice_line if not l.is_interest_line)

        return {
            'currency': self.currency_id,
            'is_credit': self.is_credit,
            'amount': amount - self.amount_subsidy, #self.amount_untaxed + self.amount_tax - self.amount_subsidy,
            'downpayment_percentage': self.deposit,
            'number_of_installments': self.installments,
            'yearly_interest_rate': self.interest_rate,
            'installment_interval': self.installment_interval,
            'installment_interval_unit': self.installment_interval_unit,
            'grace_period': self.grace_period,
            'grace_period_unit': self.grace_period_unit,
            'date_ref': self.payplan_date,
            'stock_given_out_date': self.stock_given_out_date,
            'adjustments': adjustments,
        }

    @api.one
    def _compute_payplan_lines(self):

        payplan = {}
        if self.is_credit:
            if self.move_id:
                payplan = self.compute_payplan_summary()
            else:
                payplan = json.loads(self.preview_payplan_pentaho)

        is_principal = False
        payplan_lines = self.env['payplan.line'].browse([])
        subsidy = self.amount_subsidy
        subsidy_date_payment = self.create_date
        for pline in payplan.get('installments') or []:
            if self.is_credit and self.move_id:
                subsidy_date_payment = pline[6]
            break
        if subsidy:
            vals = {
                'date_maturity': None,
                'payplan_type': 'subsidy',
                'amount': subsidy,
                'date_paid': subsidy_date_payment,
            }
            payplan_lines = self.env['payplan.line'].create(vals)
            _logger.info("Get subsidy")
        for pline in payplan.get('installments') or []:
            payplan_type = is_principal and 'principal' or 'downpayment'
            vals = {
                'date_maturity': pline[0],
                'payplan_type': payplan_type,
                'amount': pline[4],
            }
            if self.is_credit and self.move_id:
                vals['date_paid'] = pline[6]
            payplan_lines |= self.env['payplan.line'].create(vals)
            _logger.info("Get downpayment/principal")
            is_principal = True
        self.payplan_line_ids = payplan_lines.sorted(key=lambda r: r.date_maturity)

    @api.one
    def _compute_preview_payplan_pentaho(self):
        if self.is_credit:
            payplan_parameters = self._payplan_parameters()[0]
            if self.move_id:
                payplan = self.compute_payplan_summary()
            else:
                payplan = self.env['account.payment.term'].compute_preview(**payplan_parameters)
        else:
            payplan = {}
        self.preview_payplan_pentaho = json.dumps(payplan)

    @api.one
    @api.depends('invoice_line.price_subtotal',
                 'tax_line.amount',
                 'payment_term',
                 'installments',
                 'deposit',
                 'interest_rate',
                 'installment_interval',
                 'installment_interval_unit',
                 'grace_period',
                 'grace_period_unit')
    def _compute_amount(self):
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line)
        self.amount_tax = sum(line.amount for line in self.tax_line)
        self.amount_subsidy = sum(line.price_unit for line in self.invoice_line if line.discount_id and line.discount_id.subsidized)

        if self.is_credit:
            payplan_parameters = self._payplan_parameters()[0]
            self.amount_interest = self.env['account.payment.term'].compute_interest(**payplan_parameters)
            self.amount_downpayment = self.env['account.payment.term'].compute_downpayment(**payplan_parameters)
            if self.move_id:
                payplan = self.compute_payplan_summary()
            else:
                payplan = self.env['account.payment.term'].compute_preview(**payplan_parameters)
            self.preview_payplan = json.dumps(payplan)
        else:
            self.amount_interest = 0
            self.amount_downpayment = 0
            self.preview_payplan = json.dumps({})

        self.amount_total = self.amount_untaxed + self.amount_tax

    @api.model
    def _default_interest_rate(self):
        company = self.env.user.company_id
        return company.interest_rate

    amount_downpayment = fields.Float(
        string='Downpayment', digits=dp.get_precision('Account'),
        store=True, readonly=True, compute='_compute_amount', track_visibility='always')

    amount_interest = fields.Float(
        string='Interest', digits=dp.get_precision('Account'),
        store=True, readonly=True, compute='_compute_amount', track_visibility='always')

    amount_subsidy = fields.Float(
        string='Subsidy', digits=dp.get_precision('Account'),
        store=True, readonly=True, compute='_compute_amount', track_visibility='always')

    preview_payplan = fields.Text(
        "preview", compute="_compute_amount", readonly=True, store=False)

    preview_payplan_pentaho = fields.Text(
        "preview", compute="_compute_preview_payplan_pentaho", readonly=True, store=False)

    payplan_line_ids = fields.One2many(
        "payplan.line", 'invoice_id', compute="_compute_payplan_lines", store=False)

    payplan_date = fields.Date("Payplan reference date")
    deposit = fields.Integer('Deposit percentage')
    installments = fields.Integer('Number of installments')
    interest_rate = fields.Float("Interest", digits=(1,8), default=_default_interest_rate)

    installment_interval = fields.Integer('Installment interval')
    installment_interval_unit = fields.Selection([
        #'day', 'Days'),
        #('week', 'Week'),
        ('month', 'Month'),
        #('year', 'Year')
    ], string="Installment interval unit")

    grace_period = fields.Integer('Grace period')
    grace_period_unit = fields.Selection([
        #('day', 'Days'),
        #('week', 'Week'),
        ('month', 'Month'),
        #('year', 'Year')
    ], string="Grace period unit")

    adjustments = fields.Char(size=256, string="Adjustments")

    is_credit = fields.Boolean(string="Is Payplan")

    sale_order = fields.Many2one('sale.order', string="Sale Order")

    reschedule_of = fields.Many2one('account.invoice', string="Original invoice")

    stock_given_out_date = fields.Date("Stock given out date")

    @api.multi
    def _compute_residual_principal(self):
        for invoice in self:
            if invoice.move_id:
                residual_principal = 0
                paynow_interest = 0
                date_now = datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)
                next_due_date = date_now
                for line in invoice.move_id.line_id:
                    if line.date_maturity > date_now:
                        if line.date_maturity < next_due_date:
                            next_due_date = line.date_maturity

                for l in invoice.move_id.line_id:
                    if l.payplan_type == 'interest' and l.date_maturity <= next_due_date:
                        paynow_interest += l.open_amount
                    elif l.payplan_type in ('principal', 'downpayment') and not l.reconcile_id:
                        residual_principal += l.open_amount

                invoice.residual_principal = residual_principal
                invoice.paynow_interest = paynow_interest
                invoice.paynow_amount = residual_principal + paynow_interest + invoice.company_id.paynow_fee
            else:
                invoice.residual_principal = 0

    residual_principal = fields.Float(string="Residual principal", digits=dp.get_precision('Account'),
                                      readonly=True, compute='_compute_residual_principal', track_visibility='always')
    paynow_interest = fields.Float(string="Paynow interest", digits=dp.get_precision('Account'),
                                   readonly=True, compute='_compute_residual_principal', track_visibility='always')
    paynow_amount = fields.Float(string="Paynow", digits=dp.get_precision('Account'),
                                 readonly=True, compute='_compute_residual_principal', track_visibility='always')
    paynow_date = fields.Date(string="Paynow date", readonly=True)

    partner_ref = fields.Char(related='partner_id.ref')

    @api.depends('partner_id', 'partner_id.branch_id')
    @api.multi
    def _compute_branch(self):
        for inv in self:
            inv.branch = inv.partner_id.branch_id

    branch = fields.Many2one('res.branch', string="Branch", compute="_compute_branch", store=True)

    @api.multi
    def _interest_grouping_key(self, line):
        return (','.join(map(str, sorted(map(lambda t: t.id, line.invoice_line_tax_id)))),)

    @api.multi
    def update_interest(self):
        for invoice in self.with_context(updating_interest=True):
            
            grouped_interest = {}
            interest_invoice_lines = []
            for line in invoice.invoice_line:
                if line.is_interest_line:
                    interest_invoice_lines.append((2, line.id))
                    continue

                group_key = self._interest_grouping_key(line)
                grouped_interest.setdefault(group_key, {'amount': 0, 'taxes': map(lambda t: t.id, line.invoice_line_tax_id)})['amount'] += line.price_unit * line.quantity

            amount_interest = 0
            if invoice.is_credit:
                payplan_parameters = invoice._payplan_parameters()[0]
                amount_interest = self.env['account.payment.term'].compute_interest(**payplan_parameters)
                        
            for interest, part in zip(grouped_interest.values(), util.proportion(map(lambda x: x['amount'], grouped_interest.values()), amount_interest)):

                interest_invoice_lines.append((0,0,{
                    'is_interest_line': True,
                    'name': 'interest',
                    'product_id': invoice.company_id.interest_product_id and invoice.company_id.interest_product_id.id or False, 
                    'account_id': invoice.company_id.get_interest_income_account(),
                    'price_unit': part,
                    'quantity': 1,
                    'invoice_line_tax_id': [(6,0,interest['taxes'])],
                }))

            invoice.write({'invoice_line': interest_invoice_lines})

    @api.multi
    def _get_discount_journal(self):
        res = super(account_invoice, self)._get_discount_journal()
        for invoice in self:
            return invoice.company_id.discount_journal_id.id
        return res

    @api.multi
    def finalize_invoice_move_lines(self, move_lines):
        """ finalize_invoice_move_lines(move_lines) -> move_lines

            Hook method to be overridden in additional modules to verify and
            possibly alter the move lines to be created by an invoice, for
            special cases.
            :param move_lines: list of dictionaries with the account.move.lines (as for create())
            :return: the (possibly updated) final move_lines to create for this invoice
        """
        if self.type == 'out_invoice':
            if self.is_credit:
                if not self.company_id.receivable_interest_account_id:
                    raise exceptions.except_orm('Error', 'Interest receivable account on accounting configuration not set !')
    
                lines = [l for l in move_lines if l[2]['account_id'] != self.account_id.id]
    
                for t in self.payment_term.compute_payments(**self._payplan_parameters()[0]):
                    line_data = self.env['account.invoice'].line_get_convert({
                        'type': 'dest',
                        'name': t[2] == "downpayment" and 'downpayment' or 'installment',
                        'price': t[1],
                        'account_id': self.account_id.id,
                        'date_maturity': t[0],
    
                        #FIXME: currency
                        #'amount_currency': diff_currency and amount_currency,
                        #'currency_id': diff_currency and inv.currency_id.id,
                        #'ref': ref
                    }, self.partner_id.id, self.date_invoice)
    
                    line_data['payplan_type'] = t[2]
                    if t[2] == 'interest':
                        line_data['account_id'] = self.company_id.receivable_interest_account_id.id
    
                    lines.append((0,0,line_data))

                if self.amount_subsidy:
                    lines.append((0,0,self.env['account.invoice'].line_get_convert({
                        'type': 'dest',
                        'name': 'subsidy',
                        'price': self.amount_subsidy,
                        'account_id': self.account_id.id
                    }, self.partner_id.id, self.date_invoice))) 
                    lines[-1][2]['payplan_type'] = 'subsidy'

                #TODO temporary fix, remove after all rounding errors have passed
                if sum([l[2]['debit']-l[2]['credit'] for l in lines]) == 1000:
                    dp = [l for l in lines if l[2].get('payplan_type', '') == 'downpayment']
                    if len(dp) == 1:
                        dp[0][2]['debit'] -= 1000

                return lines
            else:
                return move_lines
        else:
            return move_lines

    @api.multi
    def book_downpayment(self):
        for invoice in self:
            if invoice.is_credit and invoice.amount_downpayment:
                downpayment_receivable = [l for l in self.move_id.line_id if l.payplan_type == 'downpayment']
                assert len(downpayment_receivable) == 1
                downpayment_receivable = downpayment_receivable[0]
                
                if not invoice.sale_order.downpayment_line_id:
                    # try to rereserve missing downpayment
                    invoice.sale_order.reserve_downpayment()
                elif invoice.sale_order.amount_downpayment != invoice.sale_order.downpayment_line_id.credit:
                    invoice.sale_order.unreserve_downpayment()
                    invoice.sale_order.reserve_downpayment()

                if not invoice.sale_order.downpayment_line_id:
                    raise exceptions.Warning("Downpayment missing", "Make sure the downpayment amount is available on the downpayment account!")
                elif invoice.sale_order.amount_downpayment != invoice.sale_order.downpayment_line_id.credit:
                    raise exceptions.Warning("Downpayment wrong", "Make sure the reserved downpayment matches the downpayment amount!")

                reserved_downpayment = invoice.sale_order.downpayment_line_id

                move_ref = 'Downpayment for {name}'.format(name=invoice.sale_order.name)

                # book downpayment to receivable
                move_dict = {
                    'ref': move_ref,
                    'journal_id': invoice.company_id.downpayment_journal_id.id,
                    'period_id': invoice.period_id.id,
                    'line_id': [
                        (0,0,{
                            'name': move_ref,
                            'account_id': reserved_downpayment.account_id.id,
                            'credit': 0,
                            'debit': reserved_downpayment.credit,
                            'partner_id': reserved_downpayment.partner_id.id,
                        })
                        ]
                    }
                for l in reserved_downpayment.move_id.line_id:
                    if not l.name or not l.name.startswith('Reserved Downpayment'):
                        continue
                    move_dict['line_id'].append(
                        (0,0,{
                            'name': move_ref,
                            'account_id': downpayment_receivable.account_id.id,
                            'credit': l.debit,
                            'debit': 0,
                            'partner_id': invoice.partner_id.id,
                            'date_paid_fixed': l.date_paid,
                            'bank_statement_line': l.bank_statement_line and l.bank_statement_line.id or False
                        })
                        )

                move = self.env['account.move'].create(move_dict)
                move.post()

                # reconcile
                # downpayment_receivable (invoice)
                # reserved_downpayment (bank statement)
                collect_and_reconcile(self,
                                      [downpayment_receivable, reserved_downpayment] + [l for l in move.line_id],
                                      account_ids=[downpayment_receivable.account_id.id, reserved_downpayment.account_id.id],
                                      partial=False,
                                      )

                # move balance on deposits awaiting ID to accounts receivable
                move = invoice.partner_id.book_balance(reserved_downpayment.account_id, downpayment_receivable.account_id, invoice.residual)
                
                if move:
                    lines = [l for l in move.line_id if l.name and l.name.startswith('dst Moving balance') and l.account_id.id == downpayment_receivable.account_id.id]
                    if lines:
                        for l in sorted([l for l in self.move_id.line_id if l.payplan_type in ('interest','principal','downpayment') and l.account_id.id == downpayment_receivable.account_id.id and not l.reconcile_id], key=lambda l:l.date_maturity):
                            if sum([l2.debit - l2.credit for l2 in lines]) >= 0:
                                break
                            lines.append(l)

                        collect_and_reconcile(self,
                                              lines,
                                              account_ids=[lines[0].account_id.id],
                                              partial=True
                                              )
        

    @api.multi
    def action_move_create(self):
        """Post advice to reconcile downpayment to validated payplan invoices"""
        for invoice in self:
            if invoice.is_credit and invoice.amount_downpayment:           
                if not invoice.sale_order.downpayment_line_id:
                    invoice.sale_order.reserve_downpayment()

            if not invoice.payplan_date and invoice.is_credit:
                if invoice.amount_downpayment:
                    invoice.payplan_date = invoice.sale_order.downpayment_line_id.date_paid_fixed or invoice.sale_order.downpayment_line_id.date_paid or invoice.sale_order.downpayment_line_id.date
                else:
                    invoice.payplan_date = fields.Date.context_today(self)
                
        super(account_invoice, self).action_move_create()
        self.book_downpayment()

    @api.model
    def _prepare_refund(self, invoice, date=None, period_id=None, description=None, journal_id=None):
        """ override to adjust refund for already paid amount """
        r = super(account_invoice, self)._prepare_refund(invoice, date=date, period_id=period_id, description=description, journal_id=journal_id)

        if 'open_balance' in self.env.context and self.env.context['open_balance']:
            # unset payment term
            r['payment_term'] = False
            r['is_credit'] = False

        if 'open_balance' in self.env.context and self.env.context['open_balance']:
            already_paid = invoice.amount_total - invoice.residual
            already_paid_principal = invoice.amount_total - invoice.amount_interest - invoice.residual_principal
            already_paid_interest = already_paid - already_paid_principal

            product_accounts = [
                m[2]['account_id'] for m in r['invoice_line']
                if 'product_id' in m[2] and m[2]['product_id'] and 'account_id' in m[2] and m[2]['account_id']
            ]
            # Removed the product_id conditions because migrated
            # account.invoice (payplans) do not have product_id. I don't know
            # if this has any complications
            if not product_accounts:
                product_accounts = [
                    m[2]['account_id'] for m in r['invoice_line']
                    if 'account_id' in m[2] and m[2]['account_id']
                ]

            if already_paid > 0:
                if already_paid_interest > 0:
                    interest_income_account_id = self.company_id.get_interest_income_account()
                    
                    r['invoice_line'].append((0,0,{
                        'type': 'dest',
                        'name': 'reschedule paid interest',
                        'price_unit': already_paid_interest,
                        'quantity': -1,
                        'product_id': self.company_id.interest_product_id and self.company_id.interest_product_id.id or False,
                        'account_id': interest_income_account_id
                    }))
                if already_paid_principal > 0:
                    r['invoice_line'].append((0,0,{
                        'type': 'dest',
                        'name': 'reschedule paid principal',
                        'price_unit': already_paid_principal,
                        'quantity': -1,
                        'account_id': product_accounts[0]
                    }))

        return r

    @api.model
    def rereconcile_all(self):
        # TODO check: where and when is this method executed? is it still required?
        self.env.cr.execute("""
            select distinct(id) from account_invoice where move_id in (
            select move_id from account_move_line 
            where reconcile_id is not null and reconcile_partial_id is not null and reconcile_id != reconcile_partial_id
            )""")
        self.browse([r[0] for r in self.env.cr.fetchall()]).action_rereconcile()
        return True

    @api.model
    def cron_rereconcile(self):
        """Find invoices with receivables which are not
        reconciled in order of date maturity
        and attempt to redo the reconciliation.
        """

        self.env.cr.execute("""
        select distinct(id)
        from (
            select distinct(id) from account_invoice where move_id in (
            select distinct(aml.move_id) from account_move_line aml
            left join account_move_line aml2 on (aml.move_id = aml2.move_id)
            where (aml.reconcile_id is not null
                   or aml.reconcile_partial_id is not null)
            and ((aml2.reconcile_id is null
                  and aml2.reconcile_partial_id is null)
            or (aml.reconcile_id is not null
                and aml2.reconcile_id is null)
            or (aml.reconcile_partial_id is not null
                and aml2.reconcile_partial_id is not null
                and aml.reconcile_partial_id != aml2.reconcile_partial_id)
            )
            and aml2.date_maturity < aml.date_maturity
            and aml.payplan_type in ('principal', 'interest')
            and aml2.payplan_type in ('principal', 'interest')
            )
        ) as t1 union (
            select distinct(id)
            from account_invoice
            where move_id in (
            select distinct(aml.move_id)
            from account_move_line aml
            inner join account_move_line aml2 on (aml.move_id = aml2.move_id)
            where aml2.date_maturity < aml.date_maturity
            and ((aml2.date_paid > aml.date_paid)
            or (aml2.date_paid is null and aml.date_paid is not null))
            and aml.payplan_type in ('principal', 'interest')
            and aml2.payplan_type in ('principal', 'interest')
        )
        )
        """)
        for invoice in self.browse(map(itemgetter(0), self.env.cr.fetchall())):
            try:
                _logger.info("Trying to rereconcile invoice id %d", invoice.id)
                invoice.rereconcile()
                self.env.cr.commit()
            except Exception:
                self.env.cr.rollback()
                _logger.error("Error rereconciling invoice %d", invoice.id)

        return True

    @api.multi
    def rereconcile(self):
        for invoice in self:
            own_lines = lines_to_reconcile = self.env['account.move.line']
            recs = self.env['account.move.reconcile']

            for line in invoice.move_id.line_id:
                if line.account_id != invoice.account_id:
                    continue
                if line.payplan_type not in ('principal', 'interest'):
                    continue

                if line.reconcile_id:
                    recs |= line.reconcile_id
                    lines_to_reconcile |= line.reconcile_id.line_id
                    own_lines += line
                if line.reconcile_partial_id:
                    recs |= line.reconcile_partial_id
                    lines_to_reconcile |= line.\
                        reconcile_partial_id.\
                        line_partial_ids
                    own_lines += line

            if not len(
                set(
                    filter(
                        None,
                        lines_to_reconcile.mapped('partner_id.id')))) == 1:
                _logger.error("Not reconciling invoice %d, because not "
                              "all the reconciled lines are for the same "
                              "partner!", invoice.id)
                continue

            invoices = self.env['account.invoice'].search([
                ('move_id',
                 'in',
                 list(set(lines_to_reconcile.mapped('move_id.id'))))])
            if not len(invoices) == 1:
                _logger.error("Not reconciling invoice %d, because not "
                              "all the reconciled lines are for the same "
                              "invoice!", invoice.id)
                continue

            the_lines = lines_to_reconcile - own_lines
            balance = sum([l.debit - l.credit for l in the_lines])
            for line in invoice.move_id.line_id.filtered(
                    lambda l: l.account_id == invoice.account_id
                    and l.payplan_type in ('principal', 'interest')
                    and l not in the_lines).sorted(
                    key=lambda l: l.date_maturity):

                if balance >= 0:
                    break
                the_lines += line
                balance += line.debit - line.credit

            if the_lines:
                if recs:
                    recs.unlink()
                the_lines.reconcile_partial()
                _logger.info("Rereconciled invoice %d", invoice.id)

        return True

    @api.multi
    def action_rereconcile(self):
        for invoice in self:
            lines = []
            reconcile_ids = []
            total = [0.0, 0.0]

            for line in invoice.move_id.line_id:
                if line.account_id.reconcile:
                    if line.reconcile_id:
                        reconcile_ids.append(line.reconcile_id)
                        for line2 in line.reconcile_id.line_id:
                            if line2 not in lines:
                                lines.append(line2)
                                total[0] += line2.debit
                                total[1] += line2.credit
                    if line.reconcile_partial_id:
                        reconcile_ids.append(line.reconcile_partial_id)
                        for line2 in line.reconcile_partial_id.line_partial_ids:
                            if line2 not in lines:
                                lines.append(line2)
                                total[0] += line2.debit
                                total[1] += line2.credit

            if total[1] > total[0]:
                # credit > debit: add debit receivables
                leftover_credit = total[1] - total[0]
                other_lines = invoice.move_id.line_id.filtered(lambda l: l not in lines and l.debit > 0 and l.account_id.reconcile).sorted(lambda l: l.date_maturity)
                for l in other_lines:
                    lines.append(l)
                    leftover_credit -= l.debit
                    if leftover_credit <= 0:
                        break
            for rec in list(set(reconcile_ids)):
                rec.unlink()
            self.env['account.move.line'].browse([l.id for l in lines]).reconcile_partial()
        return True

    @api.multi
    def compute_payplan_summary(self):
        preview = {
            'total_interest': 0,
            'total_principal': 0,
            'total_amount': 0,
            'total_downpayment': 0,
            'is_preview': False,
            'installments': []
        }

        if not self.move_id:
            return preview

        # Payplan Summary
        installments = {}
        for l in self.move_id.line_id:
            if l.payplan_type not in ('downpayment', 'principal', 'interest'):
                continue

            installments.setdefault(l.date_maturity, {'principal': 0, 'interest': 0, 'downpayment': 0, 'total': 0, 'orig_date_maturity': l.orig_date_maturity, 'date_paid': l.date_paid})
            installments[l.date_maturity][l.payplan_type] += l.debit - l.credit
            installments[l.date_maturity]['total'] += l.debit - l.credit

            preview['total_' + l.payplan_type] += l.debit - l.credit
            preview['total_amount'] += l.debit - l.credit

        l = [[k, v['principal'], v['interest'], v['downpayment'], v['total'], v['orig_date_maturity'], v['date_paid']] for k,v in installments.iteritems()]
        l.sort(key=lambda l: l[0])        

        preview['installments'] = l
        return preview


class account_payment_term(models.Model):
    _inherit = 'account.payment.term'

    is_credit = fields.Boolean("Payplan term")

    deposit = fields.Integer('Deposit percentage')
    installments = fields.Integer('Number of installments')

    installment_interval = fields.Integer('Installment interval')
    installment_interval_unit = fields.Selection([
        #('day', 'Days'),
        #('week', 'Week'),
        ('month', 'Month'),
        #('year', 'Year')
    ], string="Installment interval unit")
    
    grace_period = fields.Integer('Grace period')
    grace_period_unit = fields.Selection([
        #('day', 'Days'),
        #('week', 'Week'),
        ('month', 'Month'),
        #('year', 'Year')
    ], string="Grace period unit")

    @api.model
    def compute_interest(self, **kwargs):
        if not kwargs['is_credit']:
            return 0
        return sum(map(lambda t: t[1], self.compute_payments(**kwargs))) - kwargs['amount']

    @api.model
    def compute_downpayment(self, **kwargs):
        if not kwargs['is_credit']:
            return 0
        if kwargs['downpayment_percentage'] > 100:
            return kwargs['downpayment_percentage']

        return kwargs['currency'].conventional_round(kwargs['amount'] * kwargs['downpayment_percentage'] / 100)

    @api.model
    def compute_preview(self,
                        is_credit=True,
                        amount=0, 
                        downpayment_percentage=0,
                        number_of_installments=0,
                        yearly_interest_rate=0, 
                        installment_interval=0,
                        installment_interval_unit='month',
                        grace_period=0,
                        grace_period_unit='month',
                        date_ref=False,
                        stock_given_out_date=False,
                        is_reschedule=False,
                        currency=False,
                        adjustments={}
                    ):
        installment_lines = self.compute_payments(
            is_credit=is_credit,
            amount=amount,
            downpayment_percentage=downpayment_percentage,
            number_of_installments=number_of_installments,
            yearly_interest_rate=yearly_interest_rate,
            installment_interval=installment_interval,
            installment_interval_unit=installment_interval_unit,
            grace_period=grace_period,
            grace_period_unit=grace_period_unit,
            date_ref=date_ref,
            stock_given_out_date=stock_given_out_date,
            is_reschedule=is_reschedule,
            currency=currency,
            adjustments=adjustments
        )
        preview = {
            'total_interest': 0,
            'total_principal': 0,
            'total_amount': 0,
            'total_downpayment': 0,
            'is_preview': False,
            'installments': []
        }
        # Payplan Preview
        installments = {}
        for l in installment_lines:
            installments.setdefault(l[0], {'principal': 0, 'interest': 0, 'downpayment': 0, 'total': 0})
            installments[l[0]][l[2]] += l[1]
            installments[l[0]]['total'] += l[1]

            preview['total_' + l[2]] += l[1]
            preview['total_amount'] += l[1]

        l = [[k, v['principal'], v['interest'], v['downpayment'], v['total']] for k,v in installments.iteritems()]
        l.sort(key=lambda l: l[0])        

        preview['installments'] = l
        return preview

    @api.model
    def compute_payments(self,
                         is_credit=True,
                         amount=0, 
                         downpayment_percentage=0,
                         number_of_installments=0,
                         yearly_interest_rate=0, 
                         installment_interval=0,
                         installment_interval_unit='month',
                         grace_period=0,
                         grace_period_unit='month',
                         date_ref=False,
                         stock_given_out_date=False,
                         is_reschedule=False,
                         currency=False,
                         adjustments={}
                    ):
        if not is_credit:
            return []

        if not currency:
            currency=self.env.user.company_id.currency_id

        if not date_ref:
            date_ref = datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)
        start_date = datetime.strptime(date_ref, DEFAULT_SERVER_DATE_FORMAT)

        if not stock_given_out_date:
            stock_given_out_date = date_ref
        lag_days = self.env.user.company_id.lag_days
        install_date = datetime.strptime(stock_given_out_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(days=lag_days)

        downpayment = self.compute_downpayment(is_credit=is_credit, amount=amount, downpayment_percentage=downpayment_percentage, currency=currency)

        period_interest_rate = 0.0
        if yearly_interest_rate:
            if installment_interval_unit == 'month' or not installment_interval_unit:
                period_interest_rate = yearly_interest_rate / 12 * installment_interval
            elif installment_interval_unit == 'year':
                period_interest_rate = yearly_interest_rate * installment_interval
            elif installment_interval_unit == 'week':
                period_interest_rate = yearly_interest_rate / 52 * installment_interval
            elif installment_interval_unit == 'day':
                period_interest_rate = yearly_interest_rate / 365 * installment_interval                
            else:
                raise Exception(
                    "Installment interval unit '{}' is unknown".format(
                        installment_interval_unit
                    )
                )

        principal = running_total = amount - downpayment
        if running_total > 0 and number_of_installments == 0:
            number_of_installments = 1        
        installment = -1 * currency.conventional_round(numpy.pmt(period_interest_rate, number_of_installments, principal), rounding_method="UP")

        result = []
        if downpayment:
            result.append((start_date.strftime(DEFAULT_SERVER_DATE_FORMAT), downpayment, 'downpayment'))
            
        grace_delta = relativedelta(
            months=grace_period_unit=='month' and grace_period or 0,
            years=grace_period_unit=='year' and grace_period or 0,
            weeks=grace_period_unit=='week' and grace_period or 0,
            days=grace_period_unit=='day' and grace_period or 0,                                          
            )
        installment_delta = relativedelta(
            months=installment_interval_unit=='month' and installment_interval or 0,
            years=installment_interval_unit=='year' and installment_interval or 0,
            weeks=installment_interval_unit=='week' and installment_interval or 0,
            days=installment_interval_unit=='day' and installment_interval or 0,
            )

        # adjustments to repayment schedule
        adjustments = dict([(k,v) for k,v in adjustments.iteritems() if k > 0 and k <= number_of_installments])
        adjusted_periods = len(adjustments.keys())
        adjustment_extra = sum(adjustments.values()) - adjusted_periods
        reduction = 0.0
        if number_of_installments > 0 and adjusted_periods < number_of_installments:
            reduction = currency.conventional_round((adjustment_extra * installment) / (number_of_installments - adjusted_periods))

        for i in range(number_of_installments):
            installment_date = util.add_relativedelta(install_date, grace_delta + (i * installment_delta))
            if period_interest_rate == 0.0:
                principal_amount = installment
            else:
                principal_amount = -1 * currency.conventional_round(numpy.ppmt(period_interest_rate, i+1, number_of_installments, principal), rounding_method="DOWN")
            interest_amount = 0 
            
            # ADAPTED TO MATCH EXCEL
            if i + 1 == number_of_installments:
                principal_amount = running_total + reduction
            if i + 1 == number_of_installments and i + 1 in adjustments:
                principal_amount = installment + (running_total - adjustments[i+1] * installment)
                
            if principal_amount != installment:
                interest_amount = currency.conventional_round(installment - principal_amount)
                result.append([installment_date.strftime(DEFAULT_SERVER_DATE_FORMAT), interest_amount, 'interest'])

            if reduction:
                if i+1 in adjustments:
                    principal_amount = installment * adjustments[i+1] - interest_amount
                else:
                    principal_amount = installment - reduction - interest_amount

            result.append([installment_date.strftime(DEFAULT_SERVER_DATE_FORMAT), min(running_total, principal_amount), 'principal'])
            running_total -= min(running_total, principal_amount)

        if running_total:
            _logger.info('computation had left over running_total %f', running_total)
        #    result.append([installment_date.strftime(DEFAULT_SERVER_DATE_FORMAT), currency.round(running_total), 'principal'])
        
        return result


class account_move_line(models.Model):
    _inherit = 'account.move.line'

    @api.multi
    def _compute_bank_details(self):
        for line in self.sudo():
            bs = line.bank_statement_line
            if not bs:
                bs = self.env['account.bank.statement.line'].sudo().search([
                    ('journal_entry_id','=',line.move_id.id)
                ], limit=1)

            if bs:
                line.bank_statement_date = bs.date
                line.bank_statement_amount = bs.amount
                line.bank_statement_ref = bs.journal_entry_id.name
            else:
                line.bank_statement_amount = 0

    @api.multi
    @api.depends('account_id.reconcile',
                 'payplan_type',
                 'date_paid_fixed',
                 'date',
                 'date_maturity',
                 'reconcile_id.line_id',
                 'reconcile_partial_id.line_partial_ids',
                 )
    def _compute_payplan(self):
        for l in self:
            if not l.account_id.reconcile:
                continue

            if not l.payplan_type:
                l.date_paid = l.date_paid_fixed or (l.bank_statement_line and l.bank_statement_line.date) or l.date
                continue

            date_paid = l.date_paid_fixed
            if not l.date_maturity:
                l.installment_state = 'na'
                l.open_amount = 0

            if l.reconcile_id:
                # compute date paid if fully reconciled, fixed_date_paid if filled, otherwise max of credit receivalbe
                date_paid = date_paid or util.compute_date_paid(l.reconcile_id.line_id, l)[0]
                l.open_amount = 0
                if l.date_maturity >= date_paid:
                    l.installment_state = 'paid'
                else:
                    l.installment_state = 'late'
            elif l.reconcile_partial_id:
                date_paid, l.open_amount = util.compute_date_paid(l.reconcile_partial_id.line_partial_ids, l)
                if not l.open_amount and l.date_maturity >= date_paid:
                    l.installment_state = 'paid'
                elif not l.open_amount:
                    l.installment_state = 'late'
                elif l.date_maturity < datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT):
                    l.installment_state = 'overdue'
                else:
                    l.installment_state = 'open'
            else:
                l.open_amount = l.debit - l.credit
                if l.date_maturity < datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT):
                    l.installment_state = 'overdue'
                else:
                    l.installment_state = 'open'
            l.date_paid = l.date_paid_fixed or date_paid

    @api.multi
    def _compute_days_overdue(self):
        for line in self:
            line.days_overdue = None
            if not line.date_paid and line.date_maturity:
                due_date = datetime.strptime(line.date_maturity,
                                             DEFAULT_SERVER_DATE_FORMAT)
                line.days_overdue = (due_date - datetime.today()).days

    bank_statement_line = fields.Many2one('account.bank.statement.line', string="Original bank statement line", help="Used internally to keep track of bank statement line to show payment details on the payments tab of invoice in case the bank statement was not posted directly on a/r")
    bank_statement_date = fields.Date('Payment Date', compute="_compute_bank_details")
    bank_statement_amount = fields.Float('Payment Amount', compute="_compute_bank_details")
    bank_statement_ref = fields.Char('Payment ref', size=128, compute="_compute_bank_details")

    amount = fields.Float("Amount", compute="_compute_amount", store=True)

    payplan_type = fields.Selection([('downpayment','Downpayment'),('principal','Principal'),('interest','Interest'),('subsidy','Subsidy')], string='Payplan installment type')

    installment_state = fields.Selection([('na',''),('open','Open'),('paid','Paid'),('late','Late'),('overdue','Overdue')], compute='_compute_payplan', store=True)#, search='_search_payplan')
    open_amount = fields.Float('Residual', compute='_compute_payplan', store=True)
    date_paid = fields.Date("Date Paid", compute='_compute_payplan', help="Date paid for an installment (payplan_type set) is the date of the reconciled payment.", store=True)

    date_paid_fixed = fields.Date("Date Paid (fixed, internal use)")
    reschedule_refund = fields.Many2one('account.invoice', string="Reschedule refund")

    days_overdue = fields.Integer('Days Till Next Payment / Days Overdue', compute="_compute_days_overdue")
    date_last_payment = fields.Date('Date Last Payment', related="partner_id.date_last_payment", readonly=True)

    orig_date_maturity = fields.Date("Original Due Date")

    @api.model
    def get_bank_statement_line(self, account_move_line):
        if account_move_line.bank_statement_line:
            return account_move_line.bank_statement_line

        line = self.env['account.bank.statement.line'].search([
            ('journal_entry_id','=',account_move_line.move_id.id)
        ], limit=1)
        if line:
            return line
        return False

    @api.multi
    @api.depends('debit','credit')
    def _compute_amount(self):
        for line in self:
            line.amount = line.debit - line.credit

    @api.model
    def create(self, values):
        if 'date_maturity' in values:
            if 'payplan_type' in values and values['payplan_type'] in ('downpayment', 'principal', 'interest'):
                values['orig_date_maturity'] = values['date_maturity']
        return super(account_move_line, self).create(values)

    # keep as old api !!!
    def write(self, cr, uid, ids, vals, context=None, check=True, update_check=True):
        if context is None:
            context={}

        if isinstance(ids, (int, long)):
            ids = [ids]

        if 'date_maturity' in vals:
            for line in self.browse(cr, uid, ids):
                if line.payplan_type in ('downpayment', 'principal', 'interest'):
                    if not line.orig_date_maturity:
                        vals['orig_date_maturity'] = line.date_maturity
                        break
        result = super(account_move_line, self).write(cr, uid, ids, vals, context, check, update_check)

        return result
