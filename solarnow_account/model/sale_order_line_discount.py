# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, api


class sale_order_line_discount(models.Model):
    _inherit = "sale.order.line.discount"

    @api.multi
    def _is_applicable(self, order_line):
        return super(sale_order_line_discount, self)._is_applicable(order_line) and not order_line.is_interest_line

    @api.multi
    def _compute_discount(self, base, qty, order):
        return order.currency_id.conventional_round(super(sale_order_line_discount, self)._compute_discount(base, qty, order))
