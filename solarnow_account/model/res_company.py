# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2016 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp


class res_company(models.Model):
    _inherit = "res.company"

    #income_interest_account_id = fields.Many2one('account.account', 'Interest income account', domain=[('type','=','other')])
    interest_product_id = fields.Many2one('product.product', "Interest product", domain=[('type','=','service')])
    receivable_interest_account_id = fields.Many2one('account.account', 'Receivable income account', domain=[('type','=','receivable')])
    payplan_writeoff_account_id = fields.Many2one('account.account', 'Writeoff account', domain=[('type','=','other')])
    downpayment_account_id = fields.Many2one('account.account', 'Downpayment account', domain=[('type','=','other')])
    downpayment_journal_id = fields.Many2one('account.journal', 'Downpayment journal')
    discount_journal_id = fields.Many2one('account.journal', 'Discount journal')

    reschedule_fee = fields.Float(string="Reschedule Fee", digits=dp.get_precision('Account'))
    paynow_fee = fields.Float(string="Paynow Fee", digits=dp.get_precision('Account'))
    interest_rate = fields.Float("Interest", digits=(1, 8))

    lag_days = fields.Integer()

    @api.multi
    def get_interest_income_account(self):
        account_id = False
        if self.interest_product_id and self.interest_product_id.property_account_income:
            account_id = self.interest_product_id.property_account_income.id
        elif self.interest_product_id and self.interest_product_id.categ_id and self.interest_product_id.categ_id.property_account_income_categ:
            account_id = self.interest_product_id.categ_id.property_account_income_categ.id

        return account_id

    _defaults = {
        'lag_days': 3,
    }
