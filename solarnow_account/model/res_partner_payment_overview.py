# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.sql import drop_view_if_exists


class res_partner_payment_overview(models.Model):
    _name = 'res.partner.payment.overview'
    _auto = False

    ref = fields.Char("Contact reference", size=128, readonly=True)
    name = fields.Char("Name", size=128, readonly=True)
    partner_id = fields.Many2one("res.partner", 'Partner', readonly=True)
    paid = fields.Float("Paid", readonly=True)
    owed = fields.Float("Owed", readonly=True)
    currently_owed = fields.Float("Currently owed", readonly=True)

    def init(self, cr):
        drop_view_if_exists(cr, 'res_partner_payment_overview')
        cr.execute("""
            create or replace view res_partner_payment_overview as (
            select res_partner.id as id, ref, name, res_partner.id as partner_id, paid as paid1, paid2 as paid, owed, owed -paid2 as currently_owed, paid - paid2
            from
            (
            select partner_id, sum(debit) owed
            from account_move_line where move_id in
            (
            select move_id from account_invoice
            where sale_order in (
            select id from sale_order
            where procurement_group_id is not null
            )
            )
            and payplan_type in ('principal', 'interest', 'downpayment')
            and date_maturity < now()
            group by partner_id
            ) as owed_amounts
            left join
            (
            select partner_id, sum(credit - debit) paid2 from account_move_line
            inner join account_journal on (journal_id = account_journal.id)
            where ((account_journal.type = 'bank' or account_move_line.name='Write-Off')
            and account_id in (4920, 4915))
            group by partner_id
            ) as paid_amounts2 on (owed_amounts.partner_id = paid_amounts2.partner_id)
            left join
            (
            select partner_id, sum(amount) paid from account_bank_statement_line
            group by partner_id
            ) as paid_amounts on (owed_amounts.partner_id = paid_amounts.partner_id)
            inner join res_partner on (paid_amounts.partner_id = res_partner.id)
            order by paid2 - owed)
         """)
