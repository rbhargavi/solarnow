# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp import models, fields, api, _, SUPERUSER_ID
from openerp.exceptions import Warning
from openerp.tools import float_is_zero
from util import proportion
from datetime import date
import json

_logger = logging.getLogger(__name__)


class account_bank_statement_line(models.Model):
    _inherit = 'account.bank.statement.line'
    _order = 'date desc'

    partner_id_ref = fields.Char(string="Cust ref",
        related='partner_id.ref', readonly = True)    
    partner_id_phone = fields.Char(string="Phone",
        related='partner_id.phone', readonly = True) 

    manual_processing = fields.Boolean('Manual')

    payplan_reconciliation = fields.Text(compute='guess_payplan_reconciliation', string='Payplan reconciliation', store=True)
    payplan_reconciliation_summary = fields.Text(compute='guess_payplan_reconciliation', string='Payplan', store=True)
    account_move_line_ids = fields.One2many('account.move.line', 'bank_statement_line', string='Journal Items')

    @api.model
    def create(self, vals):
        if vals['partner_id']:
            partner = self.env['res.partner'].browse(vals['partner_id'])
            for invoice in partner.invoice_ids:
                if invoice.is_credit and invoice.state == 'open' and not invoice.paynow_date:
                    if vals['amount'] >= invoice.paynow_amount:
                        invoice.write({'paynow_date': date.today()})
            if not vals.get('company_id', False):
                vals.update({'company_id': partner.company_id.id})

        r = super(account_bank_statement_line, self).create(vals)
        return r

    @api.multi
    @api.depends('partner_id', 'amount')
    def guess_payplan_reconciliation(self):
        cache_per_partner = {}
        for st_line in self:
            if st_line.journal_entry_id:
                continue
            if not st_line.partner_id:
                st_line.payplan_reconciliation_summary = ''
                st_line.payplan_reconciliation = ''
                continue
            if not st_line.company_id:
                st_line.payplan_reconciliation_summary = ''
                st_line.payplan_reconciliation = ''
                continue

            vals = []
            amount = st_line.amount

            if not st_line.company_id.downpayment_account_id:
                raise Warning(_('Error!'), _('Downpayment Account not defined for this Company.'))
            downpayment_account_id = st_line.company_id.downpayment_account_id.id

            # replentish downpayment account
            # TODO
            if st_line.amount> 0 and st_line.partner_id and st_line.partner_id.customer:
                summary = []
                line_vals = {}

                open_downpayments = st_line.partner_id.find_open_downpayments()

                # rematch unlinked downpayments
                for l in st_line.partner_id.account_move_line_ids.filtered(lambda
                                                                r: r.account_id.id == downpayment_account_id
                                                                and r.credit == 0
                                                                and r.bank_statement_line.id == st_line.id):
                    vals.append({
                        'debit': 0.0,
                        'credit': l.debit,
                        'account_id': downpayment_account_id,
                        'name': st_line.name,
                        'counterpart_move_line_id': l.id
                    })
                    summary.append("Rematch Reserved Downpayment")
                    amount -= amount
                
                # find open installments.
                if st_line.partner_id.id not in cache_per_partner:
                    line_list = []
                    for account_move_line in st_line.partner_id.account_move_line_ids:
                        if not account_move_line.reconcile_id \
                            and account_move_line.payplan_type in ['principal', 'interest'] \
                            and account_move_line.state == 'valid' \
                            and account_move_line.open_amount != 0:

                            line_list += account_move_line

                    line_list.sort(key=lambda l:l.date_maturity)
                    cache_per_partner[st_line.partner_id.id] = line_list

                installments = cache_per_partner.get(st_line.partner_id.id, [])

                # first, split amount over overdue installments
                installments_by_move = {}
                totals_by_move = {}
                while len(installments) and installments[0].date_maturity < st_line.date:
                    installment = installments.pop(0)
                    installments_by_move.setdefault(installment.move_id.id, []).append(installment)
                    totals_by_move.setdefault(installment.move_id.id, 0)
                    totals_by_move[installment.move_id.id] += installment.open_amount

                for k, total in zip(totals_by_move.keys(), proportion(totals_by_move.values(), amount)):
                    total = st_line.statement_id.currency.round(total)

                    paid_amount = min(amount, total)

                    for installment in installments_by_move[k]:
                        paid_installment_amount = min(paid_amount, installment.open_amount)
                        if paid_installment_amount > 0:
                            vals.append({
                            'debit': 0.0,
                            'credit': paid_installment_amount,
                            'account_id': installment.account_id.id,
                            'name': st_line.name,
                            'counterpart_move_line_id': installment.id
                            })
                            summary.append("Overdue installment of %s" % (installment.move_id.name,))
                        paid_amount -= paid_installment_amount
                        amount -= paid_installment_amount
 
                for so in open_downpayments:
                    # check balance on downpayment account (negative)

                    reserved_lines = []
                    for sale_order in st_line.partner_id.sale_order_ids:
                        if sale_order.downpayment_line_id:
                            reserved_lines.append(sale_order.downpayment_line_id.id)

                    account_move_line_ids = []
                    for line in st_line.partner_id.account_move_line_ids:
                        if line.account_id and line.account_id.id == downpayment_account_id:
                            if line.state == 'valid' and not line.reconcile_id:
                                if line.id not in reserved_lines:
                                    account_move_line_ids += line

                    balance = reduce(lambda x,y: x+y, map(lambda t: t[0]-t[1], [(l.debit, l.credit) for l in account_move_line_ids]), 0)

                    if so.amount_downpayment + balance <= 0:
                        continue

                    if amount >= so.amount_downpayment + balance:
                        # if amount is enough to fully pay downpayment

                        amount_paid = min(amount, so.amount_downpayment + balance)
                        vals.append({
                            'debit': 0.0,
                            'credit': amount_paid,
                            'account_id': downpayment_account_id,
                            'name': st_line.name,
                            'downpayment_so': so.id
                        })
                        amount -= amount_paid
                        summary.append("Downpayment of %s" % (so.name,))
                        
                # next, fill open installments by date_maturity if there is no open downpayment
                if not open_downpayments:
                    while amount > 0 and not float_is_zero(amount, precision_rounding=0.01) and len(installments):
                        installment = installments.pop(0)

                        payment_amount = min(amount, installment.open_amount)
                        vals.append({
                            'debit': 0.0,
                            'credit': payment_amount,
                            'account_id': installment.account_id.id,
                            'name': st_line.name,
                            'counterpart_move_line_id': installment.id
                        })
                        amount -= payment_amount
                        if amount == installment.debit:
                            summary.append('Installment of %s' % (installment.move_id.name))
                        elif amount == installment.open_amount:
                            summary.append('Leftover Installment of %s' % (installment.move_id.name))
                        else:
                            summary.append('Partial installment of %s' % (installment.move_id.name))
 
                # last, process left over amount
                if not float_is_zero(amount, precision_rounding=0.01):
                    vals.append({
                        'debit': 0.0,
                        'credit': amount,
                        'account_id': downpayment_account_id,
                        'name': st_line.name,
                    })                    

                    downpayments = [so.name 
                                    for so in open_downpayments
                                    if so.id not in [v.get('downpayment_so', False) for v in vals]]
                    if downpayments:
                        summary.append("Partial downpayment of %s" % (downpayments[0], ))
                    else:
                        summary.append("Leftover amount %0.2f" % (amount, ))

                st_line.payplan_reconciliation_summary = '\n'.join(summary)
                st_line.payplan_reconciliation = json.dumps(vals)

    @api.multi
    def process(self):
        sale_obj = self.pool.get('sale.order')
        bank_statement_line_obj = self.pool.get('account.bank.statement.line')
        MoveLine = self.env['account.move.line']

        for st_line in self:
            if st_line.manual_processing:
                continue

            if st_line.journal_entry_id.id:
                continue

            if not st_line.payplan_reconciliation or not json.loads(st_line.payplan_reconciliation):
                continue

            payplan_reconciliation = json.loads(st_line.payplan_reconciliation)

            downpayment_so_list = []
            have_all_counterpart_move_lines = True
            all_valid = True
            counterpart_move_line_list = []
            for aml in payplan_reconciliation:
                if not aml.get('counterpart_move_line_id', False):
                    have_all_counterpart_move_lines = False
                else:
                    counterpart_move_line_list.append(aml['counterpart_move_line_id'])
                if 'downpayment_so' in aml and aml['downpayment_so']:
                    downpayment_so_list.append(aml['downpayment_so'])
            if have_all_counterpart_move_lines and counterpart_move_line_list:
                counterpart_move_lines = MoveLine.browse(counterpart_move_line_list)
                for line in counterpart_move_lines:
                    if line.state != 'valid':
                        all_valid = False
                        break

            if not have_all_counterpart_move_lines or all_valid:
                bank_statement_line_obj.process_reconciliation(self.env.cr, self.env.uid, st_line.id, payplan_reconciliation, context=self.env.context)

                # used old api to avoid errors (see task #6384)
                sale_obj.reserve_downpayment(self._cr, SUPERUSER_ID, downpayment_so_list)
                for sale_data in sale_obj.browse(self._cr, SUPERUSER_ID, downpayment_so_list):
                    if sale_data.downpayment_line_id and sale_data.state == 'approved':
                        sale_obj.signal_workflow(self._cr, SUPERUSER_ID, [sale_data.id], 'ship')

            else:
                st_line.manual_processing = True
                _logger.debug("Statement line: ID(%(st_line)s) has been set to manual processing because move line was not valid!" % {'st_line': st_line})
