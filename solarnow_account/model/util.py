# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import math
from openerp.tools.float_utils import _float_check_precision
from operator import attrgetter
from dateutil.relativedelta import relativedelta

def float_round(value, precision_digits=None, precision_rounding=None, rounding_method='HALF-UP'):
    """Return ``value`` rounded to ``precision_digits`` decimal digits,
       minimizing IEEE-754 floating point representation errors, and applying
       the tie-breaking rule selected with ``rounding_method``, by default
       HALF-UP (away from zero).
       Precision must be given by ``precision_digits`` or ``precision_rounding``,
       not both!

       COPIED FROM openerp/tools/float_util.py, because it doesn't support rounding_method="DOWN"


       :param float value: the value to round
       :param int precision_digits: number of fractional digits to round to.
       :param float precision_rounding: decimal number representing the minimum
           non-zero value at the desired precision (for example, 0.01 for a 
           2-digit precision).
       :param rounding_method: the rounding method used: 'HALF-UP' or 'UP', the first
           one rounding up to the closest number with the rule that number>=0.5 is 
           rounded up to 1, and the latest one always rounding up.
       :return: rounded float
    """
    rounding_factor = _float_check_precision(precision_digits=precision_digits,
                                             precision_rounding=precision_rounding)
    if rounding_factor == 0 or value == 0: return 0.0

    # NORMALIZE - ROUND - DENORMALIZE
    # In order to easily support rounding to arbitrary 'steps' (e.g. coin values),
    # we normalize the value before rounding it as an integer, and de-normalize
    # after rounding: e.g. float_round(1.3, precision_rounding=.5) == 1.5

    # TIE-BREAKING: HALF-UP (for normal rounding)
    # We want to apply HALF-UP tie-breaking rules, i.e. 0.5 rounds away from 0.
    # Due to IEE754 float/double representation limits, the approximation of the
    # real value may be slightly below the tie limit, resulting in an error of
    # 1 unit in the last place (ulp) after rounding.
    # For example 2.675 == 2.6749999999999998.
    # To correct this, we add a very small epsilon value, scaled to the
    # the order of magnitude of the value, to tip the tie-break in the right
    # direction.
    # Credit: discussion with OpenERP community members on bug 882036

    normalized_value = value / rounding_factor # normalize
    epsilon_magnitude = math.log(abs(normalized_value), 2)
    epsilon = 2**(epsilon_magnitude-53)
    if rounding_method == 'HALF-UP':
        normalized_value += cmp(normalized_value,0) * epsilon
        rounded_value = round(normalized_value) # round to integer

    # TIE-BREAKING: UP (for ceiling operations)
    # When rounding the value up, we instead subtract the epsilon value
    # as the the approximation of the real value may be slightly *above* the
    # tie limit, this would result in incorrectly rounding up to the next number
    # The math.ceil operation is applied on the absolute value in order to
    # round "away from zero" and not "towards infinity", then the sign is
    # restored.

    elif rounding_method == 'UP':
        sign = cmp(normalized_value, 0)
        normalized_value -= sign*epsilon
        rounded_value = math.ceil(abs(normalized_value))*sign # ceil to integer

    elif rounding_method == 'DOWN':
        sign = cmp(normalized_value, 0)
        normalized_value -= sign*epsilon
        rounded_value = math.floor(abs(normalized_value))*sign # ceil to integer
        
    result = rounded_value * rounding_factor # de-normalize
    return result

def proportion(values, total):
    sum_values = sum(values)
    new_values = []
    acc = 0

    if not values:
        return new_values
    if not sum(values):
        return [0 for v in values]

    for v in values:
        q, r = divmod(v * total, sum_values)
        if acc + r < sum_values:
            acc += r
        else:
            if acc > r:
                new_values[-1] += 1
            else:
                q += 1
            acc -= sum_values - r
        new_values.append(q)
    return new_values

from openerp.addons.solarnow_base.model import util as util2
collect_and_reconcile = util2.collect_and_reconcile

def compute_date_paid(move_lines, move_line):
    """Compute date paid and paid amount for move_line, based on move_lines

    move_line should be in move_lines, move_lines are all lines reconciled or partially reconciled.
    """
    s_move_lines = list(move_lines)
    s_move_lines.sort(key=lambda y: ((y.payplan_type in ('downpayment', 'interest', 'principal') and y.date_maturity) or y.date_paid_fixed or (y.bank_statement_line and y.bank_statement_line.date) or y.date, y.id))

    credit_history = [[False, 0.0]]
    debit_running_total = 0
    seen = False
    for line in s_move_lines:
        if line.credit:
            if credit_history[-1][0] != (line.date_paid_fixed or (line.bank_statement_line and line.bank_statement_line.date) or line.date):
                credit_history.append([(line.date_paid_fixed or (line.bank_statement_line and line.bank_statement_line.date) or line.date), credit_history[-1][1]])
            credit_history[-1][1] += line.credit
        elif not seen and line.debit:
            if line.id == move_line.id:
                seen = True
            else:
                debit_running_total += line.debit
                            
    if seen:
        if debit_running_total > credit_history[-1][1]:
            return False, move_line.debit
        elif debit_running_total + move_line.debit > credit_history[-1][1]:
            return False, (debit_running_total + move_line.debit) - credit_history[-1][1]
        else:
            for l in credit_history:
                if l[1] >= debit_running_total + move_line.debit:
                    return l[0], 0

    return False, move_line.debit

def collect_due_amounts():
    """Find due installments"""
    pass

def collect_received_amount(self, partner_id, amount, allow_less=True):
    """Find credit receivable amounts for partner"""
    pass

def add_relativedelta(dt, rd):
    """Add relativedelta to datetime, but adjust to last day of month 
       if start date is last day of month"""
    r = dt + rd
    if (dt.month != (dt + relativedelta(days=1)).month):
        r = r + relativedelta(months=1) + relativedelta(days=-1)
    return r
