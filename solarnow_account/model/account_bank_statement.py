# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, api, _
from openerp.exceptions import Warning


class account_bank_statement(models.Model):
    _inherit = 'account.bank.statement'

    @api.multi
    def retry_automatic_reconciliation(self):
        for st in self:
            st.line_ids.guess_payplan_reconciliation()
        return True

    @api.multi
    def automatic_confirm(self):
        """Automatic reconciliation of installments"""

        for st in self:
            j_type = st.journal_id.type
            if not self.check_status_condition(st.state, journal_type=j_type):
                continue

            st.balance_check(st.id, journal_type=j_type)
            if (not st.journal_id.default_credit_account_id) \
                    or (not st.journal_id.default_debit_account_id):
                raise Warning(_('Configuration Error!'), _('Please verify that an account is defined in the journal.'))
            for line in st.move_line_ids:
                if line.state != 'valid':
                    raise Warning(_('Error!'), _('The account entries lines are not in valid state.'))

            # process automatic reconciliations
            st.line_ids.process()

        return {}

    @api.multi
    def button_confirm_bank(self):
        """After posting, reserve downpayments"""
        r = super(account_bank_statement, self).button_confirm_bank()
        for st in self:
            st.line_ids.process()
        self.env.cr.commit()
        involved_partner_ids = tuple(st.id for st in self)
        self.env['res.partner']._compute_portfolio(involved_partner_ids)

        return r

    def test_compute_balance_end_real(self, cr, uid, journal_id, context=None):
        """Added for remote access by unit test only"""
        return self._compute_balance_end_real(cr, uid, journal_id, context=context)
