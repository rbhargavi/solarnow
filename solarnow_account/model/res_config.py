# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
                

class account_config_settings(models.TransientModel):
    _inherit = 'account.config.settings'

    interest_product_id = fields.Many2one(
        'product.product',
        related='company_id.interest_product_id',
        string="Interest Product"
        )

    receivable_interest_account_id = fields.Many2one(
        'account.account',
        related='company_id.receivable_interest_account_id',
        string="Interest Receivable Account")

    payplan_writeoff_account_id = fields.Many2one(
        'account.account',
        related='company_id.payplan_writeoff_account_id',
        string="Writeoff Account")

    downpayment_account_id = fields.Many2one(
        'account.account',
        related='company_id.downpayment_account_id',
        string="Downpayment Account")

    downpayment_journal_id = fields.Many2one(
        'account.journal', 
        related='company_id.downpayment_journal_id',
        string='Downpayment journal')

    reschedule_fee = fields.Float(
        related='company_id.reschedule_fee',
        string='Reschedule Fee')

    paynow_fee = fields.Float(
        related='company_id.paynow_fee',
        string='Paynow Fee')

    # FIXME: conflicts with sale settings from sale_stock
    default_order_policy = fields.Selection(
        [('solarnow', 'Invoice and deliver after approval and downpayment (SN)'), ('manual', 'Invoice based on sales orders'), ('picking', 'Invoice based on deliveries')],
        'The default invoicing method is', default_model='sale.order',
        help="You can generate invoices based on sales orders or based on shippings.")

    lag_days = fields.Integer(
        related='company_id.lag_days',
        help="Lag days between 'stock given out' date and installation days"
    )

    _defaults = {
        'default_order_policy': 'solarnow',
    }

    discount_journal_id = fields.Many2one(
        'account.journal', 
        related='company_id.discount_journal_id',
        string='Discount journal')


class sale_configuration(models.TransientModel):
    _inherit = 'sale.config.settings'

    default_order_policy = fields.Selection(
        [('manual', 'Invoice based on sales orders'), ('picking', 'Invoice based on deliveries'), ('solarnow', 'Solarnow')],
        'The default invoicing method is', default_model='sale.order',
        help="You can generate invoices based on sales orders or based on shippings.")
