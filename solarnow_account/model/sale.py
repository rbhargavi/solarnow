# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp import models, fields, api, _
from openerp.exceptions import Warning
from util import collect_and_reconcile
import openerp.addons.decimal_precision as dp
import json
import re
import util

_logger = logging.getLogger(__name__)


class sale_order(models.Model):
    _inherit = "sale.order"

    @api.onchange('payment_term')
    def onchange_payment_term(self):
        if not self.payment_term:
            return 
        self.is_credit = self.payment_term.is_credit
        self.deposit = self.payment_term.deposit
        self.installments = self.payment_term.installments
        self.installment_interval = self.payment_term.installment_interval
        self.installment_interval_unit = self.payment_term.installment_interval_unit
        self.grace_period = self.payment_term.grace_period
        self.grace_period_unit = self.payment_term.grace_period_unit

    @api.model
    def _default_interest_rate(self):
        company = self.env.user.company_id
        return company.interest_rate

    order_policy = fields.Selection([
        ('solarnow', 'Solarnow'),
        ('manual', 'On Demand'),
        ('picking', 'On Delivery Order'),
        ('prepaid', 'Before Delivery')
    ], 'Create Invoice', required=True, readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, default=lambda *args: 'solarnow')

    deposit = fields.Integer(
        'Deposit percentage',
        readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'assessment': [('readonly', False)], 'approval': [('readonly', False)], 'incomplete': [('readonly', False)]})
    installments = fields.Integer(
        'Number of installments',
        readonly=True,
        states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'assessment': [('readonly', False)], 'approval': [('readonly', False)], 'incomplete': [('readonly', False)]})
    interest_rate = fields.Float("Interest", digits=(1,8), default=_default_interest_rate, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'assessment': [('readonly', False)], 'approval': [('readonly', False)], 'incomplete': [('readonly', False)]})
    already_paid = fields.Float('Already Paid', digits=(1,8))
    payplan_date = fields.Date("Payplan Date")
    installment_interval = fields.Integer('Installment interval', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'assessment': [('readonly', False)], 'approval': [('readonly', False)], 'incomplete': [('readonly', False)]})
    installment_interval_unit = fields.Selection([
        #('day', 'Days'),
        #('week', 'Week'),
        ('month', 'Month'),
        #('year', 'Year')
    ], string="Installment interval unit", readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'assessment': [('readonly', False)], 'approval': [('readonly', False)], 'incomplete': [('readonly', False)]})
    
    grace_period = fields.Integer('Grace period', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'assessment': [('readonly', False)], 'approval': [('readonly', False)], 'incomplete': [('readonly', False)]})
    grace_period_unit = fields.Selection([
        #('day', 'Days'),
        #('week', 'Week'),
        ('month', 'Month'),
        #('year', 'Year')
    ], string="Grace period unit", readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'assessment': [('readonly', False)], 'approval': [('readonly', False)], 'incomplete': [('readonly', False)]})

    adjustments = fields.Char(size=256, string="Adjustments")

    want_fluctuation = fields.Boolean(string="Want fluctuating Payplan")

    invoice_id = fields.Many2one(
        string='Last Invoice',
        comodel_name='account.invoice',
        compute='_compute_invoice_id'
    )

    @api.multi
    def _compute_invoice_id(self):
        for order in self:
            invoice = self.env['account.invoice'].search([
                ('sale_order', '=', order.id)
            ], order='create_date desc', limit=1)
            order.invoice_id = invoice

    @api.multi
    def goto_invoice_id(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.invoice',
            'res_id': self.invoice_id.id,
            'name': 'Latest Payplan',
            'view_mode': 'form',
            'views': [(False, 'form')],
            'domain': [('id', '=', self.invoice_id.id)],
            'context': {'create': False},
        }

    @api.one
    def _payplan_parameters(self):
        # FIXME: better ui
        adjustments = {}
        if self.adjustments and re.match(r'^[0-9]+:[0-9]+(,[0-9]+:[0-9]+)*$', self.adjustments):
            adjustments = eval('{' + self.adjustments + '}')
        
        # amount should be amount inc vat for all non-interest lines (aka products + discounts),
        # keeping in mind not to include vat on interest, minus subsidy
        amount = sum(l.price_unit * l.product_uom_qty for l in self.order_line if not l.is_interest_line) - self.amount_subsidy
        amount -= self.already_paid
        # if an invoice exists, show actual payplan (first ?)
        payplan_date = self.payplan_date
        if not payplan_date:
            for invoice in self.invoice_ids:
                if invoice.is_credit and invoice.payplan_date and (not payplan_date or invoice.payplan_date < payplan_date):
                    payplan_date = invoice.payplan_date

        return {
            'currency': self.currency_id,
            'is_credit': self.is_credit,
            'amount': amount, #self.amount_untaxed + self.amount_tax - self.amount_subsidy, 
            'downpayment_percentage': self.deposit,
            'number_of_installments': self.installments,
            'yearly_interest_rate': self.interest_rate, 
            'installment_interval': self.installment_interval,
            'installment_interval_unit': self.installment_interval_unit,
            'grace_period': self.grace_period,
            'grace_period_unit': self.grace_period_unit,
            'date_ref': payplan_date or (self.downpayment_line_id and self.downpayment_line_id.date) or (self.date_order and self.date_order[:10]) or None,
            'adjustments': adjustments
        }

    @api.one
    @api.depends('order_line.price_unit',
                 'already_paid',
                 'payplan_date',
                 'order_line.tax_id', 
                 'payment_term', 
                 'order_line', 
                 'installments', 
                 'deposit',
                 'installment_interval', 
                 'installment_interval_unit', 
                 'grace_period', 
                 'adjustments', 
                 'grace_period_unit')
    def _compute_amount(self):
        self.amount_untaxed = sum(line.price_subtotal for line in self.order_line)
        self.amount_subsidy = sum(line.price_unit for line in self.order_line if line.discount_id and line.discount_id.subsidized)        

        if self.is_credit:
            payplan_parameters = self._payplan_parameters()[0]
            self.amount_interest = self.env['account.payment.term'].compute_interest(**payplan_parameters)
            self.amount_downpayment = self.env['account.payment.term'].compute_downpayment(**payplan_parameters)
            payplan = self.env['account.payment.term'].compute_preview(**payplan_parameters)
            #if self.env.context.get('updating_interest', None) is None:
            #    self.update_interest()
        else:
            payplan = {}
            self.amount_interest = 0
            self.amount_downpayment = 0

        self.preview_payplan = json.dumps(payplan)

    amount_downpayment = fields.Float(string='Downpayment', digits=dp.get_precision('Account'),
        store=True, readonly=True, compute='_compute_amount', track_visibility='always')

    amount_interest = fields.Float(string='Interest', digits=dp.get_precision('Account'),
        store=True, readonly=True, compute='_compute_amount', track_visibility='always')

    amount_subsidy = fields.Float(string='Subsidy', digits=dp.get_precision('Account'),
        store=True, readonly=True, compute='_compute_amount', track_visibility='always')

    preview_payplan = fields.Text("preview", compute="_compute_amount", readonly=True)

    def _interest_grouping_key(self, line):
        return (','.join(map(str, sorted(map(lambda t: t.id, line.tax_id)))),)

    @api.multi
    def update_interest(self):
        for order in self.with_context(updating_interest=True):
            previous_grouped_interest = {}
            grouped_interest = {}
            interest_order_lines = []
            for line in order.order_line:
                if line.is_interest_line:
                    previous_grouped_interest.setdefault(self._interest_grouping_key(line), []).append(line)
                    continue

                group_key = self._interest_grouping_key(line)
                grouped_interest.setdefault(group_key, {'amount': 0, 'taxes': map(lambda t: t.id, line.tax_id)})['amount'] += line.price_unit * line.product_uom_qty

            amount_interest = 0
            if order.is_credit:
                payplan_parameters = order._payplan_parameters()[0]
                amount_interest = self.env['account.payment.term'].compute_interest(**payplan_parameters)
                        
            for key, interest, part in zip(grouped_interest.keys(), grouped_interest.values(), util.proportion(map(lambda x: x['amount'], grouped_interest.values()), amount_interest)):
                if part > 0:
                    if key in previous_grouped_interest:
                        if previous_grouped_interest[key][0].price_unit != part:
                            interest_order_lines.append((1, previous_grouped_interest[key][0].id, {
                                        'price_unit': part,
                                        }))
                        del previous_grouped_interest[key]
                    else:
                        interest_order_lines.append((0,0,{
                            'is_interest_line': True,
                            'name': 'interest',
                            'product_id': order.company_id.interest_product_id and order.company_id.interest_product_id.id or False, 
                            'price_unit': part,
                            'product_uom_qty': 1,
                            'tax_id': [(6,0,interest['taxes'])],
                        }))
             
            for k,v in previous_grouped_interest.iteritems():
                for l in v:
                    interest_order_lines.append((2, l.id))

            if len(interest_order_lines):
                order.write({'order_line': interest_order_lines})

    @api.multi
    def _update_values(self, values):
        """If these payplan parameter fields are readonly on the view, they should be copied from the payment term"""
        if 'payment_term' in values and values['payment_term']: 
            payment_term = self.env['account.payment.term'].browse(values['payment_term'])
            #values['interest_rate'] = payment_term.interest_rate
            if 'installments' not in values:
                values['is_credit'] = payment_term.is_credit
                values['deposit'] = payment_term.deposit
                values['installments'] = payment_term.installments
                values['installment_interval'] = payment_term.installment_interval
                values['installment_interval_unit'] = payment_term.installment_interval_unit
                values['grace_period'] = payment_term.grace_period
                values['grace_period_unit'] = payment_term.grace_period_unit

    @api.model
    def create(self, values):
        self._update_values(values)
        r = super(sale_order, self).create(values)
        if self.env.context.get('updating_interest', None) is None: #Check so it's not endless recursive - Dennis Sluijk
            r.update_interest()

        return r

    @api.multi
    def write(self, values):
        self._update_values(values)
        res = super(sale_order, self).write(values)
        for order in self:
            if order.state not in ('cancel', 'manual', 'progress', 'done', 'invoice_except', 'shipping_except') and self.env.context.get('updating_interest', None) is None: #Check so it's not endless recursive - Dennis Sluijk
                # TODO is there a way to not recursively call this write method at all?
                order.update_interest()
        return res

    @api.multi
    def _reserve_downpayment(self, downpayment_lines):
        self.ensure_one()
        order = self

        move_ref = 'Reserved Downpayment for {name}'.format(name=order.name)

        total = sum([l.debit - l.credit for l in downpayment_lines])
        assert total < 0
        assert abs(total) >= order.amount_downpayment

        # collect downpayment in one line
        move_values = {
            'journal_id': order.company_id.downpayment_journal_id.id,
            'ref': move_ref,
            #'period_id': invoice.period_id.id,
            'line_id': [
                (0,0,{
                    'name': 'Collected ' + move_ref,
                    'account_id': downpayment_lines[0].account_id.id,
                    'credit': order.amount_downpayment,
                    'debit': 0,
                    'partner_id': downpayment_lines[0].partner_id.id,
                })]
            }

        collected_total = 0.0
        collected_lines = []
        for l in downpayment_lines.sorted(key=lambda l: (l.date_paid, l.id)):
            bank_statement_line = l.get_bank_statement_line(l)
            move_values['line_id'].append(
                (0,0,{
                    'name': move_ref,
                    'account_id': downpayment_lines[0].account_id.id,
                    'credit': 0,
                    'debit': min(l.credit, order.amount_downpayment - collected_total),
                    'partner_id': downpayment_lines[0].partner_id.id,
                    'bank_statement_line': bank_statement_line and bank_statement_line.id or False
                }))

            move_values['line_id'][0][2]['date_paid_fixed'] = l.date_paid
            collected_total += l.credit
            collected_lines.append(l)

            if collected_total > order.amount_downpayment:
                move_values['line_id'].extend([
                    (0,0,{
                        'name': "Leftover when reserving downpayment for {name}".format(name=order.name),
                        'account_id': downpayment_lines[0].account_id.id,
                        'debit': collected_total - order.amount_downpayment,
                        'credit': 0,
                        'partner_id': downpayment_lines[0].partner_id.id,
                        'bank_statement_line': bank_statement_line and bank_statement_line.id or False
                    }),
                    (0,0,{
                        'name': "Leftover when reserving downpayment for {name}".format(name=order.name),
                        'account_id': downpayment_lines[0].account_id.id,
                        'credit': collected_total - order.amount_downpayment,
                        'debit': 0,
                        'partner_id': downpayment_lines[0].partner_id.id,
                        'bank_statement_line': bank_statement_line and bank_statement_line.id or False
                    })
                    ])
                break
            elif collected_total == order.amount_downpayment:
                break

        move = self.env['account.move'].create(move_values)
        move.post()
        # reconcile
        # FIXME if writeoff_journal==False and not partial, this following line fails
        collect_and_reconcile(self,
                              [l for l in collected_lines] + [l for l in move.line_id if l.debit > 0],
                              account_ids=[downpayment_lines[0].account_id.id],
                              )

        return [l for l in move.line_id if l.credit == order.amount_downpayment and l.name.startswith("Collected Reserved Downpayment")][0]

    @api.multi
    def reserve_downpayment(self):
        """Collect the downpayment amount in a reservation booking.
        """
        for order in self:
            if order.is_credit and order.amount_downpayment and not order.downpayment_line_id:
                _logger.info("Reserving downpayment %r", order.id)
                if not self.company_id.downpayment_account_id:
                    raise Warning(
                        _('Configuration Error!'),
                        _('Please configure the downpayment account on the accounting settings.')
                    )

                if not order._check_sufficient_deposit():
                    continue

                downpayment_lines = order._create_downpayment_lines()

                if downpayment_lines:
                    #TODO: first try to find a single line that matches, otherwise bin pack to best match ?
                    downpayment_line = order._reserve_downpayment(downpayment_lines)
                    order.write({'downpayment_line_id': downpayment_line.id})
                    _logger.info("Deposit reserved for %s", order.name)                    
                else:
                    _logger.info("Insufficient available balance on deposit account for %s", order.name)

    @api.multi
    def _create_downpayment_lines(self):
        self.ensure_one()

        reserved_lines = []
        for sale_order in self.partner_id.sale_order_ids:
            if sale_order.downpayment_line_id:
                reserved_lines.append(sale_order.downpayment_line_id.id)

        # find downpayment
        downpayment_lines = None
        downpayment_total = 0.0
        partner_lines = self.partner_id.account_move_line_ids
        for line in partner_lines:
            if line.credit > 0 and not line.reconcile_id and not line.reconcile_partial_id:
                if line.account_id == self.company_id.downpayment_account_id:
                    if line.id not in reserved_lines:
                        if downpayment_lines:
                            downpayment_lines += line
                        else:
                            downpayment_lines = line
                        downpayment_total += line.debit - line.credit
        if -downpayment_total >= self.amount_downpayment:
            return downpayment_lines
        return False

    @api.multi
    def _check_sufficient_deposit(self):
        self.ensure_one()
        if not self.env.context.get('no_balance_check', False):
            downpayment_balance = 0.0
            for line in self.partner_id.account_move_line_ids:
                if line.account_id.id == self.company_id.downpayment_account_id.id:
                    downpayment_balance += line.debit - line.credit
            if -downpayment_balance < self.amount_downpayment:
                _logger.info("Insufficient balance on deposit account for %s", self.name)
                return False
        return True

    @api.multi
    def unreserve_downpayment(self):
        for order in self:
            if self.downpayment_line_id:
                if self.downpayment_line_id.reconcile_id:
                    raise Warning(_('You cannot unreserve a reconciled downpayment!'))
                if not all([l.account_id == order.downpayment_line_id.account_id for l in self.downpayment_line_id.move_id.line_id]):
                    raise Warning(_('Cannot unreserve downpayment! Please unreserve manually.'))

                for line in self.downpayment_line_id.move_id.line_id:
                    self.env['account.move.line']._remove_move_reconcile([line.id])
                line.move_id.button_cancel()
                line.move_id.unlink()   
                order.write({'downpayment_line_id': None})

    @api.multi
    def manual_reserve_downpayment(self):
        for order in self:
            order.reserve_downpayment()
            if not order.downpayment_line_id:
                raise Warning(_('Could not reserve downpayment for this order (check balance)!'))

    @api.multi
    def wkf_approved(self):
        res = super(sale_order, self).wkf_approved()
        for order in self:
            order.reserve_downpayment()
        return res

    @api.multi
    def action_ship_create(self):
        for order in self:
            if not order.downpayment_line_id:
                order.reserve_downpayment()
            if not order.downpayment_line_id:
                raise Warning(_('The downpayment for this order could not be reserved !'))

        return super(sale_order, self).action_ship_create()

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        r = super(sale_order, self)._prepare_invoice(cr, uid, order, lines, context=context)

        r.update({
            'is_credit': order.is_credit,
            'deposit': order.deposit,
            'installments': order.installments,
            'interest_rate': order.interest_rate,
            'installment_interval': order.installment_interval,
            'installment_interval_unit': order.installment_interval_unit,
            'grace_period': order.grace_period,
            'grace_period_unit': order.grace_period_unit,
            'sale_order': order.id,
            'adjustments': order.adjustments,
            'date_invoice': None,
            'payplan_date': order.downpayment_line_id and order.downpayment_line_id.date_paid_fixed or order.downpayment_line_id.date_paid or False
        })

        return r


class sale_order_line(models.Model):
    _inherit = 'sale.order.line'
    
    is_interest_line = fields.Boolean("Is interest line (internal use)")   

    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        r = super(sale_order_line, self)._prepare_order_line_invoice_line(cr, uid, line, account_id=account_id, context=context)
        r['is_interest_line'] = line.is_interest_line
        return r
