# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2017 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp


class PayplanLine(models.TransientModel):
    _name = "payplan.line"

    invoice_id = fields.Many2one('account.invoice', 'account.invoice')
    date_maturity = fields.Date()
    date_paid = fields.Date()
    payplan_type = fields.Selection([
        ('downpayment','Downpayment'),
        ('principal','Principal'),
        ('interest','Interest'),
        ('subsidy','Subsidy')
    ], string='Payplan installment type')
    amount = fields.Float(digits=dp.get_precision('Account'))
    # debit = fields.Float(digits=dp.get_precision('Account'))
    # credit = fields.Float(digits=dp.get_precision('Account'))
    name = fields.Char(compute='_compute_name')

    @api.one
    @api.depends('date_maturity', 'payplan_type')
    def _compute_name(self):
        self.name = "%s (%s)" % (self.date_maturity or '', self.payplan_type or '')

    _defaults = {
        'amount': 0.0,
        # 'debit': 0.0,
        # 'credit': 0.0,
    }
