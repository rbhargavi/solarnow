# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api


class stock_picking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def _compute_solarnow_invoice(self):
        for picking in self:
            if picking.state == 'done' and picking.sale_id.order_policy == 'solarnow' and not picking.sale_id.invoice_exists:
                picking.solarnow_invoice = True
            else:
                picking.solarnow_invoice = False

    solarnow_invoice = fields.Boolean("Can be invoiced (internal use)", compute='_compute_solarnow_invoice')
    deposit_paid = fields.Boolean("deposit paid", related='sale_id.deposit_paid')

    def _get_invoice_vals(self, cr, uid, key, inv_type, journal_id, move, context=None):
        r = super(stock_picking, self)._get_invoice_vals(cr, uid, key, inv_type, journal_id, move, context=context)

        if move.picking_id and move.picking_id.sale_id and move.picking_id.sale_id.is_credit:
            r.update({
                'is_credit': move.picking_id.sale_id.is_credit,
                'deposit': move.picking_id.sale_id.deposit,
                'installments': move.picking_id.sale_id.installments,
                'interest_rate': move.picking_id.sale_id.interest_rate,
                'installment_interval': move.picking_id.sale_id.installment_interval,
                'installment_interval_unit': move.picking_id.sale_id.installment_interval_unit,
                'grace_period': move.picking_id.sale_id.grace_period,
                'grace_period_unit': move.picking_id.sale_id.grace_period_unit,
                'sale_order': move.picking_id.sale_id.id
            })

        return r
