# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv


class account_move_line(osv.osv):
    # keep this part as it is: OLD API
    _inherit = 'account.move.line'

    def _amount_residual(self, cr, uid, ids, field_names, args, context=None):
        """ Override to adjust line residual amount for credit bookings on receivable accounts.
        """
        res = super(account_move_line, self)._amount_residual(cr, uid, ids, field_names, args, context=context)
        for line in self.browse(cr, uid, ids):
            if line.account_id and line.account_id.type == 'receivable' and line.credit > 0:
                res[line.id]['amount_residual'] *= -1
                res[line.id]['amount_residual_currency'] *= -1
        return res

    _columns = {
        'amount_residual_currency': fields.function(_amount_residual, string='Residual Amount in Currency', multi="residual", help="The residual amount on a receivable or payable of a journal entry expressed in its currency (maybe different of the company currency)."),
        'amount_residual': fields.function(_amount_residual, string='Residual Amount', multi="residual", help="The residual amount on a receivable or payable of a journal entry expressed in the company currency."),
        }
