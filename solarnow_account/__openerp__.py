# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "Solarnow Account",
    'summary': """Solarnow Account module""",
    'description': """
    Solarnow Account customizations.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.4',

    'depends': [
        'solarnow_base',
        'solarnow_sale',
        'solarnow_discount',
        'solarnow_crm',  # required for definition of comment_ids
        'account',
        'sale',
        'sale_stock',
        'stock',
        'web',
        'web_group_expand'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/solarnow_account_security.xml',
        'wizard/reschedule_view.xml',
        'wizard/postpone_view.xml',
        'wizard/paynow_view.xml',
        'view/account_view.xml',
        'view/account_invoice.xml',
        'view/sale_view.xml',
        'view/account_bank_statement_line.xml',
        'view/account_bank_statement.xml',
        'view/res_partner_view.xml',
        'view/res_config_view.xml',
        'view/res_company.xml',
        'menuitems.xml',
        'data/ir_cron_data.xml',
    ],
    'qweb': [
        'templates/payplan_preview.xml'
     ],
    'external_dependencies': {
        'python': ['numpy'],
    },
    'demo': [
        'data/demo.xml',
    ],
    'installable': True,
}
