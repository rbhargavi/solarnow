# -*- coding: utf-8 -*-
###############################################################################
#
#   report_balancesheet_xls for Odoo
#   Copyright (C) 2004-today OpenERP SA (<http://www.openerp.com>)
#   Copyright (C) 2016-today Geminate Consultancy Services (<http://geminatecs.com>).
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from openerp.osv import orm

class AccountProfitLossWizard(orm.TransientModel):
    _name = "profit.loss.webkit"    
    _inherit = ['account.common.balance.report','accounting.report']
    _description = "Profit loss Report"
    
    def pre_print_report(self, cr, uid, ids, data, context=None):
        data = super(AccountProfitLossWizard, self).pre_print_report(
            cr, uid, ids, data, context)
        data['ids'] = [data['form']['chart_account_id']]
        wizard_data = self.browse(cr, uid, ids)
        vals = {
            'fiscalyear_id': wizard_data.fiscalyear_id and wizard_data.fiscalyear_id.id or False,
            'chart_account_id': wizard_data.chart_account_id and wizard_data.chart_account_id.id or False,
            'target_move': wizard_data.target_move,
            'label_filter': wizard_data.label_filter and wizard_data.label_filter or False,
            'fiscalyear_id_cmp': wizard_data.fiscalyear_id_cmp and wizard_data.fiscalyear_id_cmp.id or False,
            'date_from_cmp': wizard_data.date_from_cmp or False,
            'date_to_cmp': wizard_data.date_to_cmp or False,
            'account_report_id': wizard_data.account_report_id and wizard_data.account_report_id.id or False
        }
        data['form'].update(vals)
        return data
        
    def xls_export(self, cr, uid, ids, context=None):
        return self.check_report(cr, uid, ids, context=context)

    def _print_report(self, cr, uid, ids, data, context=None):
        context = context or {}
        data = self.pre_print_report(cr, uid, ids, data, context=context)
        data['form'].update(self.read(cr, uid, ids, ['date_from_cmp',  'debit_credit', 'period_from_cmp', 'period_to_cmp',  'filter_cmp', 'enable_filter'], context=context)[0])
        context.update({'data': data})
        if context.get('xls_export'):
            return {'type': 'ir.actions.report.xml',
                    'report_name': 'account.account_report_profit_loss_webkit_xls',
                    'context': context,
                    'data': data}
        else:
            return {'type': 'ir.actions.report.xml',
                'report_name': 'account.account_report_profit_loss_webkit',
                'data': data,
                'context': context
                }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

