from openerp import models, fields, api, _
from datetime import date, datetime, timedelta
from openerp.osv import osv




class hr_contract(models.Model):
    _inherit = "hr.contract"
    
    @api.multi
    def write(self, vals):
        res = super(hr_contract, self).write(vals)
        print "vals==================",vals,self._context,res
        wage_obj=self.env['emp.wage.update']
        wage_contract=wage_obj.search([('contract_id','=',self.id),('state','!=','draft')])
        print "wage_contractwage_contract",wage_contract
        if vals.get('wage') and wage_contract and self._context.get('params').get('model')=='hr.contract':
            raise osv.except_osv(_('Cannot update the wages since the contract related wages is waiting for approval!!'), '')
        elif vals.get('wage') and wage_contract and self._context.get('active_model')=='hr.contract':
            raise osv.except_osv(_('Cannot update the wages since the contract related wages is waiting for approval!!'), '')

        return res
    
    
class account_invoice(models.Model):
    _inherit = "account.invoice"
    
#   updated = fields.Boolean(string='Updated')
    
    @api.multi
    def update_old_inv(self):
        inv_ids=self.search([('state','=','open'),('updated','=',False)])
        print "inv_idsinv_idsinv_ids",inv_ids,len(inv_ids)
        if inv_ids:
            count=0
            for each in inv_ids:
                each.move_id.line_id.write({'x_original_due_date' : each.payplan_date})
#                each.write('updated':True)
                if count==1000:
                    time.sleep(1)
                count+=1

            

    @api.multi
    def write(self, vals):
        for each in self:
            if vals.get('state') == 'open' and each.move_id:
                each.move_id.line_id.write({'x_original_due_date' : each.payplan_date})
        res = super(account_invoice, self).write(vals)
        return res

class emp_wage_update(models.Model):
    _name = "emp.wage.update"

    contract_id = fields.Many2one("hr.contract", string="Contract", required=True)
    employee_id = fields.Many2one(related="contract_id.employee_id", string="Employee", readonly=True)
    old_wage = fields.Float(related="contract_id.wage", string="Old Wage", readonly=True)
    new_wage = fields.Float("New Wage")
    state = fields.Selection([
            ('draft','Draft'),
            ('confirm','Confirm'),
            ('reject','Rejected'),
            ('approve_mg','Approved By Manager'),
            ('approve_dir','Approved By Director'),
        ], string='Status', default="draft")
    sent_for_approval = fields.Boolean(
        string="Waiting for Approval",
        help="If checked the HR Officer cant update the new wages")
    note = fields.Text(string='Note',required=False)
    reason = fields.Text(string='Reason')

        
    @api.multi
    def write(self, vals):
        for each in self:
            if vals.get('new_wage')  and each.sent_for_approval==True:
                raise UserError(_('Waiting for Approval from Mgr/Director.Cannot Update the wages again!!'))
        res = super(emp_wage_update, self).write(vals)
        return res

    @api.one
    def confirm_wage(self):
        user_obj=self.env['res.users']
        self.state = 'confirm'
        self.sent_for_approval=True
        user=user_obj.browse(self._uid).name
        self.note="Confirmed by %s on %s with reason being %s"%(user,date.today(),self.reason)
        self.reason=''

    @api.one
    def reject_wage(self):
        user_obj=self.env['res.users']

        self.state = 'reject'
        self.note+='\nRejected by Manager %s on %s with reason being %s'%(user_obj.browse(self._uid).name,date.today(),self.reason)
        self.reason=''

        
    @api.one
    def reject_wage_by_dir(self):
        user_obj=self.env['res.users']

        self.state = 'reject'
        self.note+='\nRejected by Director %s on %s with reason being %s'%(user_obj.browse(self._uid).name,date.today(),self.reason)
        self.reason=''

    

    @api.one
    def approve_wage_mg(self):
        user_obj=self.env['res.users']

        self.state = 'approve_mg'
        self.sent_for_approval=False
        self.note+='\nApproved by Manager %s on %s with reason being %s'%(user_obj.browse(self._uid).name,date.today(),self.reason)
        self.reason=''



    @api.one
    def approve_wage_dir(self):
        user_obj=self.env['res.users']

        self.contract_id.wage = self.new_wage
        self.state = 'approve_dir'
        self.sent_for_approval=False
        self.note+='\nApproved by Director %s on %s with reason being %s'%(user_obj.browse(self._uid).name,date.today(),self.reason)
        self.reason='Finally Approved!!!'
