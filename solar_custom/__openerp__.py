
{
    'name': 'Solar',
    'version': '1.0',
    'category': 'HR',
    'description': """
        HR WAGES UPDATIONS
    """,
    'author': 'Milan Thakkar',
    'depends': ['base','hr_contract','solarnow_ticket'],
    'data': [
             'views/wage_update_view.xml',
             'views/ticket_view.xml',
             'views/account_invoice_view.xml',
             'security/ir.model.access.csv',   
    ],
    'installable': True,
    'auto_install': False,
}
