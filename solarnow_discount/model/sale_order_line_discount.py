# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _

from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class sale_order_line_discount(models.Model):
    _name = "sale.order.line.discount"

    name = fields.Char("Discount name", required=True)
    start_date = fields.Date('Start date')
    end_date = fields.Date('End date')
    discount_active = fields.Boolean(
        string='Discount active',
        default=True)
    subsidized = fields.Boolean(
        string='Funded by subsidiary',
        default=False)
    subsidizer = fields.Many2one(
        comodel_name='res.partner',
        string="Subsidizer"
    )
    product_id = fields.Many2one(
        comodel_name='product.product',
        string="Discount Product",
        help="Select Product to show discount on Orders"
    )
    base = fields.Selection(
        [('order_amount', 'Order amount'), ('line_amount', 'Line amount')],
        string="Base amount for discount",
        required=True
    )

    pricelists = fields.Many2many('product.pricelist', 'discount_pricelist_rel', 'discount_id', 'pricelist_id', string="Pricelists")

    lines = fields.One2many('sale.order.line.discount.line', 'discount_id', string="Discount Lines")
    society_ids = fields.Many2many('res.partner', 'discount_partner_rel', 'discount_id', 'society_id', string="Societies")

    _sql_constraints = [
        ('discount_line_product_unique', 'unique(product_id)', 'There is already a discount for this product!')
    ]

    def _in_active_period(self, order_dt=False):
        if order_dt:
            order_date = datetime.strptime(order_dt, DEFAULT_SERVER_DATETIME_FORMAT).strftime(DEFAULT_SERVER_DATE_FORMAT)
        else:
            order_date = datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)
            
        if self.start_date and self.end_date and (order_date > self.start_date and order_date < self.end_date):
            return True
        elif not self.start_date or not self.end_date:
            return True
        else:
            return False

    @api.multi
    def _compute_discount(self, base, qty, order):
        self.ensure_one()
        for line in self.lines:

            if line.discount_type == 'rel':
                computed_amount = base * line.amount / 100
                if computed_amount > line.max_base:
                    computed_amount = line.max_base
                return order.currency_id.round(computed_amount)

            computed_discount = min(line.amount * qty, base)
            return order.currency_id.round(computed_discount)
        return 0

    @api.multi
    def _is_applicable(self, order_line):
        return True
