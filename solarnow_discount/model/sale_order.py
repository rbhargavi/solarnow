# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def _discount_grouping_key(self, discount, line):
        if discount.base == 'line_amount':
            return (discount.id, line.price_unit, ','.join(map(str, sorted(map(lambda t: t.id, line.tax_id)))))
        else:
            return (discount.id)

    @api.multi
    def update_discounts(self):
        for order in self.with_context(updating_discounts=True):
            active_discounts = [d for d in order.pricelist_id.discounts if d.discount_active and d._in_active_period(order.date_order)]

            previous_grouped_discounts = {}
            grouped_discounts = {}
            discount_order_lines = []
            for line in order.order_line:
                if line.is_discount_line:
                    previous_grouped_discounts.setdefault(self._discount_grouping_key(line.discount_id, line), []).append(line)
                    continue

                line_discounts = []
                for discount in [d for d in active_discounts if d._is_applicable(line)]:
                    group_key = self._discount_grouping_key(discount, line)
                    line_discounts.append(discount)
                    grouped_discounts.setdefault(group_key, {'discount': discount, 'base': 0, 'amount': 0, 'qty': 0, 'taxes': map(lambda t: t.id, line.tax_id)})['base'] += line.price_unit * line.product_uom_qty
                    if discount.base == 'line_amount':
                        grouped_discounts[group_key]['qty'] += line.product_uom_qty
                    else:
                        grouped_discounts[group_key]['qty'] = 1

                discount_order_lines.append((1,line.id,{'discount_ids': [(6,0,[d.id for d in line_discounts])]}))

            for discount_key, discount in grouped_discounts.iteritems():
                discount['amount'] = discount['discount']._compute_discount(discount['base'], discount['qty'], order)

                if not discount['amount']:
                    continue

                if discount_key in previous_grouped_discounts:
                    if discount['amount'] != previous_grouped_discounts[discount_key][0].price_unit:
                        discount_order_lines.append((1, previous_grouped_discounts[discount_key][0].id, {
                                    'price_unit': discount['amount']
                                    }))
                    del previous_grouped_discounts[discount_key]
                else:
                    discount_order_lines.append((0,0,{
                        'is_discount_line': True,
                        'name': discount['discount'].name,
                        'product_id': discount['discount'].product_id.id,
                        'price_unit': discount['amount'],
                        'product_uom_qty': not discount['discount'].subsidized and -1 or 0,
                        'tax_id': [(6,0,discount['taxes'])],
                        'discount_id': discount['discount'].id
                    }))

            for k,v in previous_grouped_discounts.iteritems():
                for l in v:
                    discount_order_lines.append((2, l.id))

            if discount_order_lines:
                order.write({'order_line': discount_order_lines})

    @api.model
    def create(self, values):
        r = super(SaleOrder, self).create(values)
        if self.env.context.get('updating_discounts', None) is None:
            r.update_discounts()
        return r

    @api.multi
    def write(self, values):
        r = super(SaleOrder, self).write(values)
        for order in self:
            if order.state not in ('cancel', 'manual', 'progress', 'done', 'invoice_except', 'shipping_except') and self.env.context.get('updating_discounts', None) is None:
                order.update_discounts()
        return r

    def _compute_discount_lines(self):
        for order in self:
            order.discount_lines = [l.id for l in order.order_line if l.is_discount_line]

    discount_lines = fields.One2many('sale.order.line', compute="_compute_discount_lines")
    non_discount_lines = fields.One2many('sale.order.line', related='order_line')
    society_id = fields.Many2one('res.partner', string='Society')
    discount_ids = fields.Many2many(comodel_name='sale.order.line.discount', related='pricelist_id.discounts',
                                        string='Applicable discounts',
                                        help='These discounts are selectable because they are related to the pricelist which this partner is on.',
                                        readonly=True)
    society_ids = fields.Many2many(comodel_name='res.partner', related='discount_ids.society_ids', string="Societies")
