# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp.exceptions import ValidationError


class SaleOrderLineDiscountLine(models.Model):
    _name = 'sale.order.line.discount.line'

    _order = 'sequence'

    discount_id = fields.Many2one('sale.order.line.discount', string="Discount", required=True)
    sequence = fields.Integer()

    discount_type = fields.Selection(
        [('rel', 'Relative'),('abs', 'Absolute')],
        string = 'Discount type')
    amount = fields.Float('Discount amount')

    max_base = fields.Float("Maximum subsidy")

    @api.one
    @api.constrains('amount', 'discount_type')
    def _check_discount_amount(self):
        if self.amount < 0:
            raise ValidationError("Discount must be defined using a positive amount.")
        elif self.discount_type == 'rel' and self.amount > 100:
            raise ValidationError("Relative discount must be between 0 and 100.")
