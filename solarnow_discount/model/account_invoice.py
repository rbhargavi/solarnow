# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, api, _
from openerp.addons.solarnow_base.model.util import collect_and_reconcile

class account_invoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def _get_discount_journal(self):
        for invoice in self:
            return invoice.journal_id.id
        return None

    @api.multi
    def _book_discount_on_invoice(self):
        for invoice in self:
            moves = []
            for line in invoice.invoice_line:
                if line.product_id and line.product_id.type == 'service':

                    discount_amount = line.price_unit #* line.quantity
                    discount = self.env['sale.order.line.discount'].search([('product_id', '=', line.product_id.id)], limit=1)

                    if not discount or not discount.subsidized:
                        continue

                    # book discount to receivable
                    move = self.env['account.move'].create({
                        'journal_id': self._get_discount_journal(),
                        'period_id': invoice.period_id.id,
                        'ref': invoice.partner_id.ref,
                        'line_id': [
                            (0,0,{
                                'name': 'funded discount for %s' % (invoice.sale_order and invoice.sale_order.name or '/'),
                                'account_id': discount.subsidizer.property_account_receivable.id,
                                'credit': 0,
                                'debit': discount_amount,
                                'partner_id': discount.subsidizer.id,
                            }),
                            (0,0,{
                                'name': 'funded discount for %s' % (invoice.sale_order and invoice.sale_order.name or '/'),
                                'account_id': invoice.account_id.id,
                                'credit': discount_amount,
                                'debit': 0,
                                'partner_id': invoice.partner_id.id,
                                'subsidized_invoice': invoice.id
                            })]
                    })
                    move.post()
                    moves.append(move)

            if moves:
                lines = [l for m in moves for l in m.line_id if l.account_id.id == invoice.account_id.id and l.partner_id.id == invoice.partner_id.id]
                if lines:
                    for l in sorted([l for l in invoice.move_id.line_id if l.payplan_type == 'subsidy' and l.account_id.id == invoice.account_id.id and not l.reconcile_id], key=lambda l:l.date_maturity):
                        if sum([l2.debit - l2.credit for l2 in lines]) >= 0:
                            break
                    lines.append(l)

                    collect_and_reconcile(
                        invoice,
                        lines,
                        account_ids=[invoice.account_id.id],
                        partial=True
                        )


    @api.multi
    def action_move_create(self):
        """Post advice to add receivable for funded discount"""
        super(account_invoice, self).action_move_create()
        self._book_discount_on_invoice()
