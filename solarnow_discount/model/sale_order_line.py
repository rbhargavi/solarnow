# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    # set if the order line is a discount line

    is_discount_line = fields.Boolean("Is discount line (internal use)")
    discount_id = fields.Many2one('sale.order.line.discount', string="Discount (internal use)")

    # set if a discount is applicable

    discount_ids = fields.Many2many(
        comodel_name='sale.order.line.discount',
        string="Discount(s)"
    )

    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        r = super(SaleOrderLine, self)._prepare_order_line_invoice_line(cr, uid, line, account_id=account_id, context=context)
        # Set price unit (for discount lines), because the original computation doesn't do it when uosqty = 0
        if 'quantity' in r and 'price_unit' in r and r['quantity'] == 0 and r['price_unit'] == 0:
            r['price_unit'] = line.price_unit
        r['discount_id'] = line.discount_id and line.discount_id.id

        return r
