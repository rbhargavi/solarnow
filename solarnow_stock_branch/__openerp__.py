# -*- coding: utf-8 -*-
# Copyright 2016 Onestein (<http://www.onestein.eu>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': "SolarNow Stock Branch",
    'summary': """Solarnow Inventory valuation""",
    'description': """
    Solarnow Inventory valuation for stock and branch.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.1',
    'depends': [
        'stock_account',
        'solarnow_stock',
        'solarnow_base'
    ],
    'data': [
        'views/stock_location_view.xml',
        'views/stock_history_view.xml',
    ],
    'installable': True,
}
