# -*- coding: utf-8 -*-
# Copyright 2016 Onestein (<http://www.onestein.eu>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from openerp import models, fields, api, _


class StockLocation(models.Model):
    _inherit = "stock.location"

    branch_id = fields.Many2one('res.branch', string='Branch')
