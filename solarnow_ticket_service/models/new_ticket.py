# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api


class ticket_ticket(models.Model):
    _inherit = "ticket.ticket"

    #Service
    state_service = fields.Selection([
        ('draft', 'New'),
        ('open', 'Open'),
        ('to_visit', 'To visit'),
        ('branch_storage_rep_awaiting', 'Branch storage awaiting repair'),
        ('hq_storage', 'HQ Storage and transport to branch'),
        ('branch_storage_repaired', 'Branch Storage repaired product'),
        ('return_to_client', 'Return to client'),
        ('done', 'Done'),
        ('closed', 'Closed'),
        ('cancel', 'Cancel'),
        ('called', 'Called'),
        ('review_cancel', 'Cancel to review'),
        ('review_solved', 'Solved to review'),
        ('transposrt_to_hq', 'Transport to HQ'),
        ('awaiting_wrkshop_rep', 'Awaiting Workshop repair'),
        ('audit', 'Audit'),
        ('verification_survey', 'Verification & Survey'),
        ('to_replace', 'To replace'),
        ('to_repair', 'To repair'),
        ('to_buy_new', 'To buy new'),
        ('solved', 'Solved'),
        ('replaced', 'Replaced'),
        ('repaired', 'Repaired'),
        ('auto', '???'),

        ],
        default='draft',
        track_visibility='always')

    installation_ticket_id = fields.Many2one('ticket.ticket', 'Installation Ticket', domain=[('type', '=', 'install')])
    installation_ticket_installation_date = fields.Date(related='installation_ticket_id.installation_date', string='Installation Date')
    responsible_user_id = fields.Many2one('res.users', 'Responsible')
    product_id = fields.Many2one('product.product', 'Related product')
    service_cause_ids = fields.Many2many('ticket.cause', 'service_ticket_cause_rel', 'ticket_id', 'cause_id', string='Causes')
    service_solution_ids = fields.Many2many('ticket.solution', 'service_ticket_solution_rel', 'ticket_id', 'solution_id', string='Solutions')
    partner_latitude = fields.Float(related='partner_id.partner_latitude', string='Customer Latitude')
    partner_longitude = fields.Float(related='partner_id.partner_longitude', string='Customer Longitude')
    detection_date = fields.Date('Detected on')
    service_installation_latitude = fields.Float(related='installation_ticket_id.installation_latitude', string='Installation Latitude')
    service_installation_longitude = fields.Float(related='installation_ticket_id.installation_longitude', string='Installation Longitude')
    service_survey_id = fields.Many2one('ticket.survey.survey', string='Survey')
    service_survey_result_id = fields.Many2one('ticket.survey.result')
    service_survey_answer_ids = fields.One2many('ticket.survey.answer', 'result_id', related='service_survey_result_id.answer_ids')
    service_other_survey_id = fields.Many2one('ticket.survey.survey', string='Survey')
    service_other_survey_result_id = fields.Many2one('ticket.survey.result')
    service_other_survey_answer_ids = fields.One2many('ticket.survey.answer', 'result_id', related='service_other_survey_result_id.answer_ids')

    @api.multi
    @api.depends('installation_ticket_id')
    def _compute_branch_id(self):
        super(ticket_ticket, self)._compute_branch_id()
        for ticket in self:
            if ticket.type == 'service' and ticket.installation_ticket_id:
                ticket.partner_branch_id = ticket.installation_ticket_id.sale_order_id.branch_id
                    
    @api.onchange('service_other_survey_id')
    def _onchange_service_other_survey_id(self):
        if self.service_other_survey_id and self.service_other_survey_result_id:
            self.service_other_survey_id.update_result(self.service_other_survey_result_id.id)
            
    @api.onchange('service_survey_id')
    def _onchange_service_survey_id(self):
        if self.service_survey_id and self.service_survey_result_id:
            self.service_survey_id.update_result(self.service_survey_result_id.id)
#  code by bhargavi

    @api.multi
    def service_button_open(self):
        for ticket in self:
            ticket.state_service = 'open'

    @api.multi
    def open_to_tovisit(self):
        for ticket in self:
            ticket.state_service = 'to_visit'
    @api.multi
    def open_to_cancel(self):
        for ticket in self:
            ticket.state_service = 'cancel'
    @api.multi
    def open_to_done(self):
        for ticket in self:
            ticket.state_service = 'done'
    @api.multi
    def tovisit_to_done(self):
        for ticket in self:
            ticket.state_service = 'done'
    @api.multi
    def tovisit_to_brstorage_awaiting(self):
        for ticket in self:
            ticket.state_service = 'branch_storage_rep_awaiting'
    @api.multi
    def done_to_cancel(self):
        for ticket in self:
            ticket.state_service = 'cancel'
    @api.multi
    def done_to_closed(self):
        for ticket in self:
            ticket.state_service = 'closed'
    @api.multi
    def done_to_tovisit(self):
        for ticket in self:
            ticket.state_service = 'to_visit'
            
    @api.multi
    def brstorage_awaiting_to_thq(self):
        for ticket in self:
            ticket.state_service = 'transposrt_to_hq'  
    @api.multi
    def brstorage_awaiting_to_tovisit(self):
        for ticket in self:
            ticket.state_service = 'to_visit'  
            
    @api.multi
    def thq_to_awaiting_wrkshp_repair(self):
        for ticket in self:
            ticket.state_service = 'awaiting_wrkshop_rep'  
    @api.multi
    def awaiting_wrkshp_repair_to_hqstorage(self):
        for ticket in self:
            ticket.state_service = 'hq_storage'  
    @api.multi
    def awaiting_wrkshp_repair_to_audit(self):
        for ticket in self:
            ticket.state_service = 'audit'  
    @api.multi
    def hqstorage_to_brnchsto_pro_ready(self):
        for ticket in self:
            ticket.state_service = 'branch_storage_repaired'  
    @api.multi
    def brnchsto_pro_ready_to_ret_to_client(self):
        for ticket in self:
            ticket.state_service = 'return_to_client'  
    @api.multi
    def ret_to_client_to_done(self):
        for ticket in self:
            ticket.state_service = 'done'  
    @api.multi
    def audit_to_awaiting_wrshp_repair(self):
        for ticket in self:
            ticket.state_service = 'awaiting_wrkshop_rep'  
    @api.multi
    def audit_to_hq_storage(self):
        for ticket in self:
            ticket.state_service = 'hq_storage'  
#code by bhargavi
            

    # @api.multi
    # def service_button_called(self):
    #     for ticket in self:
    #         ticket.state_service = 'called'

    @api.multi
    def service_button_done(self):
        for ticket in self:
            ticket.state_service = 'done'
            
    @api.multi
    def service_button_care_done(self):
        for ticket in self:
            ticket.state_service = 'done'

    @api.multi
    def service_button_to_visit(self):
        for ticket in self:
            ticket.state_service = 'to_visit'
            
            
            
    @api.multi
    def service_branch_storage_rep_awaiting(self):
        for ticket in self:
            ticket.state_service = 'branch_storage_rep_awaiting'
            
    @api.multi
    def service_button_transposrt_to_hq(self):
        for ticket in self:
            ticket.state_service = 'transposrt_to_hq'
    @api.multi
    def service_button_awaiting_wrkshop_rep(self):
        for ticket in self:
            ticket.state_service = 'awaiting_wrkshop_rep'
            
    @api.multi
    def service_button_audit(self):
        for ticket in self:
            ticket.state_service = 'audit'
            
    @api.multi
    def service_hq_storage(self):
        for ticket in self:
            ticket.state_service = 'hq_storage'
    @api.multi
    def service_branch_storage_repaired(self):
        for ticket in self:
            ticket.state_service = 'branch_storage_repaired'
            
            
    @api.multi
    def service_button_back_to_workshop(self):
        for ticket in self:
            ticket.state_service = 'back_to_workshop'
            
    @api.multi
    def service_button_return_to_client(self):
        for ticket in self:
            ticket.state_service = 'return_to_client'

    @api.multi
    def service_button_review_cancel(self):
        for ticket in self:
            ticket.state_service = 'review_cancel'

    @api.multi
    def service_button_review_solved(self):
        for ticket in self:
            ticket.state_service = 'review_solved'

    @api.multi
    def service_button_close(self):
        for ticket in self:
            if ticket.type == 'service' and (not ticket.service_survey_id or not ticket.service_survey_result_id.is_complete):
                raise Warning('Error!', 'Service Carespection is required.')
            ticket.state_service = 'closed'

    @api.multi
    def service_button_cancel(self):
        for ticket in self:
            ticket.state_service = 'cancel'

    @api.multi
    def service_button_reopen(self):
        for ticket in self:
            ticket.state_service = 'open'

    @api.multi
    def service_button_to_replace(self):
        for ticket in self:
            ticket.state_service = 'to_replace'

    @api.multi
    def service_button_to_repair(self):
        for ticket in self:
            ticket.state_service = 'to_repair'

    @api.multi
    def service_button_to_buy_new(self):
        for ticket in self:
            ticket.state_service = 'to_buy_new'

    @api.multi
    def service_button_solved(self):
        for ticket in self:
            ticket.state_service = 'solved'

    @api.multi
    def service_button_replaced(self):
        for ticket in self:
            ticket.state_service = 'replaced'

    @api.multi
    def service_button_not_approved(self):
        for ticket in self:
            ticket.state_service = 'to_repair'

    @api.multi
    def service_button_repaired(self):
        for ticket in self:
            ticket.state_service = 'repaired'

    @api.model
    def create(self, vals):
        #Create survey results
        if 'type' in vals and vals['type'] == 'service':
            default_other_survey = self.env['ticket.survey.survey'].search([('category', '=', 'other'), ('ticket_type', '=', 'service')])
            if len(default_other_survey) > 0:
                vals['service_other_survey_id'] = default_other_survey[0].id
                result = default_other_survey[0].create_result()
                vals['service_other_survey_result_id'] = result[0].id

            default_service_survey = self.env['ticket.survey.survey'].search([('category', '=', 'service'), ('ticket_type', '=', 'service')])
            if len(default_service_survey) > 0:
                vals['service_survey_id'] = default_service_survey[0].id
                result = default_service_survey[0].create_result()
                vals['service_survey_result_id'] = result[0].id
        return super(ticket_ticket, self).create(vals)

    @api.one
    def write(self, vals):
        #Create survey results if not exists yet (migration for existing tickets)
        if self.type == 'service' and not self.service_other_survey_result_id:            
            default_other_survey = self.env['ticket.survey.survey'].search([('category', '=', 'other'), ('ticket_type', '=', 'service')])
            if len(default_other_survey) > 0:
                vals['service_other_survey_id'] = default_other_survey[0].id
                result = default_other_survey[0].create_result()
                vals['service_other_survey_result_id'] = result[0].id
                
        if self.type == 'service' and not self.service_survey_result_id:
            default_service_survey = self.env['ticket.survey.survey'].search([('category', '=', 'service'), ('ticket_type', '=', 'service')])
            if len(default_service_survey) > 0:
                vals['service_survey_id'] = default_service_survey[0].id
                result = default_service_survey[0].create_result()
                vals['service_survey_result_id'] = result[0].id
                
        return super(ticket_ticket, self).write(vals)
    
