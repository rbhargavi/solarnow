# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
import openerp.addons.decimal_precision as dp


class sale_order_line(models.Model):
    _inherit = "sale.order.line"

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        related='order_id.branch_id',
    )
    # FIXME from digits_compute= to digits=
    price_unit = fields.Float('Unit Price', required=True, digits_compute=dp.get_precision('Product Price'), readonly=False, states={})

    @api.one
    @api.depends('tax_id')
    def _compute_taxes(self):
        # FIXME
        for order in self:
            self.readonly_tax_id = order.tax_id

    readonly_tax_id = fields.Many2many('account.tax', 'sale_order_tax', 'order_line_id', 'tax_id', string="Taxes", compute='_compute_taxes', readonly=True)
