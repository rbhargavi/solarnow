# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp.exceptions import Warning


class sale_order(models.Model):
    _inherit = "sale.order"

    @api.multi
    @api.depends('downpayment_line_id')
    def _check_deposit_paid(self):
        for order in self:
            order.deposit_paid = False
            if order.downpayment_line_id:
                order.deposit_paid = True

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch", change_default=True,
        readonly=True,
        default=lambda self: self.env['res.branch']._branch_default_get(
            'sale.order'
        )
    )
    
    state = fields.Selection([
        ('draft', 'Draft Quotation'),
        ('assessment', 'Ready for Assessment'),
        ('approval', 'Ready for Comity Approval'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('incomplete', 'Incomplete'),
        ('sent', 'Quotation Sent'),
        ('cancel', 'Cancelled'),
        ('waiting_date', 'Waiting Schedule'),
        ('progress', 'Sales Order'),
        ('manual', 'Sale to Invoice'),
        ('shipping_except', 'Shipping Exception'),
        ('invoice_except', 'Invoice Exception'),
        ('done', 'Done'),
    ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order.\
    \nThe exception status is automatically set when a cancel operation occurs \
    in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception).\nThe 'Waiting Schedule' status is set when the invoice is confirmed\
    but waiting for the scheduler to run on the order date.", index=True, track_visibility='always')

    order_type = fields.Many2one('sale.order.type', 'Sale order type')
#    package_id = fields.Many2one('create.package', 'Package')

    deposit_amount = fields.Float("Deposit amount")
    notes_internal = fields.Text("Internal notes", help="These internal notes are not shown on reports")
    notes_rejection = fields.Text("Reason of rejection", help="These notes are not shown on reports")

    uid_committee = fields.Many2one('res.users')

    partner_ref = fields.Char(related='partner_id.ref')

    partner_phone = fields.Char(related='partner_id.phone')
    partner_phone2 = fields.Char(related='partner_id.phone2')
    related_user_id = fields.Many2one(related='user_id', comodel_name='res.users')

    order_line = fields.One2many('sale.order.line', 'order_id', string='Order Lines', readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'incomplete': [('readonly', False)]}, copy=True)

    deposit_paid = fields.Boolean('deposit paid', readonly=True, compute='_check_deposit_paid', default=False, store=True)

    downpayment_line_id = fields.Many2one('account.move.line', string="Downpayment", copy=False)

    is_cash = fields.Boolean(related='payment_term.is_cash')
    is_credit = fields.Boolean(string="Is Payplan", readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)], 'assessment': [('readonly', False)], 'approval': [('readonly', False)], 'incomplete': [('readonly', False)]})

    _sql_constraints = [
        ('downpayment_line_unique', 'unique(downpayment_line_id)', 'Downpayment is already reserved!')
    ]
    

    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        r = super(sale_order, self).onchange_partner_id(cr,  uid, ids, part, context=context)
        r['value']['user_id'] = uid
        return r

    @api.multi
    def reject(self):
        self.ensure_one()
        context = self.env.context.copy()
        context['active_ids'] = self.ids
        wizard_model = 'wizard.rejected.reason'
        return {
            'name': _('Reason of rejection'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': wizard_model,
            'domain': [],
            'context': context,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'nodestroy': True,
        }

    @api.multi
    def wkf_assessment(self):
        self.write({'state': 'assessment'})

    @api.multi
    def wkf_committee_approval(self):
        for order in self:
            order.write({'state': 'approval', 'uid_committee': self._uid})

    @api.multi
    def wkf_approved(self):
        if self.uid_committee.id == self._uid:
            raise Warning(_('Conflicting roles!'),
            _('Please approve this order by other user, you already gave approval.'))
        else:
            self.write({'state': 'approved'})
        return True


    def _search_picking(self, operator, value):
        assert operator in ['=','in']
        return [('procurement_group_id','in',[p.group_id.id for p in self.env['stock.picking'].search([('id',operator,value)]) if p.group_id])]

    @api.multi
    def _get_picking_ids(self):
        for sale in self:
            if sale.procurement_group_id:
                sale.picking_ids = self.env['stock.picking'].search([('group_id','=',sale.procurement_group_id.id)])

    picking_ids = fields.One2many('stock.picking', compute='_get_picking_ids', string='Picking associated to this sale', search="_search_picking")
