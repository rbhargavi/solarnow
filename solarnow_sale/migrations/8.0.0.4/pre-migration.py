# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.


def migrate(cr, version):
    query = """
        UPDATE ir_attachment ia
        SET res_id = ciar.contract_id, res_model = 'sale.order'
        FROM contract_ir_attachments_rel ciar
        WHERE ia.id = ciar.attachment_id
    """
    cr.execute(query)
