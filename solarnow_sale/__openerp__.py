# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "Solarnow Sale",
    'summary': """Solarnow Sale module""",
    'description': """
    Solarnow Sale customizations.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.4',

    'depends': [
        'solarnow_base',
        'sale_crm',
        'sale',
        'sale_stock',
        'product',
        'partner_sequence',
        'crm_helpdesk',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/solarnow_sale_security.xml',
        'data/product.category.csv',
        'view/account_payment_term.xml',
        'view/sale_view.xml',
        'view/sale_order_type.xml',
        'view/product_view.xml',
        'data/workflow.xml',
        'wizard/wizard_rejected_reason.xml',
        'menuitems.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
