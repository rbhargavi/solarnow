# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "Solarnow HR Expense",
    'summary': """Solarnow HR Expense module""",
    'description': """
    Solarnow HR Expense customizations.
    Do the following manually after installing this module:
    Add Inherited 'solarnow_hr_expense.group_solarnow_expense' "Solarnow / Expense" to __export__.res_groups_81 "SolarNow 8 / Finance (Bookkeeping)"
    in Settings / Groups.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.1',

    'depends': [
        'hr_expense',
        'solarnow_base',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'data/hr_expense_workflow.xml',
        'views/hr_expense_view.xml',
    ],
    'qweb': [
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
