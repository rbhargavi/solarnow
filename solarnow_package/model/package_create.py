# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp.exceptions import Warning


class create_package_solarnow(models.Model):
    _name = "create.package.solarnow"
    
    name=fields.Char(string='Package Name')
    attachment_ids= fields.Many2many('ir.attachment', 'package_attachment_rel',
            'package_id', 'attachment_id', 'Package Image Upload')
    company_id=fields.Many2one('res.company', 'Company')
    package_lines_bundle = fields.One2many('package.product.line.solar', 'package_id', string='Package Product Lines', copy=True)


class package_product_line_solar(models.Model):
    _name = "package.product.line.solar"
    
    package_id= fields.Many2one('create.package.solarnow', 'Package Reference', required=True, ondelete='cascade', select=True, readonly=True)
    product_id = fields.Many2one('product.product', 'Product')
    qty = fields.Float('Qty')

