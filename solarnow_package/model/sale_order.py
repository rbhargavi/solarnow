# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp.exceptions import Warning


class sale_order(models.Model):
    _inherit = "sale.order"

    package_id = fields.Many2one(
        comodel_name="create.package.solarnow",
        string="Standard Package", change_default=True
    )

    
    
    def onchange_package(self, cr,uid,ids, package_id, context=None):
        print "package_idpackage_id",package_id
        order_lines,price_prod=[],0.0
        package_obj = self.pool['create.package.solarnow']
        if context is None:
            context = {}
        if not package_id:
            return {'value': {'order_line': []}}
        else:
            active_package_line_obj = package_obj.browse(cr, uid, package_id).package_lines_bundle
            print "active_package_line_objactive_package_line_obj",active_package_line_obj
        for each in active_package_line_obj:
            price_prod=each.product_id.product_tmpl_id.list_price
            print "each.product_ideach.product_id",each.product_id,each.product_id.product_tmpl_id
            order_lines.append((0,0,{'product_id':each.product_id,'product_uom_qty':each.qty,'name':each.product_id.name,'price_unit':price_prod,'product_uom':1}))
        return {'value': {'order_line': order_lines}}
