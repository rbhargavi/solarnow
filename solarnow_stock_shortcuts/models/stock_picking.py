from openerp import models, fields, api


class StockPicking(models.Model):
    _inherit = 'stock.picking'
    
    @api.multi
    @api.depends('sale_id', 'group_id') #id?
    def _compute_routes(self):
        for picking in self:
            picking.is_north_east = picking.sale_id.warehouse_id.route_id.name == 'North-East'
            picking.is_south_west = picking.sale_id.warehouse_id.route_id.name == 'South-West'
                
    is_north_east = fields.Boolean(compute='_compute_routes', store=True)
    is_south_west = fields.Boolean(compute='_compute_routes', store=True)
    