# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.exceptions import Warning
import logging
import traceback
_logger = logging.getLogger(__name__)


class res_branch(models.Model):
    _inherit = "res.branch"

    @api.one
    def _compute_warehouse_id(self):
        warehouse = self.env['stock.warehouse'].search([('branch_id', '=', self.id)], limit=1)
        if warehouse:
            self.warehouse_id = warehouse[0].id

    warehouse_id = fields.Many2one(
        'stock.warehouse',
        compute='_compute_warehouse_id', read_only=True,
        string="Warehouse", help='Warehouse for branch')

    route_id = fields.Many2one(
        'delivery.route',
        related="warehouse_id.route_id",
        string="Route",
        help='Route from branch set on warehouse')

    has_warehouse = fields.Boolean(string='Warehouse', default=True)

    def create_linked_warehouse(self, branch):

        wh_values = {
            "name": branch.name,
            "code": branch.name,  # TODO Discuss code generation policy
            "branch_id": branch.id,
            "partner_id": branch.partner_id.id
        }

        return self.env["stock.warehouse"].create(wh_values)

    @api.model
    def create(self, vals):
        _logger.debug("ONESTEiN create res_branch solarnow_stock")
        res = super(res_branch, self).create(vals)
        if vals.get("has_warehouse"):
            try:
                self.create_linked_warehouse(res)
            except:
                res.unlink()
                _logger.error("ONESTEiN error in warehouse creation {}".format(traceback.format_exc()))
                raise Warning(_('Error!'), _('Error in in the related warehouse creation'))
        return res

    @api.multi
    def write(self, vals):
        _logger.debug("ONESTEiN write res_branch solarnow_stock")
        ret = super(res_branch, self).write(vals)
        if vals.get("has_warehouse"):
            for branch in self:
                if not self.env["stock.warehouse"].search([("branch_id", "=", branch.id)]):
                    try:
                        self.create_linked_warehouse(branch)
                    except:
                        _logger.error("ONESTEiN error in warehouse creation {}".format(traceback.format_exc()))
                        raise Warning(_('Error!'), _('Error in in the related warehouse creation'))
        return ret
