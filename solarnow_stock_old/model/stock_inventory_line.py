from openerp import models, fields, api


class StockInventoryLine(models.Model):
    _inherit = 'stock.inventory.line'

    # Default in create and write because old api
    @api.model
    def create(self, vals):
        if not vals['partner_id']:
            vals['partner_id'] = self.env.user.partner_id.id
        return super(StockInventoryLine, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'partner_id' in vals and not vals['partner_id']:
            vals['partner_id'] = self.env.user.partner_id.id
        elif 'partner_id' not in vals and not self.partner_id:
            vals['partner_id'] = self.env.user.partner_id.id
        return super(StockInventoryLine, self).write(vals)
