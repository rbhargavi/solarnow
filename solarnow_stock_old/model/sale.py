# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, _


class sale_order(models.Model):
    _inherit = "sale.order"

    route_id = fields.Many2one('delivery.route',
        related="warehouse_id.route_id",
        string="Route")

    def _default_warehouse(self):
        branch = self.env['res.branch']._branch_default_get('sale.order')
        if branch.warehouse_id:
            return branch.warehouse_id[0].id

    warehouse_id = fields.Many2one(default=_default_warehouse)

    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        r = super(sale_order, self).onchange_partner_id(cr,  uid, ids, part, context=context)

        if part:
            partner = self.pool.get('res.partner').browse(cr, uid, part, context=context)
            if partner and partner.branch_id:
                warehouse = self.pool.get('stock.warehouse').search(cr, uid, [('branch_id', '=', partner.branch_id.id)], limit=1)
                if warehouse:
                    r.setdefault('value', {})['warehouse_id'] = warehouse[0]

        return r
