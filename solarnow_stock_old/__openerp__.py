# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "SolarNow Stock",
    'summary': """Solarnow stock and delivery management module""",
    'description': """
    Solarnow customizations for stock and deliveries management.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.1',
    'depends': [
        'solarnow_sale',
        'sale_stock',  # TODO Move in solarnow_sale
        'solarnow_base'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/account.journal.csv',
        'view/stock_view.xml',
        'view/delivery_route_view.xml',
        'view/branch_view.xml',
        'view/sale_view.xml',
        'menuitems.xml',
    ],
    'qweb': [
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
