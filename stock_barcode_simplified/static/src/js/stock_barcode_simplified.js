openerp.stock_barcode_simplified = function(instance) {
    instance.stock.PickingMainWidget.include({
        done: function() {
            var self = this;
            return new instance.web.Model('stock.picking')
                .call('action_done_from_ui',[self.picking.id, {'default_picking_type_id': self.picking_type_id}])
                .then(function(){
                    instance.webclient.action_manager.do_action("stock.menu", {});
                });
        }
    });
}