# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "Simplified picking barcode scanning",
    'summary': """Simplifies the barcode interface""",
    'description': """
    Simplifies the barcode interface.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Warehouse Management',
    'version': '0.1',
    'depends': [
        'stock',
    ],
    'data': [
        'views/assets.xml',
        'data/stock_picking_view.xml',
        'menuitems.xml'
    ],
    'qweb': [
        'static/src/xml/stock_barcode_simplified.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
