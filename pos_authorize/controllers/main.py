# -*- coding: utf-8 -*-

import logging

from openerp import http
from openerp.http import request

_logger = logging.getLogger(__name__)


class POSAuthroizePayment(http.Controller):
    @http.route('/pos/cc_payment_pay', type='json', auth='user', website=True)
    def cc_payment(self, order_dict, amount, currency_id, cc_card, cc_exp, cc_cvv, order_ref):
        POSOrder = request.env['pos.order']
        order = POSOrder.search([('pos_reference', '=', order_ref)])
        if not order:
            order = POSOrder.create(order_dict)

        PaymentAcquirer = request.env['payment.acquirer']
        acquirer = PaymentAcquirer.search([
            ('name', '=', 'Authorize POS'),
            ('provider', '=', 'authorize_aim')])
        if not acquirer:
            return {'result': False, 'msg': 'No Credit Card payment method configured.'}

        order.update({'acquirer': acquirer.id, 'pos_reference': order_ref})
        currency = request.env['res.currency'].browse(currency_id)
        vals = {
            'amount': amount,
            'currency_code': currency.name,
            'cc_number': cc_card,
            'cc_expiry': cc_exp,
            'cc_cvv': cc_cvv,
            'reference': order_ref,
        }
        results, txn_params = acquirer.pos_authorize_transaction(vals)
        if txn_params:
            return {'result': False, 'msg': txn_params['error']}

        response = results.get('ResponseCode')
        if response in ['1', '4']:
            res = order._authorize_aim_pos_validate(results)
            if res and (order.state == 'paid' or order.state == 'done'):
                return {'result': True, 'msg': 'Credit Card payment paid successfully.'}
            elif res and order.state == 'error':
                return {'result': False, 'msg': order.state_message}
            elif res and order.state == 'pending':
                return {'result': True, 'msg': order.state_message}
