# -*- coding: utf-8 -*-

{
    'name': 'POS Authorize Integration',
    'version': '1.0',
    'category': 'Point Of Sale',
    'sequence': 8,
    'summary': 'Point of Sale Authorize Credit Card Integration',
    #'author': '',
    'depends': ['point_of_sale', 'payment_authorize_aim'],
    'data': [
        'data/pos_authorize_data.xml',
        'views/assets.xml',
        'views/account_journal_views.xml',
        'views/pos_order_views.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml'
    ],
    'demo': [
        'data/pos_authorize_demo.xml',
    ],
    'installable': True,
}
