# -*- coding: utf-8 -*-

import logging

from openerp import fields, models, _

_logger = logging.getLogger(__name__)


class POSOrder(models.Model):
    _inherit = 'pos.order'

    acquirer = fields.Many2one('payment.acquirer', 'Acquirer')
    state = fields.Selection([
        ('draft', 'New'),
        ('cancel', 'Cancelled'),
        ('paid', 'Paid'),
        ('done', 'Posted'),
        ('invoiced', 'Invoiced'),
        ('pending', 'Pending'),
        ('error', 'Error')], 'Status', readonly=True, copy=False)
    state_message = fields.Text('Message',
        help='Field used to store error and/or validation messages for information')
    acquirer_reference = fields.Char('Transaction ID',
        help='Reference of the TX as stored in the acquirer database')
    authorize_res_text = fields.Char('Transaction Message',
        help='Response Reason Text details the specific reason for the transaction status.')

    def _authorize_aim_pos_validate(self, data):
        response = data.get('ResponseCode')
        tx_vals = {
            'acquirer_reference': data.get('TransactionID'),
            'authorize_res_text': data.get('ResponseText')
        }

        if response == '1':
            state_msg = 'Authorize Approved: for %s transaction' % self.name
            _logger.info(state_msg)
            tx_vals.update({
                'state': 'paid',
                'state_message': _(state_msg),
            })
            return self.write(tx_vals)
        elif response == '3':
            state_msg = 'Authorize Error: for %s transaction.' % self.name
            _logger.info(state_msg)
            tx_vals.update({
                'state': 'error',
                'state_message': _(state_msg),
            })
            return self.write(tx_vals)
        elif response == '4':
            state_msg = 'Authorize Held for review: for %s transaction.' % self.name
            _logger.info(state_msg)
            tx_vals.update({
                'state': 'pending',
                'state_message': _(state_msg),
            })
            return self.write(tx_vals)
        else:
            state_msg = 'Authorize Unrecognized Response: for %s transaction.' % self.name
            _logger.info(state_msg)
            tx_vals.update({
                'state': 'error',
                'state_message': _(state_msg),
            })
            return self.write(tx_vals)
