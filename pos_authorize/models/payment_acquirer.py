# -*- coding: utf-8 -*-

import logging
import urllib
import urllib2

from openerp import api, models, _

_logger = logging.getLogger(__name__)


class POSPaymentAcquirerAuthorizePayment(models.Model):
    _inherit = 'payment.acquirer'

    def _get_authorize_aim_urls(self):
        """ Authorize URLs """
        if self.environment == 'prod':
            return {'authorize_aim_url': 'https://secure.authorize.net/gateway/transact.dll'}
        else:
            return {'authorize_aim_url': 'https://test.authorize.net/gateway/transact.dll'}

    @api.multi
    def pos_authorize_transaction(self, vals):
        response_result = []
        results = []
        error_msg = None
        txn_params = {}
        delimiter = '|'
        auth_aim_url = self._get_authorize_aim_urls()

        authorize_credential = {
            'x_login': self.authorize_login_id,
            'x_tran_key': self.authorize_txn_key,
        }
        base_params = dict(authorize_credential)
        base_params.update({
            'x_version': '3.1',
            'x_test_request': 'TRUE' if not self.environment == 'prod' else 'FALSE',
            'x_delim_char': ',',
            'x_delim_data': 'TRUE',
            'x_relay_response': 'FALSE',
            'x_encap_char': delimiter,
            'x_type': 'AUTH_CAPTURE',
            'x_market_type': 0,
            'x_duplicate_window': 180
        })
        transaction_params = dict(base_params)
        transaction_params.update({
            'x_amount': float(vals.get('amount')),
            'x_currency_code': vals.get('currency_code'),
            'x_card_num': vals.get('cc_number').replace(" ", ""),
            'x_exp_date': vals.get('cc_expiry').replace(" ", ""),
            'x_card_code': vals.get('cc_cvv'),
            'x_invoice_num': vals.get('reference')
        })
        encoded_args = urllib.urlencode(transaction_params)
        response = None
        try:
            request = urllib2.Request(auth_aim_url['authorize_aim_url'], encoded_args)
            response = urllib2.urlopen(request)
            response_result += str(response.read()).split(delimiter)
        except Exception, e:
            _logger.exception('Authorize transaction request failed - %s', e)
            error_msg = 'Authorize transaction request failed - %s', e
            txn_params.update({'error': _(error_msg)})
        finally:
            response.close()

        results_data = self.namedtuple('Results', 'First \
            ResponseCode ResponseSubcode ResponseReasonCode ResponseText \
            AuthCode AVSResponse TransactionID \
            InvoiceNumber Description Amount PaymentMethod TransactionType CustomerID \
            FirstName LastName Company \
            BillingAddress BillingCity BillingState BillingZip BillingCountry \
            Phone Fax Email \
            ShippingFirstName ShippingLastName ShippingCompany ShippingAddress ShippingCity \
            ShippingState ShippingZip ShippingCountry \
            TaxAmount DutyAmount FreightAmount TaxExemptFlag PONumber MD5Hash \
            CVVResponse CAVVResponse AccountNumber CardType')
        response_result = [x for x in response_result if x != ',']
        results = results_data(*tuple(r for r in response_result))

        res = dict()
        if results.ResponseCode in ['1', '4']:
            res.update({
                'ResponseCode': results.ResponseCode,
                'ResponseText': results.ResponseText,
                'AuthCode': results.AuthCode,
                'TransactionID': results.TransactionID,
                'InvoiceNumber': results.InvoiceNumber,
                'Amount': results.Amount,
                'CustomerID': results.CustomerID or None
            })
        elif results.ResponseCode in ['2', '3']:
            message = 'Your credit card was declined by your bank: %s' % results.ResponseText
            _logger.warning(message)
            txn_params.update({'error': _(message)})
        else:
            txn_params.update({'error': _(error_msg)})
        return res, txn_params


class NamedTupleBase(tuple):
    """Base class for named tuples with the __new__ operator set, named tuples
       yielded by the namedtuple() function will subclass this and add
       properties."""
    def __new__(cls, *args, **kws):
        """Create a new instance of this fielded tuple"""
        # May need to unpack named field values here
        if kws:
            values = list(args) + [None] * (len(cls._fields) - len(args))
            fields = dict((val, idx) for idx, val in enumerate(cls._fields))
            for kw, val in kws.iteritems():
                assert kw in kws, "%r not in field list" % kw
                values[fields[kw]] = val
            args = tuple(values)
        return tuple.__new__(cls, args)
