# -*- coding: utf-8 -*-

from openerp import fields, models


class POSAccountJournal(models.Model):
    _inherit = 'account.journal'

    is_required_card = fields.Boolean('Display Credit Card Fields',
        help="Check this box if this journal depend on Credit Card then visible Credit Card fields")
