function openerp_pos_authorize(instance, module){
    var QWeb = instance.web.qweb,
    _t = instance.web._t;

    module.PaymentScreenWidget.include({
        init: function(parent, options) {
            var self = this;
            this._super(parent,options);

            this.cc_paynow = function(event){
                var node = this;
                while(node && !node.classList.contains('paymentline')){
                    node = node.parentNode;
                }

                var currentOrder = self.pos.get('selectedOrder');
                if(currentOrder.get('orderLines').models.length === 0){
                    self.pos_widget.screen_selector.show_popup('error',{
                        'message': _t('Empty Order'),
                        'comment': _t('There must be at least one product in your order before it can be validated'),
                    });
                    return;
                }
                if(node){
                    cc_pay_amount = $(node).find('.paymentline-input').val();
                    cc_card = $(node).find('input[name=cc_card]').val();
                    cc_exp = $(node).find('input[name=cc_exp]').val();
                    cc_cvv = $(node).find('input[name=cc_cvv]').val();
                    order_ref = currentOrder.attributes.name;

                    openerp.jsonRpc("/pos/cc_payment_pay", 'call', {
                        'order_dict': currentOrder.export_as_JSON(),
                        'amount': cc_pay_amount,
                        'currency_id': self.pos.currency.id,
                        'cc_card': cc_card,
                        'cc_exp': cc_exp,
                        'cc_cvv': cc_cvv,
                        'order_ref': order_ref
                    }).then(function (data) {
                        if (!data['result']) {
                            self.pos_widget.screen_selector.show_popup('error',{
                                'message': _t('Transaction Error'),
                                'comment': _t(data['msg']),
                            });
                            return;
                        }else{
                            var succ_msg = '<p style="color: green;">Approved: '+data['msg']+'</p>';
                            $(node).find('.cc_card_form').replaceWith(succ_msg);
                            $(node).find('.paymentline-delete').hide();
                        }
                    });
                }
            };
        },
        render_paymentline: function(line){
            var el_html  = openerp.qweb.render('Paymentline',{widget: this, line: line});
                el_html  = _.str.trim(el_html);

            var el_node  = document.createElement('tbody');
                el_node.innerHTML = el_html;
                el_node = el_node.childNodes[0];
                el_node.line = line;
                el_node.querySelector('.paymentline-delete')
                    .addEventListener('click', this.line_delete_handler);
                el_node.addEventListener('click', this.line_click_handler);
                el_node.querySelector('input')
                    .addEventListener('keyup', this.line_change_handler);
            if(line.cashregister.journal.is_required_card)
                el_node.querySelector('.btn_paynow')
                    .addEventListener('click', this.cc_paynow);

            line.node = el_node;

            return el_node;
        },
    });
}
