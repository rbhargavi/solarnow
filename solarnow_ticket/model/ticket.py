# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.exceptions import Warning
from datetime import datetime

TICKET_TYPE = [('repair', 'Repair'),
        ('service', 'Service'),
        ('install', 'Installation'),
        ('engineer', 'Engineering'),
        ('credit', 'Credit'),
        ('followup', 'Follow-up')]

class ticket_ticket(models.Model):
    _name = "ticket.ticket"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Ticket"
    _order = 'sequence'


    #Generic
    active = fields.Boolean("Active", default=True)
    sequence = fields.Integer("Sequence")
    type = fields.Selection(TICKET_TYPE)
    name = fields.Char("Name")
    description = fields.Text("Description")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('open', 'Open'),
        ('done', 'Installation Done'),
        ('closed', 'Closed'),
        ('cancel', 'Cancel')],
        default='draft',
        track_visibility='always')

    survey_id = fields.Many2one('survey.survey', string="Survey")
    execution_date = fields.Date('Executed on')
    execution_user_id = fields.Many2one('res.users', 'Executed by')
    partner_id = fields.Many2one('res.partner', "Customer")
    partner_branch_id = fields.Many2one('res.branch', string='Branch', compute='_compute_branch_id', store=True)
    partner_phone = fields.Char(related='partner_id.phone')
    partner_salesperson = fields.Many2one(related='sale_order_id.related_user_id', string='Salesperson')
    partner_ref = fields.Char(related='partner_id.ref')
    comment_ids = fields.One2many('ticket.comment', 'ticket_id')
    installation_latitude = fields.Float('Latitude')  # keep as generic
    installation_longitude = fields.Float('Longitude')  # keep as generic

    #Installation
    sale_order_id = fields.Many2one('sale.order', "Sale order") # field partner_salesperson is related
    installation_date = fields.Date('Installation date') # required by service ticket

    #Follow-up
    deadline_date = fields.Date('Deadline')

    #Migration
    installation_satisfaction_score = fields.Float('Installation satisfaction score')
    sale_satisfaction_score = fields.Float('Sale satisfaction score')
    
    x_create_date = fields.Datetime('Migrated Create Date')
    x_create_uid = fields.Many2one('res.users', 'Migrated Create User')
    x_create_uid_name = fields.Char('Migrated Create User Name')
    x_client_called_date = fields.Date('Migrated Client Called Date')

    @api.multi
    @api.depends('partner_id')
    def _compute_branch_id(self):
        for ticket in self:
            ticket.partner_branch_id = ticket.partner_id.branch_id

    @api.one
    def button_close(self):
        self.state = 'closed'

    @api.one
    def button_done(self):
        self.state = 'done'
        self.execution_date = datetime.now()
        self.execution_user_id = self._uid

    @api.one
    def button_cancel(self):
        self.state = 'cancel'

    @api.model
    def create(self, vals):
        # TODO check: is this necessary?
        if 'active_model' in self.env.context and self.env.context['active_model'] == 'sale.order':
            vals['sale_order_id'] = self.env.context['active_id']

        return super(ticket_ticket, self).create(vals)
