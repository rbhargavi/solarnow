from openerp import models, fields, api, exceptions
from datetime import datetime
from ticket import TICKET_TYPE

CATEGORIES = [('sales', 'Sales'), 
                                 ('install', 'Installation'), 
                                 ('credit', 'Credit Assessment'), 
                                 ('service', 'Service'), 
                                 ('after-sales', 'After-Sales'),
                                 ('engineering', 'Engineering'),
                                 ('other', 'Other')]

class TicketSurvey(models.Model):
    _name = 'ticket.survey.survey'
    
    name = fields.Char('Name')
    active = fields.Boolean('Active', default=True)
    question_ids = fields.One2many('ticket.survey.question', 'survey_id', string='Questions')
    ticket_type = fields.Selection(TICKET_TYPE, string='Ticket type')
    category = fields.Selection(CATEGORIES, string='Category')
    
    @api.one
    def create_result(self):
        result = self.env['ticket.survey.result'].create({
            'name': self.name,
            'category': self.category
        })
        for question in self.question_ids:
            result.write({'answer_ids' : [(0, 0, {
                'sequence': question.sequence,
                'name': question.name,
                'score': question.score 
                })]
            })
        
        return result
    
    @api.one
    def update_result(self, result_id):
        result = self.env['ticket.survey.result'].search([('id', '=', result_id)])
        result.name = self.name
        result.category = self.category
        result.write({'answer_ids': [(5, 0)]})
        for question in self.question_ids:
            result.write({'answer_ids' : [(0, 0, {
                'sequence': question.sequence,
                'name': question.name,
                'score': question.score 
                })]
            })
        
        return result
    
class TicketSurveyQuestion(models.Model):
    _name = 'ticket.survey.question'
    
    sequence = fields.Integer('Sequence')
    name = fields.Char('Name')
    survey_id = fields.Many2one('ticket.survey.survey', string='Survey')
    score = fields.Integer('Score')
    
    
class TicketSurveyResult(models.Model):
    _name = 'ticket.survey.result'
    
    name = fields.Char('Name')
    category = fields.Selection(CATEGORIES, string='Category')
    answer_ids = fields.One2many('ticket.survey.answer', 'result_id', string='Answers')
    score = fields.Integer('Score', compute='_compute_survey_result')
    max_score = fields.Integer('Maximum score', compute='_compute_survey_result')
    is_complete = fields.Boolean(compute='_is_complete')
    
    @api.one
    def _compute_survey_result(self):
        score = 0
        max_score = 0
        for answer in self.answer_ids:
            max_score += answer.score
            if answer.answer:
                score += answer.score
        self.score = score
        self.max_score = max_score
        
    @api.one
    def _is_complete(self):
        complete = True
        for answer in self.answer_ids:
            if not answer.answer:
                complete = False
        self.is_complete = complete
    
    
class TicketSurveyAnswer(models.Model):
    _name = 'ticket.survey.answer'
    
    result_id = fields.Many2one('ticket.survey.result', string='Survey')
    
    #Redundant for a reason
    sequence = fields.Integer('Sequence')
    name = fields.Char('Question')
    score = fields.Integer('Score')
    answer = fields.Selection([('no', 'No'), ('yes', 'Yes')], string='Answer') #Selection because answer must be selected
    
    