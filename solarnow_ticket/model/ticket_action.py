from openerp import models, fields, api


class TicketAction(models.AbstractModel):
    _name = 'ticket.action'

    name = fields.Char(
        string='Name'
    )

    description = fields.Text(
        string='Description'
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )