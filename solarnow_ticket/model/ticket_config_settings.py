from openerp import models, fields


class TicketConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    _name = 'ticket.config.settings'

    company_id = fields.Many2one(
        'res.company', string='Company', required=True,
        default=lambda self: self.env.user.company_id
    )
