# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "SolarNow Ticketing",
    'summary': """Solarnow ticketing system""",
    'description': """
    Solarnow additions for a new ticketing system.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.4',
    'depends': [
        'solarnow_base',
        'survey',
        'solarnow_sale',
        'solarnow_account',
        'web_disable_selection_options',
        'account'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/solarnow_ticket_security.xml',
        'view/ticket_view.xml',
        'view/res_branch_view.xml',
        'view/res_partner_view.xml',
        'view/ticket_cause_view.xml',
        'view/ticket_solution_view.xml',
        'view/ticket_survey_view.xml',
        'view/ticket_config_settings_view.xml',
        'menuitems.xml',
    ],
    'installable': True,
}
