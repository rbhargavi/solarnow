def migrate(cr, version):
    if not version:
        return
    cr.execute('UPDATE ticket_ticket Q '
               'SET '
               'type = T.ticket_type, '
               'partner_id = T.customer_id, '
               #'partner_latitude = T.customer_latitude, '
               #'partner_longitude = T.customer_longitude, '
               #'partner_phone = T.customer_phone, '
               #'partner_branch_id = T.branch_id, '
               'deadline_date = T.deadline, '
               'responsible_user_id = T.responsible_employee_id, '
               'installation_user_1_id = T.installation_employee_1_id, '
               'installation_user_2_id = T.installation_employee_2_id, '
               'installation_report = T.installation_doc, '
               'execution_user_id = T.execution_user '
               'FROM ticket_ticket T WHERE T.id = Q.id'
               )
    