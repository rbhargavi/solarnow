# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.


def migrate(cr, version):
    query = """
        UPDATE ir_attachment ia
        SET res_id = tiar.ticket_id, res_model = 'ticket.ticket'
        FROM ticket_ir_attachments_rel tiar
        WHERE ia.id = tiar.attachment_id
    """
    cr.execute(query)
