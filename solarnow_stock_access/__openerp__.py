# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': "SolarNow Stock Access",
    'summary': """Solarnow stock access""",
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.1',
    'depends': [
        'stock',
        'solarnow_stock',
        'solarnow_stock_shortcuts',
        'stock_picking_wave',
        'stock_account'
    ],
    'data': [
        'security/ir.model.access.csv',
        #'security/product_product_security.xml',
        #'security/product_template_security.xml',
        #'security/stock_picking_security.xml',
        
        #'views/stock_picking_wave_view.xml',
        #'views/stock_picking_view.xml',
        'menuitems.xml'
    ],
    'installable': True,
    'auto_install': True,
    'application': False,
}