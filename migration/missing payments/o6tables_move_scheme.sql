--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-12-02 15:07:23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 259 (class 1259 OID 931434)
-- Name: o6_account_move; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE o6_account_move (
    id integer NOT NULL,
    create_uid integer,
    create_date timestamp without time zone,
    write_date timestamp without time zone,
    write_uid integer,
    name character varying(64) NOT NULL,
    state character varying(16) NOT NULL,
    ref character varying(64),
    company_id integer,
    journal_id integer NOT NULL,
    period_id integer NOT NULL,
    narration text,
    date date NOT NULL,
    partner_id integer,
    to_check boolean,
    payplan_id integer,
    balance numeric
);

--
-- TOC entry 4616 (class 1259 OID 1341189)
-- Name: account_move_date_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_date_index ON o6_account_move USING btree (date);


-- Completed on 2016-12-02 15:07:23

--
-- PostgreSQL database dump complete
--

