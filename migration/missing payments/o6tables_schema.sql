--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-12-02 10:59:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 260 (class 1259 OID 931440)
-- Name: o6_account_move_line; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE o6_account_move_line (
    id integer NOT NULL,
    create_uid integer,
    create_date timestamp without time zone,
    write_date timestamp without time zone,
    write_uid integer,
    statement_id integer,
    journal_id integer NOT NULL,
    currency_id integer,
    date_maturity date,
    partner_id integer,
    reconcile_partial_id integer,
    blocked boolean,
    analytic_account_id integer,
    credit numeric,
    centralisation character varying(8),
    company_id integer,
    tax_code_id integer,
    state character varying(16),
    debit numeric,
    ref character varying(64),
    account_id integer NOT NULL,
    period_id integer NOT NULL,
    date_created date,
    date date NOT NULL,
    move_id integer NOT NULL,
    name character varying(64) NOT NULL,
    reconcile_id integer,
    tax_amount numeric,
    product_id integer,
    account_tax_id integer,
    product_uom_id integer,
    amount_currency numeric,
    quantity numeric,
    do_not_use1 numeric,
    do_not_use2 numeric,
    payplan_id integer,
    asset_id integer
);


CREATE TABLE o6_account_invoice (
    id integer NOT NULL,
    create_uid integer,
    create_date timestamp without time zone,
    write_date timestamp without time zone,
    write_uid integer,
    origin character varying(64),
    comment text,
    date_due date,
    check_total numeric,
    reference character varying(64),
    payment_term integer,
    number character varying(64),
    journal_id integer NOT NULL,
    currency_id integer NOT NULL,
    address_invoice_id integer NOT NULL,
    partner_id integer NOT NULL,
    fiscal_position integer,
    user_id integer,
    partner_bank_id integer,
    address_contact_id integer,
    reference_type character varying(16) NOT NULL,
    company_id integer NOT NULL,
    amount_tax numeric,
    state character varying(16),
    type character varying(16),
    internal_number character varying(32),
    account_id integer NOT NULL,
    reconciled boolean,
    residual numeric,
    move_name character varying(64),
    date_invoice date,
    period_id integer,
    amount_untaxed numeric,
    move_id integer,
    amount_total numeric,
    name character varying(64),
    invoice_number character varying(32),
    payplan_id integer
);


CREATE TABLE o6_account_journal (
    id integer NOT NULL,
    create_uid integer,
    create_date timestamp without time zone,
    write_date timestamp without time zone,
    write_uid integer,
    default_debit_account_id integer,
    code character varying(5) NOT NULL,
    view_id integer NOT NULL,
    currency integer,
    sequence_id integer NOT NULL,
    allow_date boolean,
    update_posted boolean,
    user_id integer,
    name character varying(64) NOT NULL,
    centralisation boolean,
    group_invoice_lines boolean,
    company_id integer NOT NULL,
    refund_journal boolean,
    entry_posted boolean,
    type character varying(32) NOT NULL,
    default_credit_account_id integer,
    analytic_journal_id integer,
    invoice_sequence_id integer
);



CREATE TABLE o6_ir_model_data (
    id integer NOT NULL,
    create_uid integer,
    create_date timestamp without time zone,
    write_date timestamp without time zone,
    write_uid integer,
    noupdate boolean,
    name character varying(128) NOT NULL,
    date_init timestamp without time zone,
    date_update timestamp without time zone,
    module character varying(64) NOT NULL,
    model character varying(64) NOT NULL,
    res_id integer
);


--
-- TOC entry 764 (class 1259 OID 933174)
-- Name: o6_payplan_contract; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE o6_payplan_contract (
    id integer NOT NULL,
    create_uid integer,
    create_date timestamp without time zone,
    write_date timestamp without time zone,
    write_uid integer,
    payplan_contract_approved_date date,
    fasttrack boolean,
    downpayment_paid boolean,
    import_error boolean,
    installation_report_approved_dv integer,
    payplan_contract_date date,
    payplan_officer integer,
    payplan_product integer,
    total_paid double precision,
    application_rejected_date date,
    partner_id integer,
    application_report integer,
    assessment_date date,
    branch_id integer,
    dealer_id integer,
    application_approved_date date,
    payplan_creation_date date,
    assessment_approved_date date,
    state character varying(26),
    application_rejected_id integer,
    assessment_rejected_id integer,
    payplan_contract integer,
    installation_report integer,
    assessment_status_ppo integer,
    installation_engineer integer,
    assessment_report integer,
    installation_report_approved_bm integer,
    assessment_rejected_date date,
    rea_payplan boolean,
    application_date date,
    assessment_status_bm integer,
    name character varying(32),
    completion_date date,
    payplan_contract_approved_bm integer,
    balance_remaining double precision,
    installation_date date,
    application_status_by integer,
    installation_report_approved_date date,
    payplan_contract_approved_dv integer,
    rea_letter_date date,
    rea_letter integer,
    date_difference_moved0 character varying(34),
    date_difference integer,
    dispatch_date date,
    contract_signed boolean,
    currently_owed numeric,
    current_running_total numeric,
    received_total numeric,
    received_total_history double precision,
    balance numeric,
    x_salesperson_moved0 integer,
    x_installer integer,
    x_lead integer,
    x_lead2 integer,
    x_salesperson1 integer,
    x_salesperson character varying(64),
    x_salesperson2 integer,
    x_salesperson3 integer,
    x_installer2 integer,
    x_sales_satisfcation integer,
    x_installation_score integer,
    x_latitude double precision,
    x_longitude double precision
);


--
-- TOC entry 4633 (class 1259 OID 1341166)
-- Name: o6_account_invoice_date_due_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_invoice_date_due_index ON o6_account_invoice USING btree (date_due);


--
-- TOC entry 4634 (class 1259 OID 1341167)
-- Name: o6_account_invoice_date_invoice_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_invoice_date_invoice_index ON o6_account_invoice USING btree (date_invoice);


--
-- TOC entry 4635 (class 1259 OID 1341171)
-- Name: o6_account_invoice_move_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_invoice_move_id_index ON o6_account_invoice USING btree (move_id);


--
-- TOC entry 4636 (class 1259 OID 1341172)
-- Name: o6_account_invoice_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_invoice_name_index ON o6_account_invoice USING btree (name);


--
-- TOC entry 4637 (class 1259 OID 1341173)
-- Name: o6_account_invoice_state_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_invoice_state_index ON o6_account_invoice USING btree (state);


--
-- TOC entry 4638 (class 1259 OID 1341175)
-- Name: o6_account_invoice_type_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_invoice_type_index ON o6_account_invoice USING btree (type);


--
-- TOC entry 4641 (class 1259 OID 1341181)
-- Name: o6_account_journal_company_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_journal_company_id_index ON o6_account_journal USING btree (company_id);


--
-- TOC entry 4618 (class 1259 OID 1341190)
-- Name: o6_account_move_line_account_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_account_id_index ON o6_account_move_line USING btree (account_id);


--
-- TOC entry 4619 (class 1259 OID 1341191)
-- Name: o6_account_move_line_date_created_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_date_created_index ON o6_account_move_line USING btree (date_created);


--
-- TOC entry 4620 (class 1259 OID 1341192)
-- Name: o6_account_move_line_date_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_date_index ON o6_account_move_line USING btree (date);


--
-- TOC entry 4621 (class 1259 OID 1341193)
-- Name: o6_account_move_line_date_maturity_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_date_maturity_index ON o6_account_move_line USING btree (date_maturity);


--
-- TOC entry 4622 (class 1259 OID 1341194)
-- Name: o6_account_move_line_journal_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_journal_id_index ON o6_account_move_line USING btree (journal_id);


--
-- TOC entry 4623 (class 1259 OID 1341195)
-- Name: o6_account_move_line_journal_id_period_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_journal_id_period_id_index ON o6_account_move_line USING btree (journal_id, period_id);


--
-- TOC entry 4624 (class 1259 OID 1341197)
-- Name: o6_account_move_line_move_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_move_id_index ON o6_account_move_line USING btree (move_id);


--
-- TOC entry 4625 (class 1259 OID 1341199)
-- Name: o6_account_move_line_partner_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_partner_id_index ON o6_account_move_line USING btree (partner_id);


--
-- TOC entry 4626 (class 1259 OID 1341200)
-- Name: o6_account_move_line_period_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_period_id_index ON o6_account_move_line USING btree (period_id);


--
-- TOC entry 4627 (class 1259 OID 1341201)
-- Name: o6_account_move_line_reconcile_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_reconcile_id_index ON o6_account_move_line USING btree (reconcile_id);


--
-- TOC entry 4628 (class 1259 OID 1341202)
-- Name: o6_account_move_line_reconcile_partial_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_reconcile_partial_id_index ON o6_account_move_line USING btree (reconcile_partial_id);


--
-- TOC entry 4629 (class 1259 OID 1341205)
-- Name: o6_account_move_line_statement_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_statement_id_index ON o6_account_move_line USING btree (statement_id);


--
-- TOC entry 4630 (class 1259 OID 1341206)
-- Name: o6_account_move_line_tax_amount_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_account_move_line_tax_amount_index ON o6_account_move_line USING btree (tax_amount);


--
-- TOC entry 4642 (class 1259 OID 1341324)
-- Name: o6_ir_model_data_model_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_ir_model_data_model_index ON o6_ir_model_data USING btree (model);


--
-- TOC entry 4643 (class 1259 OID 1341329)
-- Name: o6_ir_model_data_module_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_ir_model_data_module_index ON o6_ir_model_data USING btree (module);


--
-- TOC entry 4644 (class 1259 OID 1341330)
-- Name: o6_ir_model_data_module_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_ir_model_data_module_name_index ON o6_ir_model_data USING btree (module, name);


--
-- TOC entry 4645 (class 1259 OID 1341341)
-- Name: o6_ir_model_data_name_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_ir_model_data_name_index ON o6_ir_model_data USING btree (name);


--
-- TOC entry 4646 (class 1259 OID 1341342)
-- Name: o6_ir_model_data_res_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_ir_model_data_res_id_index ON o6_ir_model_data USING btree (res_id);


--
-- TOC entry 4647 (class 1259 OID 1341433)
-- Name: o6_payplan_contract_state_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX o6_payplan_contract_state_index ON o6_payplan_contract USING btree (state);


-- Completed on 2016-12-02 10:59:24

--
-- PostgreSQL database dump complete
--

