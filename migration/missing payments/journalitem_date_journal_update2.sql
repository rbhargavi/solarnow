﻿--Payments that were posted in the refund journal are posted incorrectly.
-- kng: compare e.g. __export__.account_move_line_1867111 (target id: 249626) -> was refund on source. FAIL
-- kng:  Use the query below to check for which Journal Items the journals do not match.
--If you look at journal entries you see lots of entries in the future (migrated data) - 
-- effective date was taken from the last due date on the PayPlan instead of the actual original invoice date
-- kgn: use .invoice.date_invoice

-- SUMMARY: UPDATE the TARGET date and journal_id field on account_move_line with the SOURCE data. (Use external ids to match)

-- Basic Foreign Datawrapper setup:
CREATE EXTENSION postgres_fdw;
CREATE SCHEMA sn_schema6;

CREATE SERVER solarnow_mp6
    FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (host 'localhost', dbname 'o6_sn_live_2016jan29_bak', port '5433'); -- Fill this with your own data.

CREATE USER MAPPING FOR postgres SERVER solarnow_mp6 OPTIONS (user 'postgres', password 'YOUR_DB_PASSWORD_HERE'); -- Fill this with your own data.

IMPORT FOREIGN SCHEMA public
LIMIT TO (account_move_line, ir_model_data, payplan_contract, account_invoice, account_journal)
    FROM SERVER solarnow_mp6 INTO sn_schema6; 
-- Use the following if you need to add another schema:
-- IMPORT FOREIGN SCHEMA public
-- LIMIT TO (account_journal)
--     FROM SERVER solarnow_mp6 INTO sn_schema6;  
-- End setup

-- START create BACKUP column
ALTER TABLE account_move_line ADD COLUMN date_backup date;
ALTER TABLE account_move_line ADD COLUMN journal_id_backup integer;
-- BEGIN TRANSACTION;
UPDATE account_move_line SET date_backup = date;
UPDATE account_move_line SET journal_id_backup = journal_id;
-- COMMIT; --Uncomment if you are sure.
-- END BACKUP

                                  -- Update Journal Item Effective date with the SOURCE Invoice Date.
CREATE TABLE payment_fix (
	date date,
	res_id integer,	-- account.move.line
	v6_journal_id integer,
	journal_id integer
);
-- INSERT the dates:
INSERT INTO payment_fix
	SELECT inv6.date_invoice AS date, md8.res_id, aml6.journal_id AS v6_journal_id
	FROM sn_schema6.account_invoice AS inv6
	INNER JOIN sn_schema6.account_move_line AS aml6
	ON aml6.move_id = inv6.move_id
	INNER JOIN sn_schema6.ir_model_data AS md6
	ON md6.res_id = aml6.id AND md6.model = 'account.move.line'
	INNER JOIN public.ir_model_data AS md8
	ON md6.name = md8.name
	WHERE inv6.date_invoice IS NOT NULL
;

-- select the journal id in TARGET which belongs to the journal ID in source belonging to the aml
UPDATE payment_fix SET journal_id = (
	SELECT aj8.id AS journal_id
	FROM account_journal AS aj8
	INNER JOIN public.ir_model_data AS md8
	ON md8.res_id = aj8.id AND md8.model = 'account.journal' 
	INNER JOIN sn_schema6.ir_model_data AS md6
	ON md6.name = md8.name AND md6.model = 'account.journal'
	INNER JOIN sn_schema6.account_journal AS aj6
	ON aj6.id = md6.res_id 
	WHERE md6.res_id = payment_fix.v6_journal_id
)
;
--  update account_move_line with payment_fix, do it in two steps in case it's slow

UPDATE account_move_line 
SET 
date = 
(
	SELECT date FROM payment_fix 
	WHERE account_move_line.id = payment_fix.res_id AND payment_fix.date IS NOT NULL
)
FROM payment_fix
WHERE payment_fix.res_id = account_move_line.id  AND payment_fix.date IS NOT NULL
;

UPDATE account_move_line 
SET 
journal_id = 
(
	SELECT journal_id FROM payment_fix 
	WHERE account_move_line.id = payment_fix.res_id AND payment_fix.journal_id IS NOT NULL
)
FROM payment_fix
WHERE payment_fix.res_id = account_move_line.id  AND payment_fix.journal_id IS NOT NULL
;



