from datetime import date
from os import path
from os.path import isfile
import erppeek
import base64

#config
directory = 'C:\Projecten\solarnow_erp_documents'
server = 'http://213.187.242.232:8069'
database = 'attachment_migration'
user = 'admin'
password = 'admin@'

source_server = 'http://85.222.224.227:8069'
source_database = 'o6_sn_live_2016jan29'
source_user = 'admin'
source_password = 'op3n@dmin2011'

source = erppeek.Client(source_server, db=source_database, user=source_user, password=source_password, verbose=False)
target = erppeek.Client(server, db=database, user=user, password=password, verbose=False)

contracts = source.model('payplan.contract').browse([])
for contract in contracts:
	source_partner = source.perm_read('res.partner', [contract.partner_id.id], context=None)
	if source_partner:
		splitted_xmlid = source_partner[0]['xmlid'].split('.')
		target_ir_model_data = target.model('ir.model.data').browse([('name', '=', splitted_xmlid[1]), ('module', '=', '__export__')])
		if target_ir_model_data:
			reference = contract.name
			attachments = source.model('ir.attachment').browse([('res_id', '=', contract.id), ('res_model', '=', 'payplan.contract')])
			for attachment in attachments:
				file = attachment.store_fname
				qualified_file_name = '%s\%s' % (directory, file)
				if isfile(qualified_file_name):
					attachments_target = target.model('ir.attachment').search([('name', 'like', file + '%')])
					if not attachments_target:
						partner = target.model('res.partner').browse([('id', '=', target_ir_model_data.res_id)])
						if partner: #Deleted migrated partner
							partner = partner[0]
							with open(qualified_file_name, 'rb') as fh:
								data = fh.read()
								file_base64 = data.encode('base64')
								new_attachment = {
									'name': file,
									'res_model': 'res.partner', 
									'res_name': reference,
									'datas': file_base64,
									'datas_fname': file,
									'type': 'binary',
									'res_id': partner.id,
									'file_type': 'application/pdf',
									'mimetype': 'application/pdf'
								}
								target.model('ir.attachment').create(new_attachment)	
							print '%s - %s' % (partner.id, reference)
						else:
							print 'Could not find partner with reference %s' % (reference)
					else:
						print 'Attachment already exists (%s)' % (qualified_file_name)
				else:
					print 'File %s not found' % (qualified_file_name)
