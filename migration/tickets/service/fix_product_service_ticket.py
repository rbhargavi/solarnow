from datetime import date
import erppeek
from difflib import SequenceMatcher

source = erppeek.Client('http://85.222.224.227:8069/', db='o6_sn_live_2016jan29', user='admin', password='op3n@dmin2011', verbose=False)
destination = erppeek.Client('http://213.187.242.232:8069', db='ticket_migration', user='admin', password='admin@', verbose=False)

tickets = destination.model('ticket.ticket').browse([('product_id', '=', False), ('type', '=', 'service')], limit=200)

#deprecated

for ticket in tickets:
	if ticket._external_id:
		name = ticket._external_id.split('.')[1]
		ir_data = source.read('ir.model.data', [('name', '=', name), ('model', '=', 'project.issue')])
		if ir_data:
			project_issue = source.read('project.issue', [('id', '=', ir_data[0]['res_id'])])
			if project_issue[0]['x_Component1']:
				component_name = project_issue[0]['x_Component1'][1]
				products = destination.read('product.product', [])
				best_id = False
				best_name = False
				best_ratio = 0
				for product in products:
					product_ratio = SequenceMatcher(None, component_name, product['name']).ratio()
					if product_ratio > best_ratio:
						best_id = product['id']
						best_name = product['name']
						best_ratio = product_ratio
				print 'Ticket %s' % (str(ticket.id))
				print str(best_ratio) + ' ' + best_name
				#destination.write('ticket.ticket', [ticket.id], {'product_id': best_id })
			