from datetime import date
import erppeek

source = erppeek.Client('http://85.222.224.227:8069/', db='o6_sn_live_2016jan29', user='admin', password='op3n@dmin2011', verbose=False)
destination = erppeek.Client('http://213.187.242.232:8069', db='ticket_migration', user='admin', password='admin@', verbose=False)

tickets = destination.model('ticket.ticket').browse([('type', '=', 'service'), ('service_cause_ids', '=', False)], limit=200)

for ticket in tickets:
	name = ticket._external_id.split('.')[1]
	ir_data = source.read('ir.model.data', [('name', '=', name), ('model', '=', 'project.issue')])
	if ir_data:
		project_issue = source.read('project.issue', [('id', '=', ir_data[0]['res_id'])])
		categ_name = project_issue[0]['categ_id'][1]
		causes = destination.read('ticket.cause', [('name', '=', categ_name)])
		if causes:
			destination.write('ticket.ticket', [ticket.id], {'service_cause_ids': [(6, 0, [causes[0]['id']])]})
			print 'Issue: %s, Ticket: %s, Cause: %s' % (str(ir_data[0]['res_id']), ticket.id, categ_name)
		else:
			print 'Cause %s does not exists' % (categ_name)

