from datetime import date
import erppeek

source = erppeek.Client('https://sas.solarnow.eu', db='solarnow', user='admin', password='yEGLc77uIlm6WYbB-3K', verbose=False)

tickets = source.model('ticket.ticket').browse([('type', '=', 'service'), ('installation_ticket_id', '=', False), ('partner_id', '!=', False)])
ticket_withoutpartner = source.model('ticket.ticket').browse([('type', '=', 'service'), ('partner_id', '=', False)])
print "DATA ERROR: {} service tickets found".format(len(ticket_withoutpartner))

print "Found {} tickets".format(len(tickets))

total_fixed = 0
total_no_so = 0
total_no_install = 0

for ticket in tickets:
    sale_order_ids = source.model('sale.order').search([('partner_id', '=', ticket.partner_id.id)])
    if sale_order_ids:
        install_ticket_ids = source.model('ticket.ticket').search([('sale_order_id', '=', sale_order_ids[0]), ('type', '=', 'install')])
        if install_ticket_ids:
            total_fixed += 1
            ticket.write({'installation_ticket_id': install_ticket_ids[0]})
            print "Updated service ticket with id {}".format(ticket.id);
        else:
            print "DATA ERROR: No installation ticket found for sale order {}".format(sale_order_ids)
            total_no_install += 1
    else:
        print "DATA ERROR: No sale order found for partner of service ticket"
        total_no_so += 1

print "Total fixed: {}".format(total_fixed)
print "Total partners without sale order: {}".format(total_no_so)
print "Total sale order without installation ticket: {}".format(total_no_install)
