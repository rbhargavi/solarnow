from datetime import date
import erppeek

destination = erppeek.Client('https://sas.solarnow.eu', db='solarnow', user='admin', password='yEGLc77uIlm6WYbB-3K', verbose=False)

invoices = destination.model('account.invoice').browse([('sale_order', '=', False), ('reference', '!=', False)])

references = []
for invoice in invoices:
        references.append(invoice.reference)

print 'References ({}) fetched'.format(len(invoices))

sale_orders = destination.model('sale.order').browse([('name', 'in', references)])

maps = {}

for sale_order in sale_orders:
        maps[sale_order.name] = sale_order.id

print 'Sale orders ({}) fetched'.format(len(sale_orders))

total = 0

for invoice in invoices:
        if maps.has_key(invoice.reference):
                invoice.write({'sale_order': maps[invoice.reference]})
                print '({}), Updated invoice ({}) with sale order ({}, {})'.format(total, invoice.id, maps[invoice.reference], invoice.reference)
                total += 1
