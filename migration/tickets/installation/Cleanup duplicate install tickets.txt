UPDATE ticket_ticket T
SET active = false
WHERE 
T.type = 'install'
AND (SELECT id FROM ticket_ticket Q WHERE Q.sale_order_id = T.sale_order_id AND Q.active = true AND Q.type = 'install' ORDER BY id ASC LIMIT 1) != T.id;

SELECT S.id FROM sale_order S
WHERE
S.is_credit = true AND
S.state IN ('approved', 'done', 'progress') AND S.deposit_paid = true AND
(SELECT COUNT(T.id) FROM ticket_ticket T WHERE T.sale_order_id = S.id AND T.state != 'cancel' AND T.active = true AND T.type = 'install') > 1;

SELECT id FROM ticket_ticket WHERE sale_order_id IS NULL AND type = 'install' AND active = true;