from datetime import date
import erppeek
import threading
import math
import sys

#do not close these ones
notin = ['SO026199','SO026398','SO026147','SO026118','SO025797','SO026433','SO026021','SO026285','SO025923','SO025621','SO026282','SO026298','SO025190','SO020552','SO025094','SO026289','SO025753','SO026175','SO026256','SO026283','SO024356','SO026099','SO026383','SO026327','SO026328','SO025846','SO026291','SO026271','SO025390','SO026381','SO026134','SO026065','SO026044','SO025582','SO025208','SO022704','SO020633','SO026059','SO026257','SO026263','SO026247','SO025318','SO026004','SO026054','SO026138','SO026235','SO026344','SO026087','SO026311','SO026260','SO026365','SO026363','SO026218','SO025375','SO026150','SO026334','SO026346','SO026332','SO026215','SO026266','SO025418','SO020591','SO026385','SO026304','SO026370','SO026427','SO026281','SO024810','SO026278','SO026230','SO025492','SO026411','SO026423','SO026404','SO025470','SO026217','SO024897','SO026100','SO026386','SO026267','SO024894','SO026453','SO026350','SO026469','SO024539','SO025911','SO026315','S0024893','SO026181','SO026428','SO026417']


thread_amount = 10
destination = erppeek.Client('http://localhost:8069', db='solarnow', user='admin', password='yEGLc77uIlm6WYbB-3K', verbose=False)

total = destination.model('ticket.ticket').browse([('type', '=', 'install')])
total_already_closed = destination.model('ticket.ticket').browse([('type', '=', 'install'),('state', '=', 'closed')])
total_to_close = destination.model('ticket.ticket').browse([('type', '=', 'install'),('state', '!=', 'closed'), ('state', '!=', 'cancel'), ('sale_order_id', 'not in', notin), ('create_date', '<', '2016-07-01 00:00:00')])

print "Total installation tickets (%s)..." % len(total)
print "Total already closed (%s)..." % len(total_already_closed)
print "Total to close (%s)..." % len(total_to_close)

total_to_close.write({'state': 'closed'})
print "Tickets closed"
class CloseThread(threading.Thread):
	ids = [];
	destination = False;

	def __init__(self, ids, destination):
		threading.Thread.__init__(self)
		self.ids = ids;
		self.destination = destination

	def run(self):
		tickets_all = self.destination.model('ticket.ticket').browse([('id', 'in', self.ids)])

		print "Closing stock operations (stock.picking) associated with tickets (%s)...." % len(tickets_all)

		total_tickets_done = 0
		for ticket in tickets_all:
			if ticket.sale_order_id:
				sale_order = self.destination.model('sale.order').browse([('id', '=', ticket.sale_order_id.id)])
				if sale_order.procurement_group_id.id[0]:
					stock_pickings = self.destination.model('stock.picking').browse([('group_id', '=', sale_order.procurement_group_id.id)])
					print "Closing %s stock operations" % len(stock_pickings)
					i = 0
					try:
						j = 0
						while(len(self.destination.model('stock.picking').search([('group_id', '=', sale_order.procurement_group_id.id), ('state', '!=', 'done')])) != 0 and j < 10):
							for stock_picking in stock_pickings:
								if stock_picking.state == 'partially_available':
									self.destination.execute('stock.picking', 'force_assign', [stock_picking.id])
									self.destination.execute('stock.picking', 'do_transfer', [stock_picking.id])
								elif stock_picking.state == 'confirmed':
									self.destination.execute('stock.picking', 'action_assign', [stock_picking.id])
									self.destination.execute('stock.picking', 'do_transfer', [stock_picking.id])
								elif stock_picking.state == 'assigned':
									self.destination.execute('stock.picking', 'do_transfer', [stock_picking.id])
								i += 1
							j += 1
					except:	
						print "ERROR"
					not_closed_pickings = self.destination.model('stock.picking').browse([('group_id', '=', sale_order.procurement_group_id.id), ('state', '!=', 'done')])
					if(len(not_closed_pickings) != 0):
						print "Not all pickings closed for ticket %s" % ticket.id;
					print "Closed %s stock operations of ticket %s" % (i, ticket.id)
			total_tickets_done += 1
			print "(%s/%s)" % (total_tickets_done, len(tickets_all))


ids = destination.model('ticket.ticket').search([('type', '=', 'install'),('sale_order_id', 'not in', notin), ('create_date', '<', '2016-07-01 00:00:00')])
print "Total tickets %s" % len(ids)
parts = int(math.ceil(float(len(ids)) / float(thread_amount)))
threads = []
destinations = []
for i in range(0, thread_amount):
	destination_ = erppeek.Client('http://localhost:8069', db='solarnow', user='admin', password='yEGLc77uIlm6WYbB-3K', verbose=False)
	thread = CloseThread(ids[i*parts:i*parts+parts], destination_)
	thread.start()
	threads.append(thread)

for thread in threads:
    thread.join()