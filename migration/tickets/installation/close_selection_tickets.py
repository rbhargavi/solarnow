from datetime import date
import erppeek
import threading
import math
import sys

selection = [21502,21563,21559,21464,17187,16195,18876,19001,16496,16382,19080,16506,16867,17639,17575,17953,17638,16292,17570,17542,18454,18235,16821,16570,17505,17245]

thread_amount = 3
destination = erppeek.Client('http://213.187.242.232:8069', db='test_1106', user='admin', password='admin@', verbose=False)

class CloseThread(threading.Thread):
	ids = [];
	destination = False;

	def __init__(self, ids, destination):
		threading.Thread.__init__(self)
		self.ids = ids;
		self.destination = destination

	def run(self):
		tickets_all = self.destination.model('ticket.ticket').browse([('id', 'in', self.ids)])

		print "Closing stock operations (stock.picking) associated with tickets (%s)...." % len(tickets_all)

		total_tickets_done = 0
		for ticket in tickets_all:
			if ticket.sale_order_id:
				sale_order = self.destination.model('sale.order').browse([('id', '=', ticket.sale_order_id.id)])
				if sale_order.procurement_group_id.id[0]:
					stock_pickings = self.destination.model('stock.picking').browse([('group_id', '=', sale_order.procurement_group_id.id)])
					print "Closing %s stock operations" % len(stock_pickings)
					i = 0
					try:
						j = 0
						while(len(self.destination.model('stock.picking').search([('group_id', '=', sale_order.procurement_group_id.id), ('state', '!=', 'done')])) != 0 and j < 10):
							for stock_picking in stock_pickings:
								if stock_picking.state == 'partially_available':
									self.destination.execute('stock.picking', 'force_assign', [stock_picking.id])
									self.destination.execute('stock.picking', 'do_transfer', [stock_picking.id])
								elif stock_picking.state == 'confirmed':
									self.destination.execute('stock.picking', 'action_assign', [stock_picking.id])
									self.destination.execute('stock.picking', 'do_transfer', [stock_picking.id])
								elif stock_picking.state == 'assigned':
									self.destination.execute('stock.picking', 'do_transfer', [stock_picking.id])
								i += 1
							j += 1
					except:	
						print "ERROR"
					not_closed_pickings = self.destination.model('stock.picking').browse([('group_id', '=', sale_order.procurement_group_id.id), ('state', '!=', 'done')])
					if(len(not_closed_pickings) != 0):
						print "Not all pickings closed for ticket %s" % ticket.id;
					print "Closed %s stock operations of ticket %s" % (i, ticket.id)
			total_tickets_done += 1
			print "(%s/%s)" % (total_tickets_done, len(tickets_all))


ids = destination.model('ticket.ticket').search([('type', '=', 'install'),('id', 'in', selection)]) #type=install just to make sure
print "Total tickets %s" % len(ids)
parts = int(math.ceil(float(len(ids)) / float(thread_amount)))
threads = []
destinations = []
for i in range(0, thread_amount):
	destination_ = erppeek.Client('http://213.187.242.232:8069', db='test_1106', user='admin', password='admin@', verbose=False)
	thread = CloseThread(ids[i*parts:i*parts+parts], destination_)
	thread.start()
	threads.append(thread)

for thread in threads:
    thread.join()