from datetime import date
import erppeek

source = erppeek.Client('http://85.222.224.227:8069/', db='o6_sn_live_2016jan29', user='admin', password='op3n@dmin2011', verbose=False)
target = erppeek.Client('http://213.187.242.232:8069', db='ticket_migration_4', user='admin', password='admin@', verbose=False)

sale_order_without_tickets = 0
sale_order_not_found = 0
migrated_sale_orders = 0

sale_orders = target.model('sale.order').browse([('deposit_paid', '=', True), ('is_credit', '=', True)])
for sale_order in sale_orders:
	installation_tickets = target.model('ticket.ticket').browse([('sale_order_id', '=', sale_order.id)])
	if not installation_tickets:
		state = 'open'
		installation_date = False
		if sale_order._external_id:
			migrated_sale_orders += 1
			external_id = sale_order._external_id.split('.')
			ir_model_data = source.model('ir.model.data').browse([('name', '=', external_id[1]), ('module', '=', external_id[0])])
			sale_order_source = False
			if ir_model_data:
				sale_order_source = source.model('sale.order').browse([('id', '=', ir_model_data.res_id)])
			else: #If is migrated but not found
				sale_order_source = source.model('sale.order').browse([('name', '=', sale_order.name)])
				if not sale_order_source:
					sale_order_not_found += 1
					print 'Migrated or exported (probably exported) sale order not found %s, %s' % (sale_order._external_id, sale_order.name)
			
			installation_date = sale_order_source.payplan_id.installation_date
			approved_date = sale_order_source.payplan_id.installation_report_approved_date
			if installation_date and approved_date:
				state = 'close'
			elif installation_date:
				state = 'done'
			else:
				state = 'open'
				
		sale_order_without_tickets += 1
		new_ticket = { 
			'x_create_date': sale_order.write_date, 
			'type': 'install',
			'sale_order_id': sale_order.id,
			'partner_id': sale_order.partner_id,
			'state': state,
			'installation_date': installation_date,
			'installation_user_1_id': False,
			'installation_user_2_id': False,
			'name': 'Installation of %s' % (sale_order.name)
		}
		#target.model('ticket.ticket').create(new_ticket)
print 'Sales orders without an installation ticket %s' % sale_order_without_tickets
print 'Sales orders not found %s' % sale_order_not_found
print 'Sales orders migrated %s' % migrated_sale_orders