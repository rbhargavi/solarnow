from datetime import date
import erppeek

source = erppeek.Client('https://sas.solarnow.eu', db='solarnow', user='admin', password='yEGLc77uIlm6WYbB-3K', verbose=False)

orders = source.model('sale.order').browse([('state', 'in', ('approved','progress','manual','done')), ('deposit_paid', '=', True)])
print "%s sales orders found" % len(orders)
i = 0

for order in orders:
	if order.procurement_group_id:
		invoices = source.model('account.invoice').browse([('sale_order', '=', order.id)])
		pickings = source.model('stock.picking').browse([('group_id', '=', order.procurement_group_id.id)])
		if len(invoices) == 0:
			print "Creating invoice for %s..." % order.name
			source.exec_workflow('sale.order', 'manual_invoice', order.id)
			print "Invoice created" 
		else:
			print "Sales order %s has already an invoice" % order.name
					
		if len(pickings) == 0:
			print "Shipping sales order %s..." % order.name
			source.exec_workflow('sale.order', 'ship', order.id)
			print "Sales order shipped"
		else:
			print "Sales order %s is already shipped" % order.name
			
		i += 1
	else:
		print "Order has no procurement_group_id trying to ship it..."
		source.exec_workflow('sale.order', 'ship', order.id)
		print "Shipping succeeded"
