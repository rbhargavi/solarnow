SELECT S.id AS sale_order_id, T.id AS ticket_id, (SELECT COUNT(*) FROM stock_picking SP WHERE SP.state != 'done' AND SP.group_id = S.procurement_group_id) AS number_of_pickings
FROM ticket_ticket T
JOIN sale_order S ON T.sale_order_id = S.id
WHERE 
T.type = 'install' AND 
T.state = 'closed' AND
T.active = TRUE AND 
(SELECT COUNT(*) FROM stock_picking SP WHERE SP.state != 'done' AND SP.group_id = S.procurement_group_id) > 0;

213.187.242.232 test_1106

 sale_order_id | ticket_id | number_of_pickings
---------------+-----------+--------------------
         46107 |     21502 |                  1
         46155 |     21563 |                  2
         46150 |     21559 |                  1
         45735 |     21464 |                  1
         42376 |     17187 |                  2
         40853 |     16195 |                  1
         44367 |     18876 |                  1
         44122 |     19001 |                  1
         36737 |     16496 |                  3
         36727 |     16382 |                  3
         41858 |     19080 |                  1
         41556 |     16506 |                  3
         42287 |     16867 |                  2
         43082 |     17639 |                  1
         41561 |     17575 |                  1
         42516 |     17953 |                  3
         43158 |     17638 |                  3
         40696 |     16292 |                  1
         42350 |     17570 |                  1
         41142 |     17542 |                  1
         44408 |     18454 |                  1
         44085 |     18235 |                  1
         42355 |     16821 |                  2
         36352 |     16570 |                  3
         37186 |     17505 |                  3
         42389 |     17245 |                  2
		 
sas.solarnow.eu solarnow


		 
		 
