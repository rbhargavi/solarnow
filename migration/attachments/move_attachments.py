#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import logging
import sys

from argparse import ArgumentParser

parser = ArgumentParser(description="Move attachments from concerned models.")
parser.add_argument('odoo_basedir')
parser.add_argument('odoo_cfg')
parser.add_argument('odoo_db')


if __name__ == '__main__':
    oargs = parser.parse_args()
    sys.path.insert(0, oargs.odoo_basedir)


try:
    from openerp import SUPERUSER_ID, api, tools
    from openerp.sql_db import db_connect
except:
    raise


_logger = logging.getLogger(__name__)


def main(oargs):
    cr = db_connect(oargs.odoo_db).cursor()
    with api.Environment.manage():
        env = api.Environment(cr, SUPERUSER_ID, {})
        partners_application = env['res.partner'].search([('application', '!=', False)])
        _logger.info("[Applications] Processing {} records".format(len(partners_application)))
        for partner in partners_application:
            partner.move_binary_application()
        partners_credit = env['res.partner'].search([('credit_form', '!=', False)])
        _logger.info("[Credit Forms] Processing {} records".format(len(partners_credit)))
        for partner in partners_credit:
            partner.move_binary_credit_form()
        sales_contract = env['sale.order'].search([('contract', '!=', False)])
        _logger.info("[Sale Contracts] Processing {} records".format(len(sales_contract)))
        for sale in sales_contract:
            sale.move_binary_contract()
        tickets_installation_report = env['ticket.ticket'].search([('installation_report', '!=', False)])
        _logger.info("[Installation Report] Processing {} records".format(len(tickets_installation_report)))
        for ticket in tickets_installation_report:
            ticket.move_binary_installation_report()
    cr.commit()
    cr.close()

if __name__ == '__main__':
    tools.config.parse_config(['-c', oargs.odoo_cfg])
    main(oargs)
