from datetime import date
import erppeek

source = erppeek.Client('http://85.222.224.227:8069', db='o6_sn_live_2016jan29', user='admin', password='op3n@dmin2011', verbose=False)
target = erppeek.Client('https://sas.solarnow.eu', db='solarnow', user='admin', password='yEGLc77uIlm6WYbB-3K', verbose=False)

comments = source.model('payplan.contract.comment').browse([], limit=10)

for comment in comments:
	if comment.payplan_id.partner_id._external_id:
		external_name = comment.payplan_id.partner_id._external_id.split('.')[1]
		data = target.model('ir.model.data').browse([('name', '=', external_name)])
		if data:
			partner_id = data[0].res_id
			description = comment.comment
			migration_user_name = comment.user.name
			target_comments = target.model('partner.comment').browse([('name', '=', description), ('partner_id', '=', partner_id)])
			if not target_comments:
				external_name_user = comment.user._external_id.split('.')[1]
				data_user = target.model('ir.model.data').browse([('name', '=', external_name_user)])
				target_user = target.model('res.users').browse([('id', '=', data_user[0].res_id)])
				user_id = 1
				if target_user:
					user_id = target_user[0].id
				values = {'name': description, 'partner_id': partner_id, 'x_followup': comment.x_follow_date, 'x_create_date': comment.create_date, 'x_create_uid': user_id, 'x_payplan_contract_number': comment.payplan_id.name, 'x_create_uid_name': migration_user_name}
				target.model('partner.comment').create(values)
				print 'Created comment'
			else:
				target_comments.write({'x_create_uid_name': migration_user_name})
				print 'Comment already exists (write, %s)' % (migration_user_name)
		else:
			print 'Partner does not exists in target'
			