# -*- coding: utf-8 -*-

{
    'name': 'Authorize Advanced Integration Method (AIM)',
    'category': 'Hidden',
    'summary': 'Payment Acquirer: Authorize Advanced Integration Method (AIM) Implementation',
    'version': '1.0',
    'description': """AIM is Authorize.Net's recommended connection method and offers the most secure and flexible integration for all types of transactions, including mobile, websites and other business applications.""",
    'author': 'OpenERP SA',
    'depends': ['payment', 'website_payment'],
    'data': [
        'views/payment_acquirer_views.xml',
        'views/payment_acquirer_templates.xml',
        'data/payment_acquirer_data.xml',
    ],
    'installable': True,
}
