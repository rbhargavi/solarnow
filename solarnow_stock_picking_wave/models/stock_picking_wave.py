import time

from openerp import models, fields, api, _
from openerp.exceptions import Warning
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT

class StockPickingWave(models.Model):
    _inherit = 'stock.picking.wave'

    @api.multi
    def done(self):
        #stupid fix
        res = super(StockPickingWave, self).done()
        for wave in self:
            for picking in wave.picking_ids:
                if not picking.date_done:
                    picking.write({'date_done': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)})
        return res