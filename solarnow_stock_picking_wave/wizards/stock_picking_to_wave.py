from openerp import models, fields, api


class StockPickingToWave(models.TransientModel):
    _inherit = 'stock.picking.to.wave'

    @api.model
    def _default_picking_type_id(self):
        active_ids = self.env.context.get('active_ids')
        if active_ids:
            pickings = self.env['stock.picking'].browse(active_ids)
            for picking in pickings:
                if picking.picking_type_id.code == 'outgoing':
                    return picking.picking_type_id
        return self.browse()

    @api.model
    def _default_wave_name(self):
        active_ids = self.env.context.get('active_ids')
        if active_ids:
            pickings = self.env['stock.picking'].browse(active_ids)
            for picking in pickings:
                date = fields.Date.today()
                picking_name = picking.picking_type_id.name
                return date + ' - ' + picking_name
        return None

    wave_id = fields.Many2one('stock.picking.wave', 'Picking Wave', required=False)
    wave_name = fields.Char(string='New Wave Name', default=_default_wave_name)
    picking_type_id = fields.Many2one('stock.picking.type', default=_default_picking_type_id)
    action_type = fields.Selection(
        [('add', 'Add to existing wave'),
         ('create', 'Create a new wave')],
        default='add',
        string='Action')

    @api.multi
    def attach_pickings(self):
        self.ensure_one()
        picking_ids = self.env.context.get('active_ids', False)
        res = False
        if self.action_type == 'add':
            res = super(StockPickingToWave, self).attach_pickings()
        if self.action_type == 'create':
            wave_vals = {'name': self.wave_name}
            new_wave = self.env['stock.picking.wave'].create(wave_vals)
            res = self.pool.get('stock.picking').write(self._cr, self._uid, picking_ids, {'wave_id': new_wave.id})
            
        #FIXME
        #Calculate second stage
        picking_objs = self.env['stock.picking'].browse(picking_ids)
        for picking in picking_objs:
            if picking.picking_type_id:
                picking.name_stage2 = None
                if picking.group_id:
                    stages = self.env['stock.picking'].search(
                        [('group_id', '=', picking.group_id.id)])
                    if len(stages) == 4:
                        sorted_stages = stages.sorted(key=lambda r: r.id)
                        picking.write({'name_stage2': sorted_stages[1].name})
        return res
