openerp.stock_barcode_auto_increment = function(instance) {
    instance.stock.PickingEditorWidget.include({
        incremented_rows: false,
        replaceElement: function($el) {
            var res = this._super($el);
            this.increment_rows($el);
            return res;
        },
        increment_rows: function($el) {
            if (this.incremented_rows) return;
            for(var i = 0; i < this.rows.length; i++)
                this.getParent().set_operation_quantity(this.rows[i].cols.qty, this.rows[i].cols.id);
            this.incremented_rows = true;
        }
    });
}