# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp import models, fields, api, _

_logger = logging.getLogger(__name__)


class hr_contract_bonus(models.Model):
    _name = 'hr.contract.bonus'
    _description = 'Contract bonus'

    name = fields.Char('Bonus', compute="_compute_name", store=True)
    hr_contract_id = fields.Many2one('hr.contract', string='Contract')
    month = fields.Selection([
            ('01', 'January'),
            ('02', 'February'),
            ('03', 'March'),
            ('04', 'April'),
            ('05', 'May'),
            ('06', 'June'),
            ('07', 'July'),
            ('08', 'August'),
            ('09', 'September'),
            ('10', 'October'),
            ('11', 'November'),
            ('12', 'December')
        ], string='Month')
    year = fields.Char()
    amount = fields.Float()

    @api.one
    @api.depends("month", "year")
    def _compute_name(self):
        self.name = "{} {}".format(self.month, self.year)


class hr_contract(models.Model):
    _inherit = 'hr.contract'

    trial_salary = fields.Float("Probation salary")
    monthly_allowance = fields.Float("Monthly allowance")
    hr_contract_bonus_ids =  fields.One2many('hr.contract.bonus', 'hr_contract_id', string='Bonuses')

    amount_before_tax = fields.Float('Amount Before Tax')
    amount_after_tax = fields.Float("Amount After Tax")
