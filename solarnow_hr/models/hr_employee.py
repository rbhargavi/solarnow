# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields


class hr_employee(models.Model):
    _inherit = 'hr.employee'

    # branch_id = fields.Many2one('res.branch', string="Branch") # Moved to solarnow_base
    signature = fields.Binary("Signature", help="Employee'signature")
    address_emergency_id = fields.Many2one('res.partner', string='Emergency Address')
    language_ids = fields.Many2many('hr.employee.language', string='Language')
    certification_ids = fields.One2many('hr.employee.certification.rel', 'employee_id')

    driving_licence = fields.Boolean(
        string="Driving licence",
        default=False)
    emergency_relation = fields.Char('Relation to employee')

    #Mobile money info
    mtn_phone = fields.Char("MTN phone number")
    mtn_bundle_size = fields.Float("MTN bundle size")
    mobile_money_operator = fields.Char("Mobile money operator")  # Maybe better m2o
    mobile_money_number = fields.Char("Mobile money number")
    mobile_money_reg_name = fields.Char("Mobile money registered name")
    sale_team_id = fields.Many2many("crm.case.section", string="Sales Team")
    personal_phone1 = fields.Char("Personal phone number 1")
    personal_phone2 = fields.Char("Personal phone number 2")
    private_email = fields.Char("Private email")
    nssf_number = fields.Char("NSSF number")
