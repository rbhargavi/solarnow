# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "Solarnow HR",
    'summary': """Solarnow HR module""",
    'description': """
    Solarnow HR customizations.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.3',

    'depends': [
        'hr',
        'hr_recruitment',
        'hr_evaluation',
        'hr_payroll',
        'hr_holidays',
        'hr_employee_related',
        'sales_team',
        'solarnow_base',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_employee_language.xml',
        'views/hr_employee_view.xml',
        'views/hr_certification.xml',
        'views/hr_contract_view.xml',
        'views/res_partner_view.xml',
        'menu_items.xml',
    ],
    'qweb': [
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
