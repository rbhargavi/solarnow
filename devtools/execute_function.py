##################################################################################
#                                                                                #
#    Copyright (C) 2011 Synkronized (<http://synkronized.be>).                   #
#                                                                                #
#    This program is free software: you can redistribute it and/or modify        #
#    it under the terms of the GNU Affero General Public License as              #
#    published by the Free Software Foundation, either version 3 of the          #
#    License, or (at your option) any later version.                             #
#                                                                                #
#    This program is distributed in the hope that it will be useful,             #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#    GNU Affero General Public License for more details.                         #
#                                                                                #
#    You should have received a copy of the GNU Affero General Public License    #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                                #
##################################################################################

from openerp.osv import osv, fields
import logging

from openerp.addons.solarnow_base.model.util import collect_and_reconcile


_logger = logging.getLogger(__name__)

class execute_function(osv.osv_memory):
    _name = 'devtools.execute_function'

    _columns = {
        'object': fields.char('Model', size=128),
        'function': fields.char('Function', size=128),
        'args': fields.text('Args'),
        'result': fields.text('Result')
        }

    _defaults = {
        'object': lambda *a: 'devtools.execute_function',
        'function': lambda *a: "invoice_manual_but_approved_orders",
        'args': lambda *a: "[[]]"
        }

    # def adjust_maturity_dates(self, cr, uid, ids, context=None):
    #     if context is None:
    #         context = {}

    #     cr.execute("""
    #        select * from account_invoice where payplan_date is null
    #        and sale_order in (
    #        select id from sale_order where procurement_group_id is not null
    #        )
    #     """)

    #     return True

    def book_balance(self, cr, uid, ids, context=None):
        cr.execute("""
            select res_partner.ref, res_partner.id, one_running_contract.invoice_id, one_running_contract.residual, balance_123.balance
            from
            (
            select partner_id, sum(debit-credit) as balance
            from account_move_line
            where partner_id is not null
            and account_id = 4920
            group by partner_id
            having sum(debit-credit) < 0
            ) as balance_123
            left join res_partner on (balance_123.partner_id = res_partner.id)
            left join (
            select partner_id, max(id) as invoice_id, sum(residual) as residual
            from account_invoice
            where residual > 0 
            group by partner_id
            having count(*) = 1
            ) as one_running_contract on (balance_123.partner_id = one_running_contract.partner_id)
            where residual > 0
        """)
        src_account = self.pool.get('account.account').browse(cr, uid, [4920])[0]
        dst_account = self.pool.get('account.account').browse(cr, uid, [4915])[0]

        for row in cr.fetchall():
            try:
                partner = self.pool.get('res.partner').browse(cr, uid, [row[1]])[0]
                move = partner.book_balance(src_account, dst_account, max_amount=row[3])
                invoice = self.pool.get('account.invoice').browse(cr, uid, [row[2]])[0]

                if move:
                    lines = [l for l in move.line_id if l.name and l.name.startswith('dst Moving balance') and l.account_id.id == invoice.account_id.id]
                    if lines:
                        for l in sorted([l for l in invoice.move_id.line_id if l.payplan_type in ('interest','principal','downpayment') and l.account_id.id == invoice.account_id.id and not l.reconcile_id], key=lambda l:l.date_maturity):
                            if sum([l2.debit - l2.credit for l2 in lines]) >= 0:
                                break
                            lines.append(l)

                        collect_and_reconcile(invoice,
                                              lines,
                                              account_ids=[lines[0].account_id.id],
                                              partial=True
                                              )
 
                    invoice.action_rereconcile()

                cr.commit()
                _logger.info('Moved balance for partner %s', row[0])
            except Exception, x:
                _logger.error('Could not move balance for partner %s (%s)', row[0], str(x))
                cr.rollback()
            

    def all_scripts(self, cr, uid, ids, context=None):
        self.correct_one_shilling_moves(cr, uid, ids, context=context)
        self.rereserve_downpayment(cr, uid, ids, context=context)
        self.retry_leftover_amounts(cr, uid, ids, query=0, context=context)
        self.rereconcile_invoices(cr, uid, ids, context=context)
        return True

    def remove_move_balance(self, cr, uid, ids, context=None):
        cr.execute("""
            select id, reconcile_id
            from account_move_line 
            where ref ilike '%moving balance%'
        """)
        for row in cr.fetchall():
            _logger.info("Unreconciling move line %d", row[0])
            self.pool.get('account.move.line')._remove_move_reconcile(cr, uid, [row[0]])

        cr.execute("""
            select id
            from account_move
            where ref ilike '%moving balance%'
        """)
        for row in cr.fetchall():
            _logger.info("Removing move %d", row[0])
            self.pool.get('account.move').button_cancel(cr, uid, [row[0]])
            self.pool.get('account.move').unlink(cr, uid, [row[0]])

            

    def rereserve_downpayment(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        context['no_balance_check'] = True

        cr.execute("""
            select sale_order.id, account_invoice.id
            from sale_order 
            left join account_move_line on (downpayment_line_id = account_move_line.id)
            left join account_journal on (account_move_line.journal_id = account_journal.id)
            inner join account_invoice on (account_invoice.sale_order = sale_order.id and account_invoice.type = 'out_invoice')
            where (account_journal.type = 'bank' or account_move_line.id is null)
            and sale_order.id in (select id from sale_order where procurement_group_id is not null)
        """)
        for row in cr.fetchall():
            _logger.info("Redoing downpayment for order %d move %d", row[0],row[1])
            sale = self.pool.get('sale.order').browse(cr, uid, [row[0]], context=context)[0]
            invoice = self.pool.get('account.invoice').browse(cr, uid, [row[1]], context=context)[0]
            try:
                if sale.downpayment_line_id:
                    sale.downpayment_line_id._remove_move_reconcile(move_ids=[sale.downpayment_line_id.id])
                sale.write({'downpayment_line_id': False})
                for l in invoice.move_id.line_id:
                    if l.payplan_type == 'downpayment':
                        booking_lines = [line for line in l.reconcile_id.line_id if line.name == 'downpayment for /']
                        assert len(booking_lines) == 1
                        assert len(booking_lines[0].move_id.line_id) == 2
                        l._remove_move_reconcile(move_ids=[l.id])
                        for line in booking_lines[0].move_id.line_id:
                            line._remove_move_reconcile(move_ids=[line.id])
                        booking_lines[0].move_id.button_cancel()
                        booking_lines[0].move_id.unlink()
                        break # there should only be one downpayment line

                invoice.book_downpayment()
                assert sale.downpayment_line_id

                cr.commit()
            except Exception, x:
                print x
                _logger.error('Could not redo downpayment  for order %d move %d', row[0], row[1])
                cr.rollback()

        return True

    def undo_moving_balance(self, cr, uid, ids, context=None):
        return

        cr.execute("""
            select id from account_move where ref like 'Moving balance for %'
        """)
        for row in [r for r in cr.fetchall()]:
            try:
                move = self.pool.get('account.move.line').browse(cr, uid, [row[0]], context=context)
                move.button_cancel()
                _logger.info("Reserved downpayment for  sale %d", row[0])
                cr.commit()
            except Exception:
                cr.rollback()
                _logger.error("Error Reserving downpayment for  %d", row[0])
        return True

    def rereconcile_invoices(self, cr, uid, ids, context=None):
        cr.execute("""
            select distinct(id) from account_invoice where move_id in (
            select distinct(aml.move_id) from account_move_line aml
            left join account_move_line aml2 on (aml.move_id = aml2.move_id)
            where (aml.reconcile_id is not null or aml.reconcile_partial_id is not null)
            and ((aml2.reconcile_id is null and aml2.reconcile_partial_id is null)
	    or (aml.reconcile_id is not null and aml2.reconcile_id is null)
	    or (aml.reconcile_partial_id is not null and aml2.reconcile_partial_id is not null and aml.reconcile_partial_id != aml2.reconcile_partial_id)
            ) 
            and aml2.date_maturity < aml.date_maturity
            and aml.payplan_type in ('principal', 'interest') and aml2.payplan_type in ('principal', 'interest')
            )
        """)
        for row in [r for r in cr.fetchall()]:
            try:
                _logger.info("Trying to rereconcile invoice id %d", row[0])
                self.pool.get('account.invoice').browse(cr, uid, [row[0]], context=context).rereconcile()
                cr.commit()
            except Exception:
                cr.rollback()
                _logger.error("Error rereconciling move %d", row[0])
                
        return True

    def post_migrated_payplans(self, cr, uid, ids, context=None):
        move_pool = self.pool.get('account.move')
        cr.execute("""
            select id from account_move where state = 'draft' and id in (select move_id from account_invoice where state not in ('draft','cancel'))
        """)
        for row in [r for r in cr.fetchall()]:
            try:
                move_pool.post(cr, uid, row[0])
                cr.commit()
            except Exception:
                cr.rollback()
                _logger.error("Error posting move %d", row[0])
        return True

    def retry_leftover_amounts(self, cr, uid, ids, query=0, context=None):
        queries = [
            """
            select account_bank_statement_line.id
            from account_bank_statement_line
            inner join account_move_line on (account_move_line.move_id = account_bank_statement_line.journal_entry_id)
            inner join account_account on (account_move_line.account_id = account_account.id)
            inner join res_partner on (account_bank_statement_line.partner_id = res_partner.id)
            where account_bank_statement_line.partner_id is not null
            and account_account.parent_id != 4910 -- bank accounts
            and account_move_line.reconcile_id is null and account_move_line.reconcile_partial_id is null
            and account_bank_statement_line.amount > 0
            -- and res_partner.ref not like 'E1%'
	    except 
            select account_bank_statement_line.id
            from account_bank_statement_line
            inner join account_move_line on (account_move_line.move_id = account_bank_statement_line.journal_entry_id)
            inner join account_account on (account_move_line.account_id = account_account.id)
            where account_bank_statement_line.partner_id is not null
            and account_account.parent_id != 4910 -- bank accounts
            and (account_move_line.reconcile_id is not null or account_move_line.reconcile_partial_id is not null)
	    order by id
            """,
            """
            select account_bank_statement_line.id 
            from account_bank_statement_line
            inner join account_move_line on (account_move_line.move_id = account_bank_statement_line.journal_entry_id)
            inner join account_account on (account_move_line.account_id = account_account.id)
            where account_bank_statement_line.partner_id is not null
            and account_account.parent_id != 4910 -- bank accounts
            and account_move_line.reconcile_id is null and account_move_line.reconcile_partial_id is null
            and manual_processing = true
            """

            ]
        bank_statement_line_pool = self.pool.get('account.bank.statement.line')
        bank_statements = []
        cr.execute(queries[query])
        for line_dict in bank_statement_line_pool.read(cr, uid, [r[0] for r in cr.fetchall()], ['statement_id']):
            line_id = line_dict['id']
            st_id = line_dict['statement_id'][0]

            if st_id not in bank_statements:
                self.pool.get('account.bank.statement').button_draft(cr, uid, [st_id])

            try:
                bank_statement_line_pool.write(cr, uid, [line_id], {'manual_processing': False})
                bank_statement_line_pool.cancel(cr, uid, [line_id])
                bank_statement_line_pool.guess_payplan_reconciliation(cr, uid, [line_id])
                bank_statement_line_pool.process(cr, uid, [line_id])
                cr.commit()
                bank_statements.append(st_id)
                _logger.info("success retrying %d", line_id)
            except Exception, e:
                cr.rollback()
                print e
                _logger.error("Error wile retrying bank_statement_line %d", line_id)
        return True

    def reapprove_cash_orders(self, cr, uid, ids, context=None):
        sale_pool = self.pool.get('sale.order')
        sales = []
        cr.execute("""
            select sale_order.id from sale_order 
            inner join wkf_instance on (res_type = 'sale.order' and res_id = sale_order.id)
            inner join wkf_workitem on (inst_id = wkf_instance.id)
            where act_id = 57 and sale_order.state = 'approved'
        """)
        count = 0
        for sale in sale_pool.browse(cr, uid, [t[0] for t in cr.fetchall()]):
            try:
                self.pool.get('sale.order').write(cr, uid, [sale.id], {'state': 'approval'})
                sale_pool.signal_workflow(cr, uid, [sale.id], 'approved')
                sales.append(sale.id)
                _logger.info('Success confirming %d', sale.id)
            except Exception, e:
                _logger.error('Error confirming %d', sale.id)
                cr.rollback()
            cr.commit()
        _logger.info("Succesfully confirmed %r", sales)
                        
    def book_ucccu_discounts(self, cr, uid, ids, context=None):
        cr.execute("""
            select id from account_invoice where move_id in 
            (select move_id from account_move_line where payplan_type='subsidy' and reconcile_id is null)
        """)
        for invoice in self.pool.get('account.invoice').browse(cr, uid, [t[0] for t in cr.fetchall()]):
            invoice._book_discount_on_invoice()

        return None

    def ship_approved_orders(self, cr, uid, ids, context=None):
        sale_pool = self.pool.get('sale.order')
        for sale_id in sale_pool.search(cr, uid, [('state','=','approved')]):
            try:
                print 'confirming', sale_id
                sale_pool.signal_workflow(cr, uid, [sale_id], 'ship')
                print 'success'
            except:
                print 'failure'
                cr.rollback()
            cr.commit()

    def correct_one_shilling_moves(self, cr, uid, ids, context=None):
        cr.execute("""
            select account_move.id
            from account_move 
            inner join account_move_line on (account_move_line.move_id = account_move.id)
            where account_move_line.state = 'draft' 
            and account_move.id in (select move_id from account_invoice where state not in ('draft','cancel'))
            group by account_move.id
            having abs(sum(debit - credit)) = 1
        """)
        for r in cr.fetchall():
            try:
                move = self.pool.get('account.move').browse(cr, uid, [r[0]], context=context)[0]
                balance = sum([l.debit - l.credit for l in move.line_id])
                self.pool.get('account.move.line').create(cr, uid,{
                        'name': 'write-off',
                        'move_id': move.id,
                        'credit': balance > 0 and balance or 0,
                        'debit': balance < 0 and abs(balance) or 0,
                        'account_id': 6029
                        })
                move.button_validate()
                _logger.info("Validated move %d", move.id)
            except Exception, x:
                _logger.error("Error validating move %d", move.id)
                cr.rollback()
            cr.commit()
            

    def invoice_manual_but_approved_orders(self, cr, uid, ids, context=None):
        sales = []
        sale_pool = self.pool.get('sale.order')

        cr.execute("""
        select sale_order.id from sale_order 
        inner join wkf_instance on (res_type = 'sale.order' and res_id = sale_order.id)
        inner join wkf_workitem on (inst_id = wkf_instance.id)
        where act_id = 58 and sale_order.state = 'manual'
        """)
        count = 0
        for sale in self.pool.get('sale.order').browse(cr, uid, [t[0] for t in cr.fetchall()]):
            try:
                print 'confirming', sale.id
                self.pool.get('sale.order').write(cr, uid, [sale.id], {'state': 'approved'})
                sale_pool.signal_workflow(cr, uid, [sale.id], 'ship')
                sales.append(sale.id)
                print 'success'
            except Exception, e:
                print e
                print 'failure'
                cr.rollback()
            cr.commit()

        print 'successful: ', sales

    def invoice_manual_orders(self, cr, uid, ids, context=None):
        sale_pool = self.pool.get('sale.order')
        for sale_id in sale_pool.search(cr, uid, [('state','=','manual')]):
            try:
                print 'confirming', sale_id
                sale_pool.signal_workflow(cr, uid, [sale_id], 'manual_invoice')
                print 'success'
            except:
                print 'failure'
                cr.rollback()
            cr.commit()


    def fix_maturity_dates(self, cr, uid, ids, context=None):
        cr.execute("""
        select id
        from account_invoice 
        where date_invoice > '2016-01-29'
        and is_credit = true and residual > 0
        """)
        count = 0
        for invoice in self.pool.get('account.invoice').browse(cr, uid, [t[0] for t in cr.fetchall()]):
            payplan_date = None
            #if invoice.sale_order and invoice.sale_order.downpayment_line_id:
            #    payplan_date = invoice.sale_order.downpayment_line_id.date_paid
            ids = self.pool.get('account.bank.statement.line').search(cr, uid, [('partner_id','=',invoice.partner_id.id)])
            if ids:
                for bsl in self.pool.get('account.bank.statement.line').browse(cr, uid, ids):
                    if bsl.date == invoice.payplan_date:
                        payplan_date = bsl.date

            if invoice.payplan_date != payplan_date:
                count += 1
                print [l.date for l in self.pool.get('account.bank.statement.line').browse(cr, uid, ids)]
                print invoice.payplan_date, payplan_date, invoice.payplan_date == payplan_date, invoice.id

        print count

    def do_reconcile(self, cr, uid, ids, context=None):
        for rec_id in [d['reconcile_partial_id'][0] for d in self.pool.get('account.move.line').read(cr, uid, self.pool.get('account.move.line').search(cr, uid, [('reconcile_partial_id','!=',False)]), ['reconcile_partial_id'])]:
            self.pool.get('account.move.reconcile').reconcile_partial_check(cr, uid, rec_id)
        return True
        
    def do_execute(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        obj_ref = self.pool.get(data.object)
        executable = getattr(obj_ref, data.function)
        args = eval(data.args)
        r = executable(cr, uid, *args)
        self.write(cr, uid, ids, {'result': str(r)})

        obj_model = self.pool.get('ir.model.data')
        model_data_ids = obj_model.search(cr,uid,[('model','=','ir.ui.view'),('name','=','view_devtools_execute_function_result')])
        resource_id = obj_model.read(cr, uid, model_data_ids, fields=['res_id'])[0]['res_id']
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'devtools.execute_function',
            'views': [(resource_id,'form')],
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': context,
            'active_id': ids[0]
        }

execute_function()
