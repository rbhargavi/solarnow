##################################################################################
#                                                                                #
#    Copyright (C) 2011 Synkronized (<http://synkronized.be>).                   #
#                                                                                #
#    This program is free software: you can redistribute it and/or modify        #
#    it under the terms of the GNU Affero General Public License as              #
#    published by the Free Software Foundation, either version 3 of the          #
#    License, or (at your option) any later version.                             #
#                                                                                #
#    This program is distributed in the hope that it will be useful,             #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#    GNU Affero General Public License for more details.                         #
#                                                                                #
#    You should have received a copy of the GNU Affero General Public License    #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                                #
##################################################################################

from openerp.osv import osv, fields

class updater(osv.osv_memory):
    _name = 'devtools.updater'

    _columns = {
        'model': fields.char('model', size=128),
        'field': fields.char('field', size=128, default='name')
        }

    def do_update(self, cr, uid, ids, context=None):
        updater = self.browse(cr, uid, ids)[0]

        ir_model_field_ref = self.pool.get('ir.model.fields')
        ir_model_field_ids = ir_model_field_ref.search(cr, uid, [('model','=',updater.model),('name','=',updater.field)])
        if len(ir_model_field_ids) != 1:
            raise osv.except_osv("Error", "Field not found, or not unique !")
        ir_model_field = ir_model_field_ref.browse(cr, uid, ir_model_field_ids)[0]

        model_ref = self.pool.get(updater.model)
        model_ids = model_ref.search(cr, uid, [])
        model_data = model_ref.read(cr, uid, model_ids, [updater.field])

        if ir_model_field.ttype == 'one2many':
            raise osv.except_osv("Error", "'%s' is not a suitable field type" % ir_model_field.ttype)

        for rec in model_data:
            if ir_model_field.ttype == 'many2one' and rec[updater.field]:
                model_ref.write(cr, uid, [rec['id']], {updater.field: rec[updater.field][0]})
            elif ir_model_field.ttype == 'many2many':
                model_ref.write(cr, uid, [rec['id']], {updater.field: [(6,0,rec[updater.field])]})
            else:
                model_ref.write(cr, uid, [rec['id']], {updater.field: rec[updater.field]})
        return {}

updater()
