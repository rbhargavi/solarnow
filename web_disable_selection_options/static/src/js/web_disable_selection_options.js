openerp.web_disable_selection_options = function(instance, local) {
    var _t = instance.web._t,
       _lt = instance.web._lt;
    var QWeb = instance.web.qweb;
    
    instance.web.form.FieldSelection.include({
        render_value: function() {
            var self = this;
            this._super.apply(this, arguments);
            if(this.options && this.options.disabled_options) {
                for(var i = 0; i < this.options.disabled_options.length; i++) {
                    this.$("select option").each(function() {
                        if($(this).attr('value').replace("\"", "").replace("\"", "") == self.options.disabled_options[i]) {
                            $(this).attr('disabled', 'disabled');
                        }
                    });
                }
            }
        }
    });
}