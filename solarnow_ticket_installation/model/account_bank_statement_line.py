
from openerp import models, api, _


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    @api.multi
    def cancel(self):
        res = super(AccountBankStatementLine, self).cancel()
        for line in self:
            if line.payplan_reconciliation and line.payplan_reconciliation_summary.lower().find('downpayment') != -1:
                downpayment_so = line.payplan_reconciliation_summary.split()[-1]
                install_ticket = self.env['ticket.ticket'].search([('sale_order_id', '=', downpayment_so), ('type', '=', 'install'), ('state', 'not in', ('done', 'closed'))])
                install_ticket.installation_set_cancel()
        return res
