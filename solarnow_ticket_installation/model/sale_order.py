# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    ticket_ids = fields.One2many('ticket.ticket', 'sale_order_id')

    @api.multi
    def _check_existing_installation_ticket(self):
        self.ensure_one()
        for ticket in self.ticket_ids:
            if ticket.type == 'install' and ticket.active:
                if ticket.state in ['open', 'stock_given_out', 'installed', 'done']:
                    return True
        return False

    @api.multi
    def _create_installation_ticket(self):
        self.ensure_one()
        self.env['ticket.ticket'].create({
            'type': 'install',
            'name': 'Installation of %s' % self.name,
            'description': '/',
            'state': 'open',
            'partner_id': self.partner_id.id,
            'sale_order_id': self.id
        })

    @api.multi
    def wkf_approved(self):
        res = super(SaleOrder, self).wkf_approved()
        for order in self:
            existing = order._check_existing_installation_ticket()
            if not existing and order.downpayment_line_id:
                order._create_installation_ticket()
        return res

    @api.multi
    def _reserve_downpayment(self, downpayment_lines):

        res = super(SaleOrder, self)._reserve_downpayment(downpayment_lines)
        for order in self:
            existing = order._check_existing_installation_ticket()
            if order.state in ['approved', 'progress', 'done'] and not existing:
                order._create_installation_ticket()
        return res
