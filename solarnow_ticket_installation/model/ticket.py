# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
_logger = logging.getLogger(__name__)

import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from openerp import models, fields, api, _
from openerp.exceptions import Warning


class ticket_ticket(models.Model):
    _inherit = "ticket.ticket"

    partner_branch_id = fields.Many2one('res.branch', string='Branch', compute='_compute_branch_id', store=True)
    state_install = fields.Selection([
        ('open', 'Open'),
        ('stock_given_out', 'Stock Given Out'),
        ('installed', 'Installed'),
        ('done', 'Installation Verified'),
        ('client_refused', 'Client Refused'),
        ('stock_received_back', 'Stock Received Back'),
        ('cancellation_verified', 'Cancellation Verified'),
        ('cancel', 'Cancel')],
        related=False, # leave this here!
        default='open',
        string='Installation State',
        track_visibility='always'
    )
    installation_user_1_id = fields.Many2one('res.users', 'Installation officer 1')
    installation_user_2_id = fields.Many2one('res.users', 'Installation officer 2')
    dispatch_date = fields.Date('Dispatch date', compute='_compute_dispatch_date', store=True)
    sales_survey_id = fields.Many2one('ticket.survey.survey', string='Survey')
    sales_survey_result_id = fields.Many2one('ticket.survey.result')
    sales_survey_answer_ids = fields.One2many('ticket.survey.answer', 'result_id', related='sales_survey_result_id.answer_ids')
    installation_survey_id = fields.Many2one('ticket.survey.survey', string='Survey')
    installation_survey_result_id = fields.Many2one('ticket.survey.result')
    installation_survey_answer_ids = fields.One2many('ticket.survey.answer', 'result_id', related='installation_survey_result_id.answer_ids')
    credit_survey_id = fields.Many2one('ticket.survey.survey', string='Survey')
    credit_survey_result_id = fields.Many2one('ticket.survey.result')
    credit_survey_answer_ids = fields.One2many('ticket.survey.answer', 'result_id', related='credit_survey_result_id.answer_ids')
    install_other_survey_id = fields.Many2one('ticket.survey.survey', string='Survey')
    install_other_survey_result_id = fields.Many2one('ticket.survey.result')
    install_other_survey_answer_ids = fields.One2many('ticket.survey.answer', 'result_id', related='install_other_survey_result_id.answer_ids')

    @api.multi
    @api.depends('sale_order_id', 'partner_id')
    def _compute_branch_id(self):
        res = super(ticket_ticket, self)._compute_branch_id()
        for ticket in self:
            if ticket.type == 'install':
                ticket.partner_branch_id = ticket.sale_order_id.branch_id
        return res

    @api.multi
    @api.depends('sale_order_id', 'sale_order_id.picking_ids', 'sale_order_id.picking_ids.date_done')
    def _compute_dispatch_date(self):
        for ticket in self:
            pickings = filter(lambda p: p.date_done, ticket.sale_order_id.picking_ids)
            if len(pickings) > 0:
                first_picking = sorted(pickings, key=lambda p: p.date_done)[0]
                ticket.dispatch_date = first_picking.date_done

    @api.onchange('sales_survey_id')
    def _onchange_sales_survey_id(self):
        if self.sales_survey_id and self.sales_survey_result_id:
            self.sales_survey_id.update_result(self.sales_survey_result_id.id)

    @api.onchange('installation_survey_id')
    def _onchange_installation_survey_id(self):
        if self.installation_survey_id and self.installation_survey_result_id:
            self.installation_survey_id.update_result(self.installation_survey_result_id.id)

    @api.onchange('credit_survey_id')
    def _onchange_credit_survey_id(self):
        if self.credit_survey_id and self.credit_survey_result_id:
            self.credit_survey_id.update_result(self.credit_survey_result_id.id)

    @api.onchange('install_other_survey_id')
    def _onchange_install_other_survey_id(self):
        if self.install_other_survey_id and self.install_other_survey_result_id:
            self.install_other_survey_id.update_result(self.install_other_survey_result_id.id)

    @api.multi
    def installation_button_stock_given_out(self):
        self.ensure_one()
        invoice = self.sale_order_id.invoice_id
        if invoice:
            invoice.write({
                'stock_given_out_date': datetime.today().strftime(DEFAULT_SERVER_DATE_FORMAT),
            })

        self.state_install = 'stock_given_out'
        pickings = self.sale_order_id.picking_ids.sorted(key=lambda r: r.id)
        if len(pickings) == 4:
            pickings[2].force_assign()
            # the wizard is needed for generating the Date Of Transfer
            wizard = self.env['stock.transfer_details'].with_context(
                active_model='stock.picking',
                active_ids=pickings[2].ids,
                active_id=pickings[2].id
            ).sudo().create({
                'picking_id': pickings[2].id
            })
            wizard.sudo().do_detailed_transfer()

    @api.multi
    def installation_button_installed(self):
        self.ensure_one()
        if not self.installation_date:
            raise Warning('Error!', 'Installation Date is required.')
        self.write({
            'state_install': 'installed',
            'execution_date': datetime.now(),
            'execution_user_id': self._uid,
        })

    @api.multi
    def installation_button_installation_verified(self):
        self.ensure_one()
        invoice = self.sale_order_id.invoice_id
        if invoice and invoice.state not in ['open', 'paid', 'cancel']:
            invoice.write({
                'date_invoice': datetime.today().strftime(DEFAULT_SERVER_DATE_FORMAT),
            })
            invoice.signal_workflow('invoice_open')
        self.state_install = 'done'

        # TODO Translate
        if not self.sales_survey_id or not self.sales_survey_result_id.is_complete:
            raise Warning('Error!', 'Sales Carespection is required.')
        if not self.installation_survey_id or not self.installation_survey_result_id.is_complete:
            raise Warning('Error!', 'Installation Carespection is required.')
        if not self.credit_survey_id or not self.credit_survey_result_id.is_complete:
            raise Warning('Error!', 'Credit Assessment Carespection is required.')

        pickings = self.sale_order_id.picking_ids.sorted(key=lambda r: r.id)
        if len(pickings) == 4:
            pickings[3].force_assign()
            # the wizard is needed for generating the Date Of Transfer
            wizard = self.env['stock.transfer_details'].with_context(
                active_model='stock.picking',
                active_ids=pickings[3].ids,
                active_id=pickings[3].id
            ).sudo().create({
                'picking_id': pickings[3].id
            })
            wizard.sudo().do_detailed_transfer()

    @api.multi
    def installation_button_client_refused(self):
        self.ensure_one()
        self.state_install = 'client_refused'
        pickings = self.sale_order_id.picking_ids.sorted(key=lambda r: r.id)
        if len(pickings) == 4:
            return_picking_wizard = self.env['stock.return.picking'].with_context(
                active_ids=pickings[2].ids,
                active_id=pickings[2].id,
            ).sudo().create({})
            return_picking_wizard.sudo()._create_returns_ticket_installation()

    @api.multi
    def installation_button_installation_stock_received_back(self):
        self.ensure_one()
        self.state_install = 'stock_received_back'
        pickings = self.sale_order_id.picking_ids.sorted(key=lambda r: r.id)
        if len(pickings) == 5:
            pickings[4].force_assign()
            # the wizard is needed for generating the Date Of Transfer
            wizard = self.env['stock.transfer_details'].with_context(
                active_model='stock.picking',
                active_ids=pickings[4].ids,
                active_id=pickings[4].id
            ).sudo().create({
                'picking_id': pickings[4].id
            })
            wizard.sudo().do_detailed_transfer()

    @api.multi
    def installation_button_installation_cancellation_verified(self):
        self.ensure_one()
        self.state_install = 'cancellation_verified'


    @api.multi
    def button_reset(self):
        self.ensure_one()
        self.state_install = 'open'

    @api.multi
    def installation_set_cancel(self):
        for ticket in self:
            ticket.state_install = 'cancel'

    @api.model
    def create(self, vals):
        #Create survey results
        if 'type' in vals and vals['type'] == 'install':
            default_sales_survey = self.env['ticket.survey.survey'].search([('category', '=', 'sales'), ('ticket_type', '=', 'install')])
            if len(default_sales_survey) > 0:
                vals['sales_survey_id'] = default_sales_survey[0].id
                result = default_sales_survey[0].create_result()
                vals['sales_survey_result_id'] = result[0].id
            default_installation_survey = self.env['ticket.survey.survey'].search([('category', '=', 'install'), ('ticket_type', '=', 'install')])
            if len(default_installation_survey) > 0:
                vals['installation_survey_id'] = default_installation_survey[0].id
                result = default_installation_survey[0].create_result()
                vals['installation_survey_result_id'] = result[0].id
            default_credit_survey = self.env['ticket.survey.survey'].search([('category', '=', 'credit'), ('ticket_type', '=', 'install')])
            if len(default_credit_survey) > 0:
                vals['credit_survey_id'] = default_credit_survey[0].id
                result = default_credit_survey[0].create_result()
                vals['credit_survey_result_id'] = result[0].id
            default_other_survey = self.env['ticket.survey.survey'].search([('category', '=', 'other'), ('ticket_type', '=', 'install')])
            if len(default_other_survey) > 0:
                vals['install_other_survey_id'] = default_other_survey[0].id
                result = default_other_survey[0].create_result()
                vals['install_other_survey_result_id'] = result[0].id

        return super(ticket_ticket, self).create(vals)

    @api.one
    def write(self, vals):
        #Create survey results if not exists yet (migration for existing tickets)
        if self.type == 'install' and not self.sales_survey_result_id:
            default_sales_survey = self.env['ticket.survey.survey'].search([('category', '=', 'sales'), ('ticket_type', '=', 'install')])
            if len(default_sales_survey) > 0:
                vals['sales_survey_id'] = default_sales_survey[0].id
                result = default_sales_survey[0].create_result()
                vals['sales_survey_result_id'] = result[0].id
                
        if self.type == 'install' and not self.installation_survey_result_id:
            default_installation_survey = self.env['ticket.survey.survey'].search([('category', '=', 'install'), ('ticket_type', '=', 'install')])
            if len(default_installation_survey) > 0:
                vals['installation_survey_id'] = default_installation_survey[0].id
                result = default_installation_survey[0].create_result()
                vals['installation_survey_result_id'] = result[0].id
                
        if self.type == 'install' and not self.credit_survey_result_id:
            default_credit_survey = self.env['ticket.survey.survey'].search([('category', '=', 'credit'), ('ticket_type', '=', 'install')])
            if len(default_credit_survey) > 0:
                vals['credit_survey_id'] = default_credit_survey[0].id
                result = default_credit_survey[0].create_result()
                vals['credit_survey_result_id'] = result[0].id
                
        if self.type == 'install' and not self.install_other_survey_result_id:            
            default_other_survey = self.env['ticket.survey.survey'].search([('category', '=', 'other'), ('ticket_type', '=', 'install')])
            if len(default_other_survey) > 0:
                vals['install_other_survey_id'] = default_other_survey[0].id
                result = default_other_survey[0].create_result()
                vals['install_other_survey_result_id'] = result[0].id

        if self.type == 'install' and 'installation_date' in vals and vals['installation_date']:
            today = time.strftime('%Y-%m-%d')
            if vals['installation_date'] > today:
                raise Warning('Error!', 'The Installation Date cannot be in the future.')

        return super(ticket_ticket, self).write(vals)

    @api.onchange('installation_date')
    def onchange_installation_date(self):
        if self.installation_date:
            today = time.strftime('%Y-%m-%d')
            if self.installation_date > today:
                return {'warning': {
                    'title': _('Wrong Installation Date'),
                    'message': _('The Installation Date cannot be in the future.')
                    }
                }

    @api.multi
    def clean_tickets(self):
        self.ensure_one()
        tickets = self.env['ticket.ticket'].search(
            [('type', '=', 'install')]
        )
        for ticket in tickets:
            if ticket.sale_order_id and not ticket.sale_order_id.invoice_id:
                _logger.info('Removing ticket ' + ticket.sale_order_id.name)
                ticket.unlink()
