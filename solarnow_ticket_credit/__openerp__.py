{
    'name': "SolarNow Credit Ticketing",
    'summary': """SolarNow Credit Ticketing""",
    'description': """
    Solarnow additions for a credit ticketing system.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '8.0.1.0.0',
    'depends': [
        'solarnow_base',
        'solarnow_ticket',
        'solarnow_account',
        'solarnow_system',
        'sale',
        'account',
        'base'
    ],
    'data': [
        'security/ticket_security.xml',
        'security/ir.model.access.csv',
        'views/ticket_cause_credit_view.xml',
        'views/ticket_view.xml',
        'views/ticket_config_settings_view.xml',
        'views/account_invoice_view.xml',
        'menu_items.xml',
        'data/ir_cron_data.xml'
    ],
    'installable': True,
}