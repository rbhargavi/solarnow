from openerp import models, fields, api


class TicketCauseCredit(models.Model):
    _name = 'ticket.cause.credit'

    name = fields.Char(
        string='Name'
    )

    description = fields.Text(
        string='Description'
    )

    active = fields.Boolean(
        string='Active',
        default=True
    )
