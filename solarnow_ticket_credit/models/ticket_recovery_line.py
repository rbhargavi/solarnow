from openerp import fields, models, api, exceptions
import openerp.addons.decimal_precision as dp


class TicketRecoveryLine(models.Model):
    _name = 'ticket.recovery.line'

    ticket_id = fields.Many2one(
        string='Ticket',
        comodel_name='ticket.ticket'
    )

    ticket_state = fields.Selection(
        string='Ticket State',
        related='ticket_id.state_credit'
    )

    sale_order_line_id = fields.Many2one(
        string='Origin',
        comodel_name='sale.order.line'
    )

    product_id = fields.Many2one(
        string='Product',
        comodel_name='product.product'
    )

    # product_uom_qty = fields.Float(
    #     string='Quantity',
    #     digits=dp.get_precision('Product Unit of Measure'),
    #     required=True
    # )
    #
    product_uom = fields.Many2one(
        string='Unit of Measure',
        comodel_name='product.uom',
        required=True
    )

    to_recover = fields.Boolean(
        string='To Recover',
        default=True
    )

    is_recovered = fields.Boolean(
        string='Is Recovered'
    )

    recover_condition = fields.Selection(
        string='Condition',
        selection=[('bad', 'Bad'), ('good', 'Good')]
    )

    # @api.one
    # @api.constrains('to_recover')
    # def _check_to_recover(self):
    #     if self.to_recover > self.product_uom_qty:
    #         raise exceptions.ValidationError(
    #             'To Recover can not be greater than Quantity.'
    #         )
    #     elif self.to_recover < 0:
    #         raise exceptions.ValidationError(
    #             'To Recover can not be smaller than 0.'
    #         )

    # @api.one
    # @api.constrains('is_recovered')
    # def _check_is_recovered(self):
    #     if self.is_recovered > self.to_recover:
    #         raise exceptions.ValidationError(
    #             'Is Recovered can not be greater than To Recover.'
    #         )
    #     elif self.is_recovered < 0:
    #         raise exceptions.ValidationError(
    #             'Is Recovered can not be smaller than 0.'
    #         )

    @api.one
    @api.constrains('is_recovered')
    def _check_is_recovered(self):
        if self.is_recovered and not self.to_recover:
            raise exceptions.ValidationError(
                'Product "{}" should not have been recovered.'.format(
                    self.product_id.display_name
                )
            )
