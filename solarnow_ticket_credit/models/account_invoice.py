from openerp import models, fields, api


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    credit_ticket_ids = fields.One2many(
        string='Credit Tickets',
        comodel_name='ticket.ticket',
        inverse_name='invoice_id'
    )
