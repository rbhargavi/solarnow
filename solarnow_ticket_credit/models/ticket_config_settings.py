from openerp import models, fields, api


class TicketConfigSettings(models.TransientModel):
    _inherit = 'ticket.config.settings'

    # related to company_id so it is 'ready' for multi company
    credit_ticket_days_in_arrears = fields.Integer(
        string='Automatically create credit ticket if',
        related='company_id.credit_ticket_days_in_arrears'
    )
