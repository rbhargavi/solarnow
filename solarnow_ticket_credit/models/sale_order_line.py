#Deprecated
from openerp import models, fields, api


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    to_recover = fields.Float(
        string='To Recover',
        default=lambda line: line.product_uom_qty
    )

    is_recovered = fields.Float(
        string='Is Recovered'
    )

    recover_condition = fields.Selection(
        string='Condition',
        selection=[('bad', 'Bad'), ('good', 'Good')],
        default='bad'
    )
