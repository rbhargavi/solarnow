from openerp import fields, models, api, exceptions


class ResCompany(models.Model):
    _inherit = 'res.company'

    credit_ticket_days_in_arrears = fields.Integer(
        default=15,
        required=True
    )

    @api.one
    @api.constrains('credit_ticket_days_in_arrears')
    def _check_credit_ticket_days_in_arrears(self):
        if self.credit_ticket_days_in_arrears < 0:
            raise exceptions.ValidationError(
                'Days in arrears can not be smaller than 0.'
            )
