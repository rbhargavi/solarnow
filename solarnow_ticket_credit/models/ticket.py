from openerp import models, fields, api, exceptions
import openerp.addons.decimal_precision as dp
from math import ceil

time_units = [
    # 'day', 'Days'),
    # ('week', 'Week'),
    ('month', 'Month'),
    # ('year', 'Year')
]

class Ticket(models.Model):
    _inherit = 'ticket.ticket'

    state_credit = fields.Selection(
        string='Credit State',
        selection=[
            ('draft', 'New'),
            ('open', 'Open'),

            ('to_visit', 'To Visit'),
            ('visit_done', 'Visit Done'),

            ('to_reschedule', 'To Reschedule'),
            ('reschedule_done', 'Reschedule Done'),
            ('reschedule_applied', 'Reschedule Applied'),
            ('reschedule_rejected', 'Reschedule Rejected'),

            ('to_recover', 'To Recover'),
            ('recovery_defined', 'Recovery Defined'),
            ('recovery_rejected', 'Recovery Rejected'),
            ('deliver_dn', 'Deliver Demand Notice'),
            ('dn_delivered', 'Demand Notice Delivered'),
            ('client_paid', 'Client Paid'),
            ('system_recovered', 'Recovered'),
            ('recovery_acceptable', 'Recovery Acceptable'),
            ('recovery_unacceptable', 'Recovery Unacceptable'),

            ('done', 'Done'),
            ('closed', 'Closed'),
            ('canceled', 'Canceled')
        ],
        default='draft',
        track_visibility='always'
    )

    invoice_id = fields.Many2one(
        string='Invoice',
        comodel_name='account.invoice'
    )

    invoice_date = fields.Date(
        string='Invoice Date',
        related='invoice_id.date_invoice',
        help=''
    )

    system_id = fields.Many2one(
        string='System',
        comodel_name='system.system',
        compute='_compute_system_id'
    )

    # system_product_ids = fields.One2many(
    #     string='Products To Recover',
    #     comodel_name='sale.order.line',
    #     related='system_id.product_ids'
    # )

    recovery_line_ids = fields.One2many(
        string='Products To Recover',
        comodel_name='ticket.recovery.line',
        inverse_name='ticket_id'
        # compute='_compute_recovery_line_ids'
    )

    amount_open = fields.Float(
        string='Open Amount',
        digits=dp.get_precision('Account'),
        related='partner_id.payplan_summary'
    )

    amount_open_payplans = fields.Float(
        string='Open Payplans Amount',
        digits=dp.get_precision('Account'),
        help='Total amount of all open payplans.',
        related='partner_id.total_amount_running_payplans'
    )

    days_in_arrears = fields.Integer(
        string='Days In Arrears',
        related='partner_id.days_overdue'
    )

    last_payment_date = fields.Date(
        string='Date Last Payment',
        compute='_compute_last_payment_date'
    )

    cause_ids = fields.Many2many(
        string='Causes',
        comodel_name='ticket.cause.credit',
        relation='credit_ticket_cause_rel',
        column1='ticket_id',
        column2='cause_id'
    )

    credit_action = fields.Selection(
        string='Action',
        selection=[
            ('visit', 'Visit'),
            ('reschedule', 'Reschedule'),
            ('recovery', 'Recovery')
        ]
    )

    credit_visit_date = fields.Date(
        string='Visit Date'
    )

    credit_credit_survey_id = fields.Many2one(
        string='Survey',
        comodel_name='ticket.survey.survey'
    )
    credit_credit_survey_result_id = fields.Many2one(
        string='Survey Result',
        comodel_name='ticket.survey.result'
    )
    credit_credit_survey_answer_ids = fields.One2many(
        string='Survey Answers',
        comodel_name='ticket.survey.answer',
        inverse_name='result_id',
        related='credit_credit_survey_result_id.answer_ids'
    )

    credit_other_survey_id = fields.Many2one(
        string='Survey',
        comodel_name='ticket.survey.survey'
    )
    credit_other_survey_result_id = fields.Many2one(
        string='Survey Result',
        comodel_name='ticket.survey.result'
    )
    credit_other_survey_answer_ids = fields.One2many(
        string='Survey Answers',
        comodel_name='ticket.survey.answer',
        inverse_name='result_id',
        related='credit_other_survey_result_id.answer_ids'
    )

    # Payplan reschedule parameters
    reschedule_payment_term = fields.Many2one(
        string='Payment Term',
        comodel_name='account.payment.term'
    )
    reschedule_installments = fields.Integer(
        string='Number Of Installments'
    )
    reschedule_installment_interval = fields.Integer(
        string='Installment Interval'
    )
    reschedule_installment_interval_unit = fields.Selection(
        string='Installment Interval Unit',
        selection=time_units
    )
    reschedule_grace_period = fields.Integer(
        string='Grace Period'
    )
    reschedule_grace_period_unit = fields.Selection(
        string='Grace Period Unit',
        selection=time_units
    )
    origin_ticket_id = fields.Many2one(
        string='Origin Ticket',
        comodel_name='ticket.ticket'
    )
    subsequent_ticket_id = fields.Many2one(
        string='Subsequent Ticket',
        comodel_name='ticket.ticket'
    )

    @api.onchange('reschedule_payment_term')
    def _onchange_reschedule_payment_term(self):
        self.reschedule_installments = \
            self.reschedule_payment_term.installments
        self.reschedule_installment_interval = \
            self.reschedule_payment_term.installment_interval
        self.reschedule_installment_interval_unit = \
            self.reschedule_payment_term.installment_interval_unit
        self.reschedule_grace_period = \
            self.reschedule_payment_term.grace_period
        self.reschedule_grace_period_unit = \
            self.reschedule_payment_term.grace_period_unit

    @api.onchange('partner_id')
    def _onchange_partner_id_credit_ticket(self):
        self.invoice_id = False

    @api.onchange('credit_credit_survey_id')
    def _onchange_credit_credit_survey_id(self):
        if self.credit_credit_survey_id and \
                self.credit_credit_survey_result_id:
            self.credit_credit_survey_id.update_result(
                self.credit_credit_survey_result_id.id
            )

    @api.onchange('credit_other_survey_id')
    def _onchange_credit_other_survey_id(self):
        if self.credit_other_survey_id and \
                self.credit_other_survey_result_id:
            self.credit_other_survey_id.update_result(
                self.credit_other_survey_result_id.id
            )

    # Move to res.partner
    @api.depends('partner_id')
    def _compute_last_payment_date(self):
        for ticket in self:
            ticket.last_payment_date = self.env['account.move.line'].search([
                ('partner_id', '=', ticket.partner_id.id),
                ('installment_state', '=', 'paid')
            ], order='date_maturity desc', limit=1).date_maturity

    # @api.depends('invoice_id')
    # def _compute_recovery_line_ids(self):
    #     for ticket in self:
    #         ticket.recovery_line_ids.unlink()# = [(5, 0)]
    #         recovery_lines = []
    #         for sale_order_line in ticket.system_id.product_ids:
    #             recovery_lines.append((0, 0, {
    #                 'sale_order_line_id': sale_order_line.id,
    #                 'product_id': sale_order_line.product_id.id,
    #                 'product_uom_qty': sale_order_line.product_uom_qty,
    #                 'product_uom': sale_order_line.product_uom.id
    #             }))
    #         ticket.recovery_line_ids = recovery_lines

    @api.onchange('system_id')
    def _onchange_system_id(self):
        self.recovery_line_ids = [(5, 0)]
        recovery_lines = []
        for sale_order_line in self.system_id.product_ids:
            for i in range(0, int(ceil(sale_order_line.product_uom_qty))):
                recovery_lines.append((0, 0, {
                    'sale_order_line_id': sale_order_line.id,
                    'product_id': sale_order_line.product_id.id,
                    'product_uom': sale_order_line.product_uom.id
                    # 'product_uom_qty': sale_order_line.product_uom_qty,
                }))
        if recovery_lines:
            self.recovery_line_ids = recovery_lines

    # Move to account.invoice
    @api.depends('invoice_id')
    def _compute_system_id(self):
        for ticket in self:
            ticket.system_id = self.env['account.invoice.line'].search([
                ('invoice_id', '=', ticket.invoice_id.id),
                ('system_id', '!=', False)
            ], limit=1).system_id

    @api.multi
    def create_new_ticket(self):
        self.ensure_one()
        subsequent_ticket = self.create({
            'name': 'SUBSEQUENT: {}'.format(self.name),
            'type': self.type,
            'partner_id': self.partner_id.id,
            'invoice_id': self.invoice_id.id,
            'cause_ids': [(6, 0, self.cause_ids.ids)],
            'origin_ticket_id': self.id,
            'state_credit': 'draft'
        })
        self.subsequent_ticket_id = subsequent_ticket.id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'ticket.ticket',
            'name': 'Subsequent Ticket',
            'res_id': subsequent_ticket.id,
            'domain': [('id', '=', subsequent_ticket.id)],
            'view_mode': 'form',
        }

    @api.one
    def credit_open(self):
        self.state_credit = 'open'

    @api.one
    def credit_to_visit(self):
        self.credit_action = 'visit'
        self.state_credit = 'to_visit'

    @api.one
    def credit_visit_done(self):
        self.state_credit = 'visit_done'

    @api.one
    def credit_to_reschedule(self):
        self.credit_action = 'reschedule'
        self.state_credit = 'to_reschedule'

    @api.one
    def credit_reschedule_done(self):
        self.state_credit = 'reschedule_done'

    @api.one
    def credit_reschedule_applied(self):
        reschedule_wizard = self.env['wizard.invoice.reschedule'].create({
            'invoice': self.invoice_id.id,
            'payment_term': self.reschedule_payment_term.id,
            'installments': self.reschedule_installments,
            'installment_interval': self.reschedule_installment_interval,
            'installment_interval_unit':
                self.reschedule_installment_interval_unit,
            'grace_period': self.reschedule_grace_period,
            'grace_period_unit': self.reschedule_grace_period_unit
        })
        reschedule_wizard.do_reschedule()
        self.state_credit = 'reschedule_applied'

    @api.one
    def credit_reschedule_rejected(self):
        self.state_credit = 'reschedule_rejected'

    @api.one
    def credit_reschedule_sent(self):
        self.state_credit = 'done'

    @api.one
    def credit_to_recover(self):
        self.credit_action = 'recovery'
        self.state_credit = 'to_recover'

    @api.one
    def credit_recovery_defined(self):
        self.state_credit = 'recovery_defined'

    @api.one
    def credit_recovery_rejected(self):
        self.state_credit = 'recovery_rejected'

    @api.one
    def credit_deliver_dn(self):
        self.state_credit = 'deliver_dn'

    @api.one
    def credit_dn_delivered(self):
        self.state_credit = 'dn_delivered'

    @api.one
    def credit_client_paid(self):
        self.state_credit = 'client_paid'

    @api.one
    def credit_client_did_not_pay(self):
        self.state_credit = 'dn_delivered'

    @api.one
    def credit_system_recovered(self):
        self.state_credit = 'system_recovered'

    @api.one
    def credit_client_paid_now(self):
        """
        A client can pay anytime through the credit tickets life.
        This method will put state_credit to 'done' and sends a message in the
        comment section 'Client Paid'. The credit ticket going to 'done' state
        doensn't always mean they paid; this is why this method exists.
        """
        self.state_credit = 'done'
        self.message_post(
            body='Client Paid'
        )

    @api.one
    def _validate_recover_condition(self):
        for line in self.recovery_line_ids:
            if line.is_recovered and not line.recover_condition:
                raise exceptions.ValidationError(
                    'Please specify the Condition of all recovered products.'
                )

    @api.one
    def credit_recovery_acceptable(self):
        self._validate_recover_condition()
        self.state_credit = 'recovery_acceptable'

    @api.one
    def credit_recovery_unacceptable(self):
        self._validate_recover_condition()
        self.state_credit = 'recovery_unacceptable'

    @api.one
    def credit_write_off_done(self):
        self.state_credit = 'done'

    @api.one
    def credit_done(self):
        self.credit_credit_survey_id.update_result(
            self.credit_credit_survey_result_id.id
        )
        self.credit_other_survey_id.update_result(
            self.credit_other_survey_result_id.id
        )
        self.state_credit = 'done'

    @api.one
    def credit_close(self):
        if not self.credit_credit_survey_id or not \
                self.credit_credit_survey_result_id.is_complete:
            raise exceptions.Warning('Error!', 'Carespection is required.')
        self.state_credit = 'closed'

    @api.one
    def credit_canceled(self):
        self.state_credit = 'canceled'

    @api.one
    def credit_reset(self):
        self.state_credit = 'open'

    @api.model
    def create(self, vals):
        if 'type' in vals and vals['type'] == 'credit':
            default_credit_survey = self.env['ticket.survey.survey'].search([
                ('category', '=', 'credit'),
                ('ticket_type', '=', 'credit')
            ], limit=1)
            if default_credit_survey:
                vals['credit_credit_survey_id'] = default_credit_survey.id
                result = default_credit_survey.create_result()
                vals['credit_credit_survey_result_id'] = result[0].id

            default_other_survey = self.env['ticket.survey.survey'].search([
                ('category', '=', 'other'),
                ('ticket_type', '=', 'credit')
            ], limit=1)
            if default_other_survey:
                vals['credit_other_survey_id'] = default_other_survey.id
                result = default_other_survey.create_result()
                vals['credit_other_survey_result_id'] = result[0].id
        return super(Ticket, self).create(vals)

    @api.model
    def _cron_create_credit_tickets(self):
        # Get companies
        companies = self.env['res.company'].search([])
        for company in companies:
            # Check whether it's enabled for the company
            if not company.credit_ticket_days_in_arrears:
                continue
            # Get overdue / in arrears partners of companies
            partners = self.env['res.partner'].search([
                ('company_id', '=', company.id),
                ('days_overdue', '>=', company.credit_ticket_days_in_arrears)
            ])
            for partner in partners:
                # Get last invoice
                last_open_invoice = self.env['account.invoice'].search([
                    ('state', 'not in', ['paid', 'cancel']),
                    ('partner_id', '=', partner.id),
                    ('is_credit', '=', True),
                    ('type', '=', 'out_invoice')
                ], order='date_invoice desc', limit=1)

                # Has this partner an open credit ticket?
                open_tickets = self.env['ticket.ticket'].search([
                    ('type', '=', 'credit'),
                    ('state_credit', 'not in', ['done', 'closed', 'canceled']),
                    ('partner_id', '=', partner.id)
                ])
                if open_tickets:
                    continue
                # Do a closed recovery credit ticket exists for this
                # last_open_invoice
                closed_tickets = self.env['ticket.ticket'].search([
                    ('type', '=', 'credit'),
                    ('partner_id', '=', partner.id),
                    ('invoice_id', '=', last_open_invoice.id),
                    ('state_credit', 'in', ['done', 'closed', 'canceled']),
                    ('credit_action', '=', 'recovery')
                ])
                if closed_tickets:
                    continue
                # Create ticket
                ticket = self.env['ticket.ticket'].create({
                    'name': partner.name,
                    'partner_id': partner.id,
                    'invoice_id': last_open_invoice.id,
                    'type': 'credit'
                })
                ticket._onchange_system_id()
                ticket.credit_open()
