"use strict";
openerp.web_group_expand = function(openerp) {
    var QWeb = openerp.web.qweb;

    openerp.web.ListView.include({
        reload_content: function() {
            var self = this;
            var res = this._super.apply(this, arguments);
            res.done(function() {
                var expand_on_reload = self.get('expand_on_reload');
                expand_on_reload = expand_on_reload &&
                    (expand_on_reload == '1' || expand_on_reload.toLowerCase() == 'true')
                if(expand_on_reload)
                    self.ViewManager.expand_groups();
            });
            return res;
        },
    });

    //TODO: Move methods (collapse_groups and expand_groups) are not methods that belong to ViewManager
    openerp.web.ViewManager.include({
        switch_mode: function(view_type, no_store, view_options) {
            if (view_type != 'list' && view_type != 'tree') {
                this.$el.find("ul#oe_group_by").remove();
            }
            if(view_type == 'tree'){
                this.load_expand_buttons();
                this.$ExpandButtons.find("a#oe_group_by_reset").click(function(){
                    $('.oe_open .treeview-tr.oe-treeview-first').filter(function(){return ($(this).parents('tr').attr('data-level') == 1)}).click()
                });
                this.$ExpandButtons.find("a#oe_group_by_expand").click(function(){
                    $('.treeview-tr.oe-treeview-first').filter(function(){return (!$(this).parents().is('.oe_open')) & ($(this).parents().css( "display" ) != 'none')}).click();
                });
            }
            return this._super.apply(this, arguments);
        },
        setup_expand: function(domains, contexts, groupbys) {
            var self = this;
            this.$el.find("ul#oe_group_by").remove();
            if(groupbys.length && this.active_view == 'list') {
                this.load_expand_buttons();
                this.$el.find("a#oe_group_by_reset").click(function() {
                    self.collapse_groups();
                });
                this.$el.find("a#oe_group_by_expand").click(function() {
                    self.expand_groups();
                });
            }
        },
        collapse_groups: function() {
            $('span.ui-icon-triangle-1-s').click();
        },
        expand_groups: function() {
            $('span.ui-icon-triangle-1-e').click();
        },
        load_expand_buttons:function() {
            var self = this;
            this.$el.find("ul#oe_group_by").remove();
            this.$ExpandButtons = $(QWeb.render("GroupExpand.Buttons", {'widget':self}));
            this.$el.find("ul.oe_view_manager_switch.oe_button_group.oe_right").before(this.$ExpandButtons);
        },
        setup_search_view: function(view_id, search_defaults) {
            var self = this;
            var res = this._super.apply(this, arguments);
            this.searchview.on('search_data', self, this.setup_expand);
            return res;
        },
    });

    openerp.web.search.GroupbyGroup.include({
        search_change: function() {
            var self = this;
            var facet = this.view.query.find(_.bind(this.match_facet, this));
            if(facet) {
                facet.values.each(function (v) {
                    var vm = self.view.view_manager;
                    var controller = vm.views[vm.active_view].controller;
                    controller.set('expand_on_reload', v.attributes.value.attrs.expand)
                });
            }
            return this._super.apply(this, arguments);
        }
    })
}
