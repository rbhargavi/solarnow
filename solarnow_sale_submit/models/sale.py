# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api


class sale_order(models.Model):
    _inherit = "sale.order"

    submitted_customer_name = fields.Char('Customer name')
    submitted_customer_ref = fields.Char('Customer ref')
    submitted_customer_phone = fields.Char('Customer phone')
    submitted_customer_phone2 = fields.Char('Customer phone2')

    submitted_birthplace = fields.Char('Place of birth')
    submitted_birthdate2 = fields.Char('Date of birth')
    submitted_gender = fields.Char('Gender')
    submitted_marital_status = fields.Char('Marital Status')
    submitted_customer_relative = fields.Char('Spouse')
    submitted_customer_relative_phone = fields.Char('Spouse contact')
    submitted_house = fields.Char('House')
    submitted_number_rooms = fields.Char('Number of rooms')
    submitted_live_in_house = fields.Char('Year living in the house')
    submitted_branch_distance = fields.Char('Distance to the branch')

    submitted_country = fields.Char('Country')
    submitted_landmark = fields.Char('Landmark')
    submitted_village = fields.Char('Village')
    submitted_district = fields.Char('District')
    submitted_county = fields.Char('County')
    submitted_subcounty = fields.Char('Parish')

    submitted_saldo = fields.Float('Balance')
    submitted_ratio_factor = fields.Float('Ratio factor')
    submitted_saldo_ratio = fields.Float('Balance ratio')
    submitted_other_loans = fields.Text('Other loans')
    submitted_investment_registration = fields.Text('Investment registration')

    submitted_installments = fields.Integer('Number of installments')
    submitted_deposit = fields.Integer('Deposit percentage')
    submitted_interest_rate = fields.Float("Interest", digits=(1,8))
    submitted_installment_interval = fields.Integer('Installment interval')
    submitted_grace_period = fields.Integer('Grace period')

    @api.multi
    def wkf_assessment(self):
        res = super(sale_order, self).wkf_assessment()
        for order in self:
            partner = order.partner_id
            partner_dict = {
                'submitted_customer_name': partner.name,
                'submitted_customer_ref': partner.ref,
                'submitted_customer_phone': partner.phone,
                'submitted_customer_phone2': partner.phone2,

                'submitted_birthplace': partner.birthplace,
                'submitted_birthdate2': partner.birthdate2,
                'submitted_gender': partner.gender,
                'submitted_marital_status': partner.marital_status,
                'submitted_customer_relative': partner.customer_relative,
                'submitted_customer_relative_phone': partner.customer_relative_phone,
                'submitted_house': partner.house,
                'submitted_number_rooms': partner.number_rooms,
                'submitted_live_in_house': partner.live_in_house,
                'submitted_branch_distance': partner.branch_distance,

                'submitted_country': partner.country_id and partner.country_id.name,
                'submitted_landmark': partner.landmark,
                'submitted_village': partner.village,
                'submitted_district': partner.district_id and partner.district_id.name,
                'submitted_county': partner.county_id and partner.county_id.name,
                'submitted_subcounty': partner.subcounty_id and partner.subcounty_id.name,

                'submitted_saldo': partner.saldo,
                'submitted_ratio_factor': partner.ratio_factor,
                'submitted_saldo_ratio': partner.saldo_ratio,
                'submitted_other_loans': partner.other_loans,
                'submitted_investment_registration': partner.investment_registration,

                'submitted_installments': order.installments,
                'submitted_deposit': order.deposit,
                'submitted_interest_rate': order.interest_rate,
                'submitted_installment_interval': order.installment_interval,
                'submitted_grace_period': order.grace_period,
            }
            order.write(partner_dict)
        return res
