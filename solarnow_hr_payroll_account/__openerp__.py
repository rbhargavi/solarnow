# -*- coding: utf-8 -*-
# © 2016 ONESTEiN BV (<http://www.onestein.eu>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Solarnow Payroll Accounting',
    'version': '1.0',
    'category': 'Human Resources',
    'description': """
Solarnow Payroll Accounting.

    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'depends': [
        'solarnow_base',
        'hr_payroll_account'
    ],
    'data': [
    ],
    'installable': True,
    'auto_install': False,
}
