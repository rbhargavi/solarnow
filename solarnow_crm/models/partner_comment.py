# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields


class partner_comment(models.Model):
    _name = 'partner.comment'
    _description = 'Partner comment'

    x_create_date = fields.Datetime('Migrated Create Date')
    x_create_uid = fields.Many2one('res.users', 'Migrated Create User')
    x_create_uid_name = fields.Char('Migrated Create User name')
    x_payplan_contract_number = fields.Char('Migrated Payplan Contract Number')

    name = fields.Text('Comment')
    comment_type = fields.Many2one('comment.type',
        string = "Comment type")
    partner_id = fields.Many2one('res.partner',
        string="Customer")
