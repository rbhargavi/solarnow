# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp import models, fields, api, _

_logger = logging.getLogger(__name__)


class crm_lead_ref(models.Model):
    _name = 'crm.lead.ref'
    _description = 'Customer References'

    customer_lead_name = fields.Char('Name')
    customer_lead_phone = fields.Char('Phone')
    customer_lead_reason = fields.Char('Relationship')
    customer_lead_ids = fields.One2many('crm.lead', 'crm_lead_ref_id',
        string='Generated lead',
        help="Lead through this customer.")
    partner_id = fields.Many2one('res.partner', string='Referred by')

    lead = fields.Boolean('Lead', default=True)

    @api.multi
    def write(self, vals):
        # create a lead if bool(lead) is checked)
        if not self.customer_lead_ids and vals.get('lead', False):
            self.create_customer_lead(vals, self)
        return super(crm_lead_ref, self).write(vals)

    @api.model
    def create(self, vals):
        # create a lead if bool(lead) is checked)
        lead_ref = super(crm_lead_ref, self).create(vals)

        if not self.customer_lead_ids and vals.get('lead', False):
            self.create_customer_lead(vals, lead_ref)

        return lead_ref

    @api.multi
    def open_customer_lead(self):
        _logger.debug(20 * u"Z", self._context.get('customer_lead_ids'))
        _logger.debug(20 * 'z')
        _logger.debug(self._context.get('customer_lead_ids'))
        if self.customer_lead_ids:
            return {
                'Name': 'Leads',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'crm.lead',
                'res_id': self.customer_lead_ids[0].id,
                'type': 'ir.actions.act_window',
                'context': {
                },
            }

    @api.multi
    def create_customer_lead(self, vals, lead_ref):
        if not self.customer_lead_name and not vals.get('customer_lead_name', False):
            vals.update({'customer_lead_name': 'No name yet'})
            lead_ref.customer_lead_name = 'No name yet'
#        default_tags = [case.id for case in self.env['crm.case.categ'].search([('default_refer_customer','=',True)])]

        vals = {'name': lead_ref.customer_lead_name or vals.get('customer_lead_name', False),
                'phone': lead_ref.customer_lead_phone,
                'description': lead_ref.customer_lead_reason,
                # 'categ_ids': default_tags, # TODO restore - rewrite to o2m
                'crm_lead_ref_id': lead_ref.id,
                'referred_by_id': lead_ref.partner_id.id,
                }

        self.env['crm.lead'].create(vals)
