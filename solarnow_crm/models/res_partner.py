# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp import models, fields, api, _
from openerp.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class District(models.Model):
    _name = 'res.district'
    _description = 'Uganda district'

    name = fields.Char('District')


class County(models.Model):
    _name = 'res.county'
    _description = 'Uganda county'

    name = fields.Char('County')
    district = fields.Many2one('res.district', string='District')


class Subcounty(models.Model):
    _name = 'res.subcounty'
    _description = 'Uganda Parish'

    name = fields.Char('Parish')
    county = fields.Many2one('res.county', string='County')


class res_partner_income(models.Model):
    _name = 'res.partner.income'
    _description = 'Customer income'

    partner_id = fields.Many2one('res.partner')
    # currency_id = fields.Many2one('res.currency', 'Secondary Currency',
    #     help="Forces all moves for this account to have this secondary currency.")
    name = fields.Char('Income')         # TODO No longer used
    amount = fields.Float('Low Income Month')
    amount_high = fields.Float('High Income Month')
    income_type_id = fields.Many2one('res.partner.income.type', string="Income type")


class res_partner_income_type(models.Model):
    _name = 'res.partner.income.type'
    _description = 'Customer income type'

    name = fields.Char('Income')
    default_type = fields.Boolean('Default income type')


class res_partner_expense_type(models.Model):
    _name = 'res.partner.expense.type'
    _description = 'Customer expense type'

    name = fields.Char('Expense')
    default_type = fields.Boolean('Default expense type')
    

class res_partner_expense(models.Model):
    _name = 'res.partner.expense'
    _description = 'Customer expense'

    partner_id = fields.Many2one('res.partner')
    expense_type_id = fields.Many2one('res.partner.expense.type', string="Expense type")
    amount = fields.Float('Monthly Amount')


class res_partner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def _default_expense_types(self):
        ids = [(0,0,{'expense_type_id':t.id,'name':t.name}) 
            for t in self.env['res.partner.expense.type'].search([
                ('default_type','=',True)])]
        return ids

    @api.multi
    def _get_partner_comments_count(self):
        for partner in self:
            partner.partner_comments_count = len(partner.comment_ids)

    @api.multi
    @api.onchange('branch_id')
    def _default_credit_responsible(self):
        for partner in self:
            partner.credit_responsible = None
            if partner.branch_id:
                if partner.branch_id.credit_responsible:
                    resp_id = partner.branch_id.credit_responsible.id
                    partner.credit_responsible = resp_id

    credit_responsible = fields.Many2one('hr.employee', string='Credit responsible', domain=[('employee', '=', 'True')])

    campaign = fields.Many2one('crm.marketing.campaign', string='Marketing Campaign')
    customer_reffered = fields.Many2one('res.partner',
        string='Referred from',
        help="Customer who referred this customer to us.")
    customer_lead_name = fields.Char('Name')
    customer_lead_phone = fields.Char('Phone')
    customer_lead_reason = fields.Char('Relationship')
    customer_lead_ids = fields.One2many('crm.lead', 'partner_id',
        string='Generated lead',
        help="Lead through this customer.")
    customer_lead_ref_ids = fields.One2many('crm.lead.ref',
                                            'partner_id',
                                            string="Customer Reference")

    landmark = fields.Char('Landmark')
    village = fields.Char('Village')
    district_id = fields.Many2one('res.district', string='District')
    county_id = fields.Many2one('res.county', string='County')
    subcounty_id = fields.Many2one('res.subcounty', string='Parish')

    # When a customer is being created these expense ids must be filled automatically.
    expense_ids = fields.One2many('res.partner.expense', 'partner_id',
        default=_default_expense_types)
    #income_set_id = fields.Many2one('res.partner.income.set')
    income_ids = fields.One2many('res.partner.income', 'partner_id')

    # Optional : Add mean over the last 3 month
    saldo = fields.Float(compute='_compute_saldo',
        string="Balance", store=True)
    saldo_ratio = fields.Float(compute='_compute_saldo_ratio',
        string="Balance ratio", store=True)
    ratio_factor = fields.Float('Ratio factor',
        related='company_id.default_ratio_factor', readonly=True)

    investment_registration = fields.Text("Investment registration")
    other_loans = fields.Text('Other loans')

    lead_id = fields.Many2one('crm.lead', string="Lead", help="Lead from which this customer was created.")

    ref = fields.Char('Reference', index=1, readonly=False)
    comment_ids = fields.One2many('partner.comment', 'partner_id',
        string="Comments")
    user_id = fields.Many2one('res.users', 'Salesperson', help='The internal user that is in charge of communicating with this contact if any.', default=lambda self: self.env.user)
    partner_comments_count = fields.Integer(compute='_get_partner_comments_count', string='Partner Comments Count')

    @api.model
    def create(self, vals):
        res = super(res_partner, self).create(vals)
        #res.ref = vals.get('ref', False) or res.ref # Workaround to keep the filled value
        if res.customer:
            if res.branch_id and res.branch_id.customer_seq_id:
                res.ref = "{}{}".format(res.branch_id.code, res.env['ir.sequence'].next_by_id(res.branch_id.customer_seq_id.id))

        if self.supplier:
            pass

        #res.ref = "{}{}".format(res.branch_id.code, res.env['ir.sequence'].next_by_code('res.partner.ref'))

        if self._context.get('default_lead_id', False):
            vals.update({'lead_id': self._context['default_lead_id']})
            lead = self.env['crm.lead'].browse(self._context['default_lead_id'])
            lead.write({'generated_partner_id': res.id, 'partner_id': res.id})
            lead._upgrade_stage()

        return res

    @api.one
    @api.constrains('phone')
    def _phone_unique(self):
        if not self.phone:
            return 
        other_phone = self.env['res.partner'].search([('phone','=',self.phone), ('id','!=',self.id)])
        for phone in other_phone:
            raise ValidationError('Partner %s has already phone number %s'%
                (phone.name,phone.phone))

    @api.constrains('ratio_factor')
    def _check_ratio_factor(self):
        for record in self:
            if record.ratio_factor > 100 or record.ratio_factor < 0:
                raise ValidationError("Please enter a factor between 0 and 100.")


    @api.multi
    def create_customer_lead(self):
        if not self.customer_lead_name:
            self.customer_lead_name = 'No name yet'

        default_tags = [case.id for case in self.env['crm.case.categ'].search([('default_refer_customer','=',True)])]

        return {
            'Name': 'Leads',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.lead',
            'type': 'ir.actions.act_window',
            'context': {
                'default_name': self.customer_lead_name,
                'default_phone': self.customer_lead_phone,
                'default_description': self.customer_lead_reason,
                'default_categ_ids': default_tags,
                'default_referred_by_id': self.id,
            },
        }

    # @api.model
    # def create(self, vals):
    #     partner = super(res_partner, self).create(vals)
    #     # Write created customer back to lead if this exists
    #     # Not implemented because this will create rubbish
    #     import pdb;pdb.set_trace()

    #     return partner

    @api.one
    @api.depends('expense_ids', 'income_ids')
    def _compute_saldo(self):
        total_income = 0
        for income in self.income_ids:
            total_income += income.amount
        total_expense = 0
        for expense in self.expense_ids:
            total_expense += expense.amount
        self.saldo = total_income - total_expense

    @api.one
    @api.depends('expense_ids', 'income_ids','ratio_factor')
    def _compute_saldo_ratio(self):       
        self.saldo_ratio = self.saldo * (self.ratio_factor / 100.0)

    @api.model
    def _address_fields(self):
        fields = super(res_partner, self)._address_fields()
        fields.append('landmark')
        fields.append('village')
        return fields

    @api.model
    def _display_address(self, address, without_company=False):

        '''
        The purpose of this function is to build and return an address formatted accordingly to the
        standards of the country where it belongs.

        Overwritten for Solarnow_crm.

        :param address: browse record of the res.partner to format
        :returns: the address formatted in a display that fit its country habits (or the default ones
            if not country is specified)
        :rtype: string
        '''

        # get the information that will be injected into the display format
        # get the address format

        address_format = address.country_id.address_format or \
              "%(street)s\n%(street2)s\n%(city)s %(state_code)s %(zip)s\n%(country_name)s"

        args = {
            'state_code': address.state_id.code or '',
            'state_name': address.state_id.name or '',
            'country_code': address.country_id.code or '',
            'country_name': address.country_id.name or '',
            'company_name': address.parent_name or '',
            'district_name': address.district_id.name  or '',
            'county_name': address.county_id.name or '',
            'parish_name': address.subcounty_id.name or ''
        }

        for field in self._address_fields():
            args[field] = getattr(address, field) or ''
        if without_company:
            args['company_name'] = ''
        elif address.parent_id:
            address_format = '%(company_name)s\n' + address_format

        return address_format % args
