# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp import models, fields, api, _
from openerp.exceptions import Warning, ValidationError


_logger = logging.getLogger(__name__)


class crm_lead(models.Model):
    _inherit = "crm.lead"

    def _default_country(self):
        assert isinstance(self.env['ir.model.data'].get_object('base', 'ug').id, object)
        return self.env['ir.model.data'].get_object('base', 'ug').id

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('crm.lead')
    )
    branch_ref_id = fields.Many2one(
        comodel_name="res.branch",
        string="Destination Branch",
        default=lambda self: self.env.user.employee_ids and self.env.user.employee_ids[0] and self.env.user.employee_ids[0].branch_id
    )
    
    
    @api.multi
    @api.depends('branch_ref_id')
    def _compute_branch_assigned(self):
        for lead in self:
            lead.branch_assigned = lead.branch_id.id == lead.branch_ref_id.id
    
    #TODO: Fix me there was a problem with the filter assigned_leads this is a workaround
    branch_assigned = fields.Boolean(compute='_compute_branch_assigned', store=True)

    crm_lead_ref_id = fields.Many2one('crm.lead.ref', string="Customer Reference")
    origin_id = fields.Many2one('crm.marketing.campaign',
                                string="Origin")

    employee_id = fields.Many2one('hr.employee', string='Sales person', default=lambda self: self.env.user.employee_ids and self.env.user.employee_ids[0])

    landmark = fields.Char('Landmark')
    village = fields.Char('Village')
    district_id = fields.Many2one('res.district', string='District')
    county_id = fields.Many2one('res.county', string='County')
    subcounty_id = fields.Many2one('res.subcounty', string='Parish')

    category_id = fields.Many2one('crm.lead.category',
                                  string="Category")
    country_id = fields.Many2one('res.country', string='Country', default=_default_country)
    generated_partner_id = fields.Many2one('res.partner', string='Created Customer', help='The customer that was created using this lead.')
    referred_by_id = fields.Many2one('res.partner', string='Referred by')
    not_snsys = fields.Boolean(string='Has non-SolarNow system')
    name = fields.Char('Name', required=True, index=1)
    phone2 = fields.Char(string="Phone 2")
    categ_ids = fields.Many2many('crm.case.categ', 'crm_lead_category_rel', 'lead_id', 'category_id', string='Campaigns', \
        domain="['|', ('section_id', '=', section_id), ('section_id', '=', False), ('object_id.model', '=', 'crm.lead')]", help="Classify and analyze your lead/opportunity categories like: Training, Service")

    source2_id = fields.Many2one('crm.tracking.source', string="Source")

    user_id = fields.Many2one('res.users', '-----')  # hidden

    @api.multi
    def action_view_customer(self):
        '''
        This function returns an action that displays the related partner.
        '''
        if self.generated_partner_id:
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'res.partner',
                'res_id': self.generated_partner_id.id,
                'view_mode': 'form',
            }

    def _convert_opportunity_data(self, cr, uid, lead, customer, section_id=False, context=None):

        if not section_id:
            section_id = lead.section_id and lead.section_id.id or False
        val = {
            'planned_revenue': lead.planned_revenue,
            'probability': lead.probability,
            'name': lead.name,
            'partner_id': customer and customer.id or False,
            'type': 'opportunity',
            'date_action': fields.datetime.now(),
            'date_open': fields.datetime.now(),
            'email_from': customer and customer.email or lead.email_from,
            'phone': customer and customer.phone or lead.phone,

            'landmark': lead.landmark,
            'village': lead.village,
            'district_id': lead.district_id,
            'county_id': lead.county_id,
            'subcounty_id': lead.subcounty_id,
        }
        if not lead.stage_id or lead.stage_id.type == 'lead':
            val['stage_id'] = self.stage_find(cr, uid, [lead], section_id, [('type', 'in', ('opportunity', 'both'))],
                                              context=context)
        return val

    @api.multi
    def _upgrade_stage(self):
        """Upgrade to the next stage by sequence."""
        stage_obj = self.env['crm.case.stage']
        next_stage = stage_obj.search([('sequence', '>', self.stage_id.sequence), ('type', '!=', 'opportunity')], order='sequence', limit=1)[0]
        self.stage_id = next_stage

    @api.multi
    def create_customer_from_lead(self):
        # default_tags = [tag.id for tag in self.env['crm.case.categ'].search([('crm.case.categ','=',True)])]
        if not self.generated_partner_id:
            return {
                'Name': 'Customer',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'res.partner',
                'type': 'ir.actions.act_window',
                'context': {
                    'default_name': self.name,
                    'default_email': self.email_from,
                    'default_phone': self.phone,
                    'default_phone2': self.phone2,
                    'default_comment': self.description,
                    'default_street': self.street,
                    'default_city': self.city,
                    'default_country_id': self.country_id.id,
                    'default_district_id': self.district_id.id,
                    'default_county_id': self.county_id.id,
                    'default_subcounty_id': self.subcounty_id.id,
                    'default_village': self.village,
                    'default_landmark': self.landmark,
                    'default_user_id': self.env.user.id,
                    'default_section_id': self.section_id.id,
                    'default_lead_id': self.id,
                    'default_branch_id': self.branch_ref_id.id,
                    # 'default_categ_ids': default_tags,
                },
            }
        raise Warning(_('Customer already created'), _("There already is a customer created through this lead: %s (id: %s)" % (self.generated_partner_id.name, self.generated_partner_id.id)))
