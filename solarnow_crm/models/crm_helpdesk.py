# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _


class crm_helpdesk(models.Model):
    _inherit = 'crm.helpdesk'

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('crm.helpdesk')
    )

    state = fields.Selection(
        [('draft', 'New'),
         ('assigned', 'In Progress'),
         ('done', 'Closed')], 'Status', readonly=True, track_visibility='onchange',
                          help='The status is set to \'Draft\', when a ticket is created.\
                          \nIf the case is in progress the status is set to \'Assign\'.\
                          \nWhen the ticket is executed, the status is set to \'Done\'.'
                          )
    approved_by = fields.Many2one('res.users',
        string="Approved by",
        track_visibility='onchange')

    # These option should be represented in the corrosponding menu items.
    ticket_category = fields.Selection([
        ('installation', 'Installation'),
        ('service', 'Service'),
        ('credit', 'Credit'),
        ('engineering', 'Engineering'),
        ('followup', 'Follow up')
        ])

    followup_cat = fields.Selection([
        ('call', 'Call'),
        ('vistit', 'Visit'),
        ('sms', 'SMS'),
        ],
        string="Follow-up cat")

    @api.multi
    def approve_installation(self):
        self.approved_by = self._uid

    @api.multi
    def reschedule_payplan(self):
        pass
