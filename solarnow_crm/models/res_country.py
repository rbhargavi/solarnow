# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields


class ResCountry(models.Model):
    _inherit = 'res.country'

    address_format = fields.Text('Address Format',
        help="""You can state here the usual format to use for the \
        addresses belonging to this country.\n\nYou can use the python-style string patern with \
        all the field of the address (for example, use '%(street)s' to display the field 'street')\
        plus:
            %(state_name)s: the name of the state
            %(state_code)s: the code of the state
            %(country_name)s: the name of the country
            %(country_code)s: the code of the country
            %(village)s: the name of the village
            %(landmark)s: the name of the lanmark
            %(district_name)s: the district name
            %(county_name)s: the county name
            %(parish_name)s: the Parish name""")
