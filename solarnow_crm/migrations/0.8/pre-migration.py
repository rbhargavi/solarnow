# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2016 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


def migrate(cr, version):
    if not version:
        return

    cr.execute(
        'ALTER TABLE crm_lead '
        'DROP COLUMN IF EXISTS source2_id')

    cr.execute(
        'ALTER TABLE crm_lead '
        'RENAME COLUMN medium_id TO source2_id')

    cr.execute(
        'ALTER TABLE crm_lead '
        'ADD CONSTRAINT crm_lead_source2_id_fkey1 FOREIGN KEY (source2_id) '
        'REFERENCES crm_tracking_source (id) MATCH SIMPLE '
        'ON UPDATE NO ACTION ON DELETE SET NULL')


    cr.execute(
        'ALTER TABLE crm_lead '
        'DROP COLUMN IF EXISTS employee_id')

    cr.execute(
        'ALTER TABLE crm_lead '
        'RENAME COLUMN user_id TO employee_id')

    cr.execute(
        'ALTER TABLE crm_lead '
        'ADD CONSTRAINT crm_lead_employee_id_fkey1 FOREIGN KEY (employee_id) '
        'REFERENCES hr_employee (id) MATCH SIMPLE '
        'ON UPDATE NO ACTION ON DELETE SET NULL')

    cr.execute(
        'DROP INDEX IF EXISTS crm_lead_user_id_index')
