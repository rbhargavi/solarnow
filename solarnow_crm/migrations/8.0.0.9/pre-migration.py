# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.


def migrate(cr, version):
    query = """
        UPDATE ir_attachment ia
        SET res_id = cfiar.credit_form_id, res_model = 'res.partner'
        FROM credit_form_ir_attachments_rel cfiar
        WHERE ia.id = cfiar.attachment_id;
    """
    cr.execute(query)
