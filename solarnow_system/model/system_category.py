# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _


class system_category(models.Model):
    _name = "system.category"
    _description = "System Category"

    _constraints = [
        (models.Model._check_recursion, 'Error ! You cannot create recursive categories.', ['parent_id'])
    ]

    @api.multi
    def name_get(self):
        res = []
        for cat in self:
            names = [cat.name]
            pcat = cat.parent_id
            while pcat:
                names.append(pcat.name)
                pcat = pcat.parent_id
            res.append((cat.id, ' / '.join(reversed(names))))
        return res

    @api.multi
    def _name_get_fnc(self):
        res = self.name_get()
        return dict(res)

    name = fields.Char("System Tag", required=True)
    complete_name = fields.Char(compute='_name_get_fnc', string='Name')
    parent_id = fields.Many2one('system.category', 'Parent System Tag', index=True)
    child_ids = fields.One2many('system.category', 'parent_id', 'Child Categories')
    system_ids = fields.Many2many('system.system', 'system_category_rel', 'category_id', 'system_id', 'Systems')
