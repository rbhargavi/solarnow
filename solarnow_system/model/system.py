# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from openerp import models, fields, api, _

_logger = logging.getLogger(__name__)


class system_system(models.Model):
    _name = "system.system"
    _inherit = 'mail.thread'
    _description = "System sold to the customer"

    company_id = fields.Many2one('res.company', string='Company',
        default=lambda self: self.env['res.company']._company_default_get('system.system'))

    name = fields.Char("Description")
    system_type = fields.Char("System type")
    owner_id = fields.Many2one("res.partner",
        string="System Owner")
    address_id = fields.Many2one("res.partner",
        string="System Address")

    product_ids = fields.One2many("sale.order.line", 'system_id')
    category_id = fields.Many2many('system.category', 'system_category_rel', 'system_id', 'category_id', string="Tags")
    powergrid_available = fields.Boolean('Is power grid available?')
    system_latitude = fields.Float('Geo Latitude', digits=(16, 5))
    system_longitude = fields.Float('Geo longitude', digits=(16, 5))
