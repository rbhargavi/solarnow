# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "Solarnow",
    'summary': """Solarnow module""",
    'description': """
    Solarnow Module.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.2',
    'depends': [
        'solarnow_account',
        'solarnow_base',
        'solarnow_crm',
        'solarnow_discount',
        'solarnow_hr',
        'solarnow_hr_payroll',
        'solarnow_hr_employee_benefit',
        'solarnow_hr_payroll_account',
        'solarnow_sale',
        'solarnow_stock',
        'solarnow_system',
        'solarnow_purchase',
        'solarnow_ticket',
        'solarnow_sale_access',
        'solarnow_branch_access',
        'solarnow_sale_submit',
        'solarnow_ticket_service',
        'solarnow_ticket_installation',
        'solarnow_deposits_in_sales',
        'solarnow_hr_expense',
        'solarnow_migration_data',
        'solarnow_res_partner_listing',
        'solarnow_sale_order_listing',
        'solarnow_stock_picking_wave',
        'solarnow_stock_shortcuts',
        'solarnow_stock_branch',
        'web_add_button_top',
        'web_disable_selection_options'
    ],
    'installable': True,
}
