# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.


def migrate(cr, version):
    query = """
        UPDATE ir_attachment ia
        SET res_id = aiar.application_id, res_model = 'res.partner'
        FROM application_ir_attachments_rel aiar
        WHERE ia.id = aiar.attachment_id;
    """
    cr.execute(query)
