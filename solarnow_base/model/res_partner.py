# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _


class ResPartner(models.Model):
    _inherit = "res.partner"

    distance_to_branch = fields.Float("Distance to branch")

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('res.partner')
    )
    branch_ref_id = fields.Many2one(
        comodel_name="res.branch",
        string="Referred branch",
    )

    @api.multi
    def name_get(self):
        result = []
        for partner in self:
            result.append((partner.id, '{} ({})'.format(partner.name, partner.ref)))
        return result

    phone2 = fields.Char(string="Phone 2")
    birthplace = fields.Char('Place of birth')
    birthdate2 = fields.Date('Date of birth')
    gender = fields.Selection(selection=[('other', 'Other'), ('male', 'Male'), ('female', 'Female')], string='Gender')
    customer_relative = fields.Char('Spouse')
    customer_relative_phone = fields.Char('Spouse contact')
    marital_status = fields.Selection(selection=[('single', 'Single'), ('married', 'Married')], string='Marital Status')
    house = fields.Selection(
        selection=[('unknown','Unknown'), ('rent', 'Rent'), ('buy', 'Buy')],
        default='unknown')
    number_rooms = fields.Integer('Number of rooms')
    live_in_house = fields.Integer('Year living in the house')
    branch_distance = fields.Integer('Distance to the branch')

    loyalty = fields.Many2one('res.partner.loyalty', string="Loyalty program")

    opt_out = fields.Boolean('Opt-Out', default=True)
    mobile_money_number = fields.Boolean("Mobile money number")
    mobile_money_number2 = fields.Boolean("Mobile money number 2")

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        # TODO could it be refined to improve performances?
        MIN_LOOKUP_LEN = 3
        if not name == '' and len(name) > MIN_LOOKUP_LEN:
            phone_ids = self.search([('phone', 'like', name)]).name_get()
            ref_ids =  self.search([('ref', 'like', name)]).name_get()
            partners = phone_ids + ref_ids + super(ResPartner, self).name_search(name=name, args=args, operator=operator, limit=limit)

            seen = []
            return [p for p in partners if p[0] not in seen and not seen.append(p[0])]
        else:
            return super(ResPartner, self).name_search(name=name, args=args, operator=operator, limit=limit)
