# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.osv import fields as oldfields, osv
from openerp import SUPERUSER_ID, tools
import logging
_logger = logging.getLogger(__name__)


class res_branch(models.Model):
    # _inherit = ['res.branch','mail.thread']
    _inherit = ['res.branch']

    company_id = fields.Many2one('res.company', string='Company',
        default=lambda self: self.env['res.company']._company_default_get('res.branch'))

    partner_id = fields.Many2one("res.partner",
        string="Branch Location")
    # Sequence for generating customer ref
    customer_seq_id = fields.Many2one('ir.sequence', 'Customer sequence')
    partner_latitude = fields.Float(related='partner_id.partner_latitude')
    partner_longitude = fields.Float(related='partner_id.partner_longitude')
    location_map = fields.Binary("Map image")
    informal_address = fields.Text('Informal address', help="")
    sales_team_ids = fields.One2many("crm.case.section", "branch_id")
    credit_responsible = fields.Many2one('hr.employee',
        'Credit responsible')

    phone_1 = fields.Char("Phone 1")
    phone_2 = fields.Char("Phone 2")
    branch_description = fields.Text("Branch description")

    rent_start_date = fields.Date("Start date")
    rent_due_date = fields.Date("Next due date")
    rent_paid_date = fields.Date("Date paid")
    rent_amount = fields.Float("Amount")
    rent_pay_period = fields.Selection([
        ("year", "Yearly"), ("quarter", "Quarterly"), ("month", "Monthly")])
    rent_end_date = fields.Date("Contract end date")
    offical_location = fields.Char("Offical location")

    landlord_id = fields.Many2one("res.partner",
        string="Landlord")
    landlord_phone = fields.Char(related='landlord_id.phone')
    landlord_bank = fields.One2many(related='landlord_id.bank_ids')
    tenacy_agreement = fields.Binary("Tenacy agreement",
    	help="Tenacy agreement")
    tenacy_agreement_file_name = fields.Char("Agreement file name", default='agreement.pdf')


    license_partner_id = fields.Many2one("res.partner",
        string="License partner")
    #bank_ids = fields.One2many('res.partner.bank', 'branch_id', 'Banks')
    license_bank_ids = fields.One2many(related='license_partner_id.bank_ids')
    trading_license = fields.Binary("Trading license",
        help="Trading license")

    amount = fields.Float("Amount paid per year")
    date_paid = fields.Date("Date paid")
    date_due = fields.Date("Due date")

    outlook_date_painted_inside = fields.Date("Last painted inside")
    outlook_date_painted_outside = fields.Date("Last painted outside")
    image_branch = fields.Binary("Image")
    indoor_image_branch = fields.Binary("Indoor picture")


    vehicle_ids = fields.One2many('fleet.vehicle', 'branch_id',
    	string="Branch vehicles")
    asset_ids = fields.One2many('account.asset.asset', 'branch_id',
    	string="Branch assets")
    # furniture_ids = fields.One2many('res.branch.furniture', 'branch_id',
    # 	string="Branch furniture")
    # security_ids = fields.One2many('res.branch.security', 'branch_id',
    #     string="Branch security items")
    item_rel_ids = fields.One2many('res.branch.item.rel', 'branch_id',
        string="Branch items")

    demo_vehicle_unit_ids = fields.One2many('res.branch.vehicle.demo.unit', 'branch_id',
        string="Branch demo unit")
    demo_unit_ids = fields.One2many('res.branch.demo.unit', 'branch_id',
    	string="Branch demo unit")

    employee_ids = fields.One2many('hr.employee', 'branch_id')
    user_ids = fields.Many2many("res.users", "res_branch_users_rel",
                                         "branch_id", "user_id",
                                         string="Users")
    code = fields.Char(string='Code', size=2)


class sales_team(models.Model):
    _inherit = "crm.case.section"

    # TODO Add company_id ???
    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('crm.case.section')
    )


class fleet_vehicle(models.Model):
    _inherit = 'fleet.vehicle'

    company_id = fields.Many2one('res.company', string='Company',
        default=lambda self: self.env['res.company']._company_default_get('fleet.vehicle'))

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('fleet.vehicle')
    )

    insurance_paid = fields.Date("Insurance paid")
    insurance_duration = fields.Date("End insurance period")
    insurance_amount = fields.Float("Amount")

    fuel_card_number = fields.Char("Fuel card number")
    driver_id = fields.Many2one('hr.employee', 'Driver')


class account_asset_asset(models.Model):
    _inherit = 'account.asset.asset'

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('account.asset.asset')
    )
    asset_serial_nr = fields.Char("Serial nr")


class res_branch_item_cat(models.Model):
    _name = 'res.branch.item.cat'

    name = fields.Char('Category')


class res_branch_item_rel(models.Model):
    _name = "res.branch.item.rel"

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('res.branch.item.rel')
    )
    item_id = fields.Many2one('res.branch.item',
        string="Name")
    received = fields.Date("Date received")
    quantity = fields.Integer("Quantity")
    # item_id = fields.One2many('res.branch.item', 'branch_id',
    #     string="Branch items")
    item_cat = fields.Char(related='item_id.item_cat_id.name',
        string="Item category")


class res_branch_item(models.Model):
    _name = 'res.branch.item'

    company_id = fields.Many2one('res.company', string='Company',
        default=lambda self: self.env['res.company']._company_default_get('res.branch.item'))

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('res.branch.item')
    )

    name = fields.Char("Item name")
    item_serial_nr = fields.Char("Serial nr")
    item_cat_id = fields.Many2one('res.branch.item.cat',
        string="Item category")


# class res_branch_security(models.Model):
#     _name = 'res.branch.security'

#     company_id = fields.Many2one('res.company', string='Company',
#         default=lambda self: self.env['res.company']._company_default_get('res.branch.security'))

#     branch_id = fields.Many2one(
#         comodel_name="res.branch",
#         string="Branch",
#         default=lambda self: self.env['res.branch']._branch_default_get('res.branch.security')
#     )

#     name = fields.Char("Security item name")
#     received = fields.Date("Date received")
#     quantity = fields.Integer("Quantity")


# class res_branch_furniture(models.Model):
#     _name = 'res.branch.furniture'

#     company_id = fields.Many2one('res.company', string='Company',
#         default=lambda self: self.env['res.company']._company_default_get('res.branch.furniture'))

#     branch_id = fields.Many2one(
#         comodel_name="res.branch",
#         string="Branch",
#         default=lambda self: self.env['res.branch']._branch_default_get('res.branch.furniture')
#     )

#     name = fields.Char("Furniture name")
#     received = fields.Date("Date received")
#     quantity = fields.Integer("Quantity")


class res_branch_demo_vehicle_unit(models.Model):
    _name = 'res.branch.vehicle.demo.unit'

    company_id = fields.Many2one('res.company', string='Company',
        default=lambda self: self.env['res.company']._company_default_get('res.branch.vehicle.demo.unit'))

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('res.branch.vehicle.demo.unit')
    )

    product_id = fields.Many2one('product.product',
        string="Product")
    serial = fields.Char("Unit serial number", length=24)
    received = fields.Date("Date received")
    vehicle_id = fields.Many2one('fleet.vehicle')


class res_branch_demo_unit(models.Model):
    _name = 'res.branch.demo.unit'

    company_id = fields.Many2one('res.company', string='Company',
        default=lambda self: self.env['res.company']._company_default_get('res.branch.demo.unit'))

    branch_id = fields.Many2one(
        comodel_name="res.branch",
        string="Branch",
        default=lambda self: self.env['res.branch']._branch_default_get('res.branch.demo.unit')
    )

    product_id = fields.Many2one('product.product',
        string="Product")
    serial = fields.Char("Unit serial number", length=24)
    received = fields.Date("Date received")
