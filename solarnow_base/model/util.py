def collect_and_reconcile(self, move_lines, account_ids=[], writeoff_account=False, writeoff_period=False, writeoff_journal=False, partial=False):
    to_reconcile_ids = {}
    for line in move_lines:
        if line.account_id.id in account_ids and not line.reconcile_id and line.account_id.reconcile:
            to_reconcile_ids.setdefault(line.account_id.id, []).append(line.id)                
            if line.reconcile_partial_id:
                to_reconcile_ids[line.account_id.id].extend([l.id for l in line.reconcile_partial_id.line_partial_ids])

    for account in to_reconcile_ids.keys():
        if partial:
            self.pool.get('account.move.line').reconcile_partial(self.env.cr,
                                                                 self.env.uid, 
                                                                 list(set(to_reconcile_ids[account])))
        else:
            # FIXME if writeoff_journal==False, this following line fails
            self.pool.get('account.move.line').reconcile(self.env.cr, 
                                                         self.env.uid, 
                                                         list(set(to_reconcile_ids[account])), 
                                                         writeoff_acc_id=writeoff_account,
                                                         writeoff_period_id=writeoff_period,
                                                         writeoff_journal_id=writeoff_journal)

    return True
