# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2015 Onestein BV (www.onestein.eu).`
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful'
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp import models, fields, api, _
from openerp.osv import fields as oldfields, osv
from openerp import SUPERUSER_ID, tools
import logging
_logger = logging.getLogger(__name__)

class multi_branch_default(models.Model):
    """
    Manage multi Branch default value
    """
    _name = 'multi_branch.default'
    _description = 'Default multi branch'
    _order = 'branch_id,sequence,id'

    sequence = fields.Integer(string='Sequence', default= 100)
    name = fields.Char(string="Name", required=True,
                       help='Name it to easily find a record')
    branch_id = fields.Many2one(
        comodel_name='res.branch', string='Main Branch', required=True,
        help='Branch where the user is connected')
    branch_dest_id = fields.Many2one(
        comodel_name="res.branch", string="Default Branch",
        required=True, help='Branch to store the current record')
    object_id = fields.Many2one(
        comodel_name="ir.model", string= "Object", required=True,
        help='Object affected by this rule')
    expression = fields.Char(string='Expression', required=True,
                             help='Expression, must be True to match\nuse '
                                  'context.get or user (browse)',
                             default='True')
    field_id = fields.Many2one(
        comodel_name="ir.model.fields",
        string= "Field", help='Select field property')


    def copy(self, cr, uid, id, default=None, context=None):
        """
        Add (copy) in the name when duplicate record
        """
        if not context:
            context = {}
        if not default:
            default = {}
        branch = self.browse(cr, uid, id, context=context)
        default = default.copy()
        default['name'] = branch.name + _(' (copy)')
        return super(multi_branch_default, self).copy(
            cr, uid, id, default, context=context
        )

class res_branch(osv.Model):
    _name = "res.branch"
    _inherit = 'mail.thread'
    _description = "Branch"

    _columns = {
        'name': oldfields.char(string="Branch", required=True, size=64),
        'parent_id': oldfields.many2one("res.branch",
                                     string="Parent Branch"),
        'child_ids': oldfields.one2many("res.branch","parent_id",
                                     string="Child Branch"),
        'account_id': oldfields.many2one("account.account",
                                     string="Account"),
        }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
         'The Branch name must be unique !')
    ]

    def _branch_default_get(self, cr, uid, object=False,
                           field=False, context=None):
        """
        Check if the object for this unit has a default value
        """

        if not context:
            context = {}

        self.env = api.Environment(cr, uid, context)
        proxy = self.pool.get('multi_branch.default')
        args = [
            ('object_id.model', '=', object),
            ('field_id', '=', field),
        ]

        ids = proxy.search(cr, uid, args, context=context)
        user = self.pool.get('res.users').browse(
            cr, SUPERUSER_ID, uid, context=context
        )
        for rule in proxy.browse(cr, uid, ids, context):
            if eval(rule.expression, {'context': context, 'user': user}):
                return rule.branch_dest_id.id
        return user.employee_ids and user.employee_ids[0].branch_id or user.branch_id or self.env['res.branch']

    def name_search(self, cr, uid, name='', args=None,
                    operator='ilike', context=None, limit=100
                    ):
        context = dict(context or {})
        if context.pop('user_preference', None):
            # We browse as superuser. Otherwise, the user would be able to
            # select only the currently visible companies (according to rules,
            # which are probably to allow to see the child companies) even if
            # she belongs to some other companies.
            user = self.pool.get('res.users').browse(
                cr, SUPERUSER_ID, uid, context=context)
            cmp_ids = list(set([user.branch_id.id] +
                               [cmp.id for cmp in user.branch_ids]))
            uid = SUPERUSER_ID
            args = (args or []) + [('id', 'in', cmp_ids)]
        return super(res_branch, self).name_search(
            cr, uid, name=name, args=args, operator=operator,
            context=context, limit=limit)

    @tools.ormcache()
    def _get_branch_children(self, cr, uid=None, branch=None):
        if not branch:
            return []
        ids =  self.search(cr, uid, [('parent_id','child_of',[branch])])
        return ids

    def _get_parent_hierarchy(self, cr, uid, branch_id, context=None):
        if branch_id:
            parent_id = self.browse(cr, uid, branch_id)['parent_id']
            if parent_id:
                return self._get_parent_hierarchy(
                    cr, uid, parent_id.id, context
                )
            else:
                return self._get_parent_descendance(
                    cr, uid, branch_id, [], context
                )
        return []

    def _get_parent_descendance(self, cr, uid, branch_id, descendance, context=None):
        descendance.append(self.browse(cr, uid, branch_id).partner_id.id)
        for child_id in self._get_branch_children(cr, uid, branch_id):
            if child_id != branch_id:
                descendance = self._get_parent_descendance(cr, uid, child_id, descendance)
        return descendance

    #
    # This function restart the cache on the _get_branch_children method
    #
    def cache_restart(self, cr):
        self._get_branch_children.clear_cache(self)


class res_users(osv.Model):
    _inherit = 'res.users'

    _columns = {
        'branch_id':  oldfields.many2one("res.branch", string="Branch",
                                        help='The Branch this user '
                                             'is currently working for.'),
        'branch_ids': oldfields.many2many("res.branch", "res_branch_users_rel",
                                         "user_id", "branch_id",
                                         string="Branches")
    }

    def __init__(self, pool, cr):
        init_res = super(res_users, self).__init__(pool, cr)
        # duplicate list to avoid modifying the original reference
        self.SELF_WRITEABLE_FIELDS = list(self.SELF_WRITEABLE_FIELDS)
        self.SELF_WRITEABLE_FIELDS.extend(['branch_id'])

        self.SELF_READABLE_FIELDS = list(self.SELF_READABLE_FIELDS)
        self.SELF_READABLE_FIELDS.extend(['branch_id'])

        return init_res

    def _get_branch(self,cr, uid, context=None, uid2=False):
        if not uid2:
            uid2 = uid
        # Use read() to compute default Branch, and pass load=_classic_write to
        # avoid useless name_get() calls. This will avoid prefetching fields
        # while computing default values for new db columns, as the
        # db backend may not be fully initialized yet.
        user_data = self.pool['res.users'].read(cr, uid, uid2, ['branch_id'],
                                                context=context, load='_classic_write')
        branch_id = user_data['branch_id']
        return branch_id

    def _get_branchs(self, cr, uid, context=None):
        c = self._get_branch(cr, uid, context)
        if c:
            return [c]
        return False

    def _check_branch(self, cr, uid, ids, context=None):
        return all(((this.branch_id in this.branch_ids) or not this.branch_ids)
                 for this in self.browse(cr, uid, ids, context))

    _constraints = [
       (_check_branch, 'The chosen Branch is not in the allowed '
                      'Branches for this user',
        ['branch_id', 'branch_ids']),
       ]

    _defaults = {
         'branch_id': _get_branch,
         'branch_ids': _get_branchs,
         }

# The following might be needed on all branch related models when issues on multi branch functionality arrise. 
# "No access to res.users issue" is now solved by extending record rules.
# class crm_lead(osv.osv):
#     _inherit = 'crm.lead'
    
#     def search(self, cr, uid, args, offset=0, limit=None, order=None, context=None, count=False):
#         if not context:
#             context = {}
#         if not 'branch_ids' in context:
#             branch_list = self.pool.get('res.users').read(cr, 1, uid, fields=['branch_ids'], context=context)['branch_ids']
#         if not(uid == 1):
#             args.extend([('branch_id.id', 'in', branch_list)])
#         return super(crm_lead, self).search(cr, uid, args, offset, limit, order, context, count)
# End security isue solution
