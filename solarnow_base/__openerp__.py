# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 ONESTEiN BV (<http://www.onestein.nl>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': "SolarNow Base",
    'summary': """Solarnow base module""",
    'description': """
    Solarnow customizations, base module.
    """,
    'author': "ONESTEiN BV",
    'website': "http://www.onestein.eu",
    'category': 'Custom',
    'version': '0.7',
    'depends': [
        'base',
#        'solarnow_legacy_fix',
        'sales_team',
        'fleet',
        'account',
        'account_asset',
        'base_geolocalize',
        'crm_helpdesk',
        'hr',
        'knowledge',
        'user_partner_is_employee'
    ],
    'data': [
        "security/ir.model.access.csv",
        "data/module_data.xml",
        "security/security.xml",
        "security/branch_security.xml",
        "data/branch_data.xml",
        "view/res_branch_view.xml",
        "view/fleet_view.xml",
        "view/asset_view.xml",
        "view/res_company_view.xml",
        "view/res_partner_view.xml",
        'view/res_partner_loyalty.xml',
        "view/branch_item_view.xml",
        "view/res_partner_bank_view.xml",
        "view/knowledge_view.xml",
        "view/comment_type_view.xml"
    ],
    'qweb': [
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
}
